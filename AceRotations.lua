addonName, Rotations = ...

--UIErrorsFrame:UnregisterEvent("UI_ERROR_MESSAGE")
local longest = 25;
local initialized = false;
local orig_AuctionFrameBrowse_Update;
LURS_Settings = {
	MinimapPos = 45,
	IsVisible = false,
	ROTATION_STARTED = true,
	INTERRUPTER_STARTED = false,
	FISH_STARTED = false

}
Ace = {}
Ace.DebugText = {label = "", text = "",frame = nil}
function LURS_MinimapButton_Reposition()
	LURS_MinimapButton:SetPoint("TOPLEFT","Minimap","TOPLEFT",52-(80*cos(LURS_Settings.MinimapPos)),(80*sin(LURS_Settings.MinimapPos))-52)
end

function LURS_MinimapButton_DraggingFrame_OnUpdate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UIParent:GetScale()+70
	ypos = ypos/UIParent:GetScale()-ymin-70

	LURS_Settings.MinimapPos = math.deg(math.atan2(ypos,xpos))
	LURS_MinimapButton_Reposition()

end

function LURS_MinimapButton_OnClick()
	LURS_Settings.IsVisible = not LURS_Settings.IsVisible
	if LURS_Settings.IsVisible then LURS_frame:Show() TitleFrame:Show()  else LURS_frame:Hide() TitleFrame:Hide() end
end


local TitleFrame
function LURS_frame_OnLoad()

	TitleFrame = CreateFrame("Frame","TitleFrame",LURS_frame)
	TitleFrame:SetPoint('TOPLEFT', LURS_frame, 0, 15)
	TitleFrame:SetSize(longest * 8,30);
	TitleFrame:SetBackdrop({ bgFile = 'Interface\\RaidFrame\\Raid-Bar-Resource-Fill', bgtexture = 'Interface\\RaidFrame\\Raid-Bar-Resource-Background',
	tile = false, tileSize = 16, edgeSize = 1});
	--TitleFrame:SetBackdropColor(.1, .2, .3, 1)
	--TitleFrame:SetBackdropBorderColor(.4, .4, .4, 1)

	TitleFrame:SetBackdropBorderColor(0,0,0,1);
	TitleFrame:SetBackdropColor(.5,.2,.9,1);
	
	TitleFrame:SetMovable(true)
	TitleFrame:SetScript("OnMouseDown", function(self) self:GetParent():StartMoving() TitleFrame:SetPoint('TOPLEFT', LURS_frame, 0, 15) end)
	TitleFrame:SetScript("OnMouseUp", function(self) self:GetParent():StopMovingOrSizing() TitleFrame:SetPoint('TOPLEFT', LURS_frame, 0, 15) end)
	TitleFrame:SetScript("OnHide", function(self) LURS_Settings.IsVisible = false end)
	TitleFrame:SetScript("OnShow", function(self) LURS_Settings.IsVisible = true end)
	TitleFrame:Hide()
	 
	local TitleText = TitleFrame:CreateFontString("TitleText","OVERLAY")
	TitleText:SetFont("Fonts\\FRIZQT__.TTF",12)
	TitleText:SetText("Ace Rotations")
	TitleText:SetPoint('Left', TitleFrame, 10, 1)
	
	
	local ExitText = TitleFrame:CreateFontString("TitleText","OVERLAY")
	ExitText:SetFont("Fonts\\FRIZQT__.TTF",12)
	ExitText:SetText("X")
	ExitText:SetPoint('Right', TitleFrame, -10, 1)
	
	
	local b = CreateFrame("Button", "TestButton", TitleFrame, "")
	b:SetSize(24, 24)
	b:SetPoint("Right" ,-2,0)
	--b:RegisterForClicks("AnyUp", "AnyDown")
	b:SetScript("OnClick", function(self, button, down)
		LURS_MinimapButton_OnClick()
	end)

	LURS_frame:SetSize(200, 200)
	LURS_frame:SetPoint("CENTER", -200, 0)
	--LURS_frame:SetBackdrop({ bgFile = "Interface\\DialogFrame\\UI-DialogBox-Gold-Background", edgeFile = "Interface\\Tooltips\\UI-Tooltip-Border", tileSize = 12, edgeSize = 12, insets = {left=3, right=3, top=3, bottom=3}, })
	--LURS_frame:SetBackdropColor(.1, .2, .3, 1)
	LURS_frame:SetBackdrop({
		bgFile = "Interface\\Buttons\\WHITE8x8", 
		edgeFile = "Interface\\Buttons\\WHITE8x8", 
		tile = false, tileSize = 16, edgeSize = 2, 
		insets = { left = 1, right = 1, top = 1, bottom = 1 }});
	LURS_frame:SetBackdropColor(0,0,0,.8);
	LURS_frame:SetBackdropBorderColor(.5,.2,.9,1);
	LURS_frame:EnableMouse(true)
	LURS_frame:SetMovable(true)
	LURS_frame:SetUserPlaced(true)
	LURS_frame:SetClampedToScreen(true)
	LURS_frame:SetScript("OnMouseDown", function(self) self:StartMoving() TitleFrame:SetPoint('TOPLEFT', LURS_frame, 0, 20) end)
	LURS_frame:SetScript("OnMouseUp", function(self) self:StopMovingOrSizing() TitleFrame:SetPoint('TOPLEFT', LURS_frame, 0, 20)end)
	LURS_frame:SetScript("OnHide", function(self) LURS_Settings.IsVisible = false end)
	LURS_frame:SetScript("OnShow", function(self) LURS_Settings.IsVisible = true end)
	LURS_frame:RegisterEvent("ADDON_LOADED")
	LURS_frame:SetScript("OnEvent", function(...)
	LURS_frame:CreateTitleRegion()
	DamageKey:SetText(tostring(DAMAGE_KEY))
	HealingKey:SetText(tostring(HEALING_KEY))
	end)
	LURS_frame:SetFrameStrata("DIALOG")
	LURS_frame:Hide()

	_G[Rotation_checkbox:GetName()..'Text']:SetText('Rotation Enabled')
	Rotation_checkbox:SetPoint('TOPLEFT', LURS_frame, 10, -10)
	Rotation_checkbox:SetSize(20, 20)
	Rotation_checkbox:SetHitRectInsets(0, -20, 0, 0)
	Rotation_checkbox:SetChecked(LURS_Settings.ROTATION_STARTED)

	_G[Interrupter_checkbox:GetName()..'Text']:SetText('FishBot')
	Interrupter_checkbox:SetPoint('TOPLEFT', LURS_frame, 10, -30)
	Interrupter_checkbox:SetSize(20, 20)
	Interrupter_checkbox:SetHitRectInsets(0, -20, 0, 0)
	Interrupter_checkbox:SetChecked(LURS_Settings.INTERRUPTER_STARTED)

	BindingRotationButton:SetPoint('TOPLEFT', LURS_frame, 40, -10)
	BindingRotationButton:SetSize(60, 20)

	BindingInterrupterButton:SetPoint('TOPLEFT', LURS_frame, 40, -30)
	BindingInterrupterButton:SetSize(60, 20)

	--_G[DamageKey:GetName()..'Text']:SetText('Damage Key.')
	DamageKeyText:SetPoint('TOPLEFT', LURS_frame, 10, -60)
	DamageKeyText:SetSize(200,25)
	DamageKeyText:SetTextColor(1, 1, 1, 1)
	DamageKey:SetSize(50,10)
	DamageKey:SetPoint('TOPLEFT', LURS_frame,100, -68)
	DamageKey:SetText(tostring(DAMAGE_KEY) or "")
	DamageKey:SetMaxLetters(5)
	DamageKey:SetAutoFocus(false)
	--_G[HealingKey:GetName()..'Text']:SetText('Healing Key.')
	HealingKeyText:SetPoint('TOPLEFT', LURS_frame, 10, -90)
	HealingKeyText:SetSize(200,25)
	HealingKeyText:SetTextColor(1, 1, 1, 1)
	HealingKey:SetSize(50,10)
	HealingKey:SetPoint('TOPLEFT', LURS_frame,100, -98)
	HealingKey:SetText(tostring(HEALING_KEY) or "")
	HealingKey:SetMaxLetters(5)
	HealingKey:SetAutoFocus(false)
	
	CurrentTargetText:SetPoint('TOPLEFT', LURS_frame, 10, -120)
	CurrentTargetText:SetSize(200,25)
	CurrentTargetText:SetTextColor(1, 1, 1, 1)
	LastSpellText:SetPoint('TOPLEFT', LURS_frame, 10, -140)
	LastSpellText:SetSize(200,25)
	LastSpellText:SetTextColor(1, 1, 1, 1)

end
function addDebugText(label, text)
	local exists = false
	for i=1, #Ace.DebugText do 
		local labelcheck = Ace.DebugText[i].label
		if(labelcheck == label) then
			Ace.DebugText[i].label = label
			Ace.DebugText[i].text = text
			exists = true;
		end
	end
	if exists == false then
		table.insert(Ace.DebugText,{label = label,text = text})
	end
	
end
function showDebugText()

local height = 1;
	for i=1, #Ace.DebugText do 
		local label = Ace.DebugText[i].label
		local text =  Ace.DebugText[i].text
		
		if not Ace.DebugText[i].frame then
			local TitleText = LURS_frame:CreateFontString("TitleText"..label,"OVERLAY","GameFontNormal")
			Ace.DebugText[i].frame = TitleText
		end
		--Ace.DebugText[i].frame:SetFont("Fonts\\ARIALN.TTF",14)
		Ace.DebugText[i].frame:SetText((label).." : "..(text or ''))
		Ace.DebugText[i].frame:SetTextColor(1, 1, 1, 1)
		if i == 1 then
		Ace.DebugText[i].frame:SetPoint('TOPLEFT', LURS_frame, 10, -165)
		else
		Ace.DebugText[i].frame:SetPoint('TOPLEFT', LURS_frame, 10, (145 + (20 * i) )* -1) 
		end
		height = i
		if string.len(Ace.DebugText[i].frame:GetText()) > longest then
			longest = string.len(Ace.DebugText[i].frame:GetText())
		end
	end
	TitleFrame:SetSize(longest * 8,22);
	LURS_frame:SetSize(longest * 8, 160 +(20 * (height + 1)))
end
function LURS_Rotation_checkbox_OnClick()
	LURS_Settings.ROTATION_STARTED = not LURS_Settings.ROTATION_STARTED
	Rotation_checkbox:SetChecked(LURS_Settings.ROTATION_STARTED)
end

function LURS_Interrupter_checkbox_OnClick()
	LURS_Settings.FISH_STARTED = not LURS_Settings.FISH_STARTED
	Interrupter_checkbox:SetChecked(LURS_Settings.FISH_STARTED)
end
function updateDamageKey()
	DAMAGE_KEY = DamageKey:GetText()
	DamageKey:ClearFocus()
end

function updateHealingKey()
	--print("set healing key")
	HEALING_KEY = HealingKey:GetText()
	HealingKey:ClearFocus()
end
function createLabel(name,text,x,y)
	Frame:CreateFontString(name,"OVERLAY","GameFontNormal")
	name:SetSize(63,34)
	name:SetPoint('TOPLEFT', LURS_frame, x,y)
	name:SetText(text)
end
function updateCurrentTarget(unit)
	CurrentTargetText:SetText("Target: "..(unit or "None"))
	if string.len(CurrentTargetText:GetText()) > longest then
		TitleFrame:SetSize(longest * 8,30);
		LURS_frame:SetSize(longest * 8, 160 +(20 * (1 + 1)))
	end
end
function updateLastSpell(spellId)
	local text = "Spell: "..(GetSpellInfo(spellId) or "None");
	LastSpellText:SetText(text)
	if string.len(LastSpellText:GetText()) > longest then
		TitleFrame:SetSize(longest * 8,30);
		LURS_frame:SetSize(longest * 8, 160 +(20 * (1 + 1)))
	end
end
local functionsToHook = {'UnitClass','UnitReaction','UnitName','UnitIsPlayer','UnitHealth','UnitIsDead','UnitInParty','UnitInRaid'}
function HookFunctions()

	for k,v in pairs(_G) do 
		if type( v ) == "function" then 
			for _,name in pairs(functionsToHook) do		
			--addDebugText(tostring(k) , tostring(v) )
				if k == name then
					print("|cff00FF7F[Hooked:|cffffff00", k..'|cff00FF7F]')
					if _G["stored"..k] == nil then  _G["stored"..k] = _G[k] end
					_G[k] = function(...)
						return _G['stored'..k](PrepareGUID(...))
					end
				end
			end
		end
	end
	-- _G['GetAuctionItemTimeLeft'] = function(...)
		-- return GetExactAuctionTimeLeft(...)
	-- end
end


--local _G = _G
Rotation_Starter_Frame = _G['Rotation_Starter']
if Rotation_Starter_Frame == nil then Rotation_Starter_Frame = CreateFrame('frame', 'Rotation_Starter', UIParent) end
Rotation_Starter_Frame:RegisterEvent("ADDON_ACTION_FORBIDDEN")
Rotation_Starter_Frame:RegisterEvent("AUCTION_HOUSE_SHOW")
Rotation_Starter_Frame:SetScript("OnEvent", function(...)
  -- We can attempt to hide these without totally raping the UI
  local t, event ,addon= ...
  if addon == addonName then
 
  end
  if event == 'AUCTION_HOUSE_SHOW' then
	OnAuctionHouseShow()
  end
end)

function OnAuctionHouseShow()
    Rotation_Starter_Frame:RegisterEvent("AUCTION_HOUSE_CLOSED", OnAuctionHouseClosed)

    -- hook the AH update function
    if not Orig_AuctionFrameBrowse_Update then
        --Orig_AuctionFrameBrowse_Update = AuctionFrameBrowse_Update
        --AuctionFrameBrowse_Update = AuctionFrameBrowse_UpdateHook
        Orig_AuctionFrameBrowse_Update = true
        hooksecurefunc( "AuctionFrameBrowse_Update", hookAuctionFrameBrowse_Update)
    end
end
function hookAuctionFrameBrowse_Update()
	local numItems = GetNumAuctionItems("list")
	local offset = FauxScrollFrame_GetOffset(BrowseScrollFrame)

	for i = 1, NUM_BROWSE_TO_DISPLAY do
		local index = offset + i + NUM_AUCTION_ITEMS_PER_PAGE * AuctionFrameBrowse.page
		if index > numItems + NUM_AUCTION_ITEMS_PER_PAGE * AuctionFrameBrowse.page then return end
		buttonName = "BrowseButton"..i;
		_G[buttonName.."ClosingTimeText"]:SetText(SecondsToTime(GetExactAuctionTimeLeft("List",offset+i) / 1000));
		_G[buttonName.."ClosingTime"].tooltip = SecondsToTime(GetExactAuctionTimeLeft("List",offset+i) / 1000);
		
	end
end

Rotation_Starter_Frame:SetScript('OnUpdate', function(self, elapsed)
	--RegisterMovement(elapsed)
	--print(GetKeyState)
	eventHandlerSetup()
	showDebugText()
	if LURS_Settings.FISH_STARTED then
		startFishing()
	end
	TimeToDieRefresh(allUnits)
	if (PrepareGUID and initialized == false) then
			HookFunctions()
			print("|cff00FF7F[|cffffff00AceRotations Loaded|cff00FF7F]")
		
			initialized = true
	end

				
	
	
	local throttle = 0.35
	self.st = elapsed + (self.st or 0)
	if self.st > throttle then
		if LURS_Settings.ROTATION_STARTED and GetKeyState ~= nil then
			UnitCacheManager(40)
			local MyClass = select(2, UnitClass('player')):lower()
			Rotations[MyClass].ROTATION()
		end
		self.st = 0
	end
end)
--113--
