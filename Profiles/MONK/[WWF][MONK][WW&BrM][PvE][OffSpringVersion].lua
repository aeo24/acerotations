WWFMONKWWBRMROTATIONPRESETS = false
    
    SLASH_BUFFMODE1 = "/SwitchBuffMode"
    function SlashCmdList.BUFFMODE()
        if RBMV[1][2] == 1 then RBMV[1][2] = 0 else RBMV[1][2] = 1 end
        print(" ")
        print(string.format("Buff Mode: %s.", RBMV[1][2] == 0 and "|cff00ff00Player|r" or "|cff00ff00Group|r"))
    end
    
    SLASH_DISPELLMODE1 = "/SwitchDispellMode"
    function SlashCmdList.DISPELLMODE()
        if DMV[1][2] == 1 then DMV[1][2] = 0 else DMV[1][2] = 1 end
        print(" ")
        print(string.format("Dispell Mode: %s.", DMV[1][2] == 0 and "|cff00ff00Player|r" or "|cff00ff00Group|r"))
    end
    
    SLASH_ROTATION1 = "/SwitchRotation"
    function SlashCmdList.ROTATION()
        if (RV[1][2] == 0 or RV[1][2] == 2) then
            RV[1][2] = 1
            print(" ")
            print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." Single Rotation Activated.|R")
        elseif RV[1][2] == 1 then
            if not IsPlayerSpell(116847) then 
                RV[1][2] = 0
                print(" ")
                print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." AoE Rotation Activated.|R")
            else 
                RV[1][2] = 2
                print(" ")
                print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." Cleave Rotation Activated.|R")
            end
        end
    end
    
    SLASH_TOGGLEMYSPELL1 = "/togglemyspell"
    function SlashCmdList.TOGGLEMYSPELL(msg)
        local Command, SpellName = msg:match("^(%S*)%s*(.-)$")
        if Command == "info" then
            print(" ")
            print("Full list of spells that can be included into rotation:")
            print(" ")
            for i, v in pairs(VFQ) do print(i..") "..GetSpellLink(VFQ[i][3])) end
            print(" ")
            print("Use macro '/togglemyspell localized spell name' to activate these spells for one use.")
            print("Use macro '/togglemyspell CD localized spell name' to activate these spells for using off of CD.")
            if GetSpecialization() == 3 then 
            print("Use macro '/castfistsoffury' to activate FOF with high priority for one use.")
            print("Use macro '/castfistsoffury CD' to activate FOF with high priority for unlimited use.")
            end
            print("Use macro '/togglemyspell active' to get info about spells included into rotation.")
            print("Use macro '/SwitchBuffMode' for switching Player or Raid/Group buff tracking.")
            print("Use macro '/SwitchDispellMode' for switching Player or Raid/Group debuff tracking.")
            print("Use macro '/SwitchRotation' for switching Single/Cleave/AoE.")
            if GetSpecialization() == 3 then print("Use macro '/TigereyeBrewToggle' for toggle ON/OFF Tigereye Brew.") end
            if GetSpecialization() == 3 then print("Use macro '/RangedRotationToggle' for toggle ON/OFF ranged attacks.") end
            if GetSpecialization() == 1 then print("Use macro '/AutoTauntToggle' for toggle ON/OFF auto-taunt.") end
        elseif Command == "active" then
            print(" ")
            print("List of spells included into rotation:")
            print(" ")
            x = 0
            for i, v in pairs(VFQ) do if VFQ[i][2] == 1 then x = x + 1 print(x..") "..GetSpellLink(VFQ[i][3])) end end
        elseif Command == "CD" and SpellName ~= "" then
            for i, v in pairs(VFQ) do
                if VFQ[i][1] == SpellName and IsPlayerSpell(VFQ[i][3]) then
                    if GetSpecialization() == 3 and i == 11 then FOFVFQ[1][2] = 0 end
                    if VFQ[i][2] == 1 then VFQ[i][2] = 0 VFQ[i][5] = 0 else VFQ[i][2] = 1 VFQ[i][5] = 0 end
                    print(" ")
                    if GetSpecialization() == 3 and i == 11 then 
                        print(string.format("%s has been %s.", GetSpellLink(SpellName), VFQ[i][2] == 1 and "|cff00ff00included|r into rotation" or "|cffff0000excluded|r from rotation"))
                    else
                        print(string.format("%s has been %s.", GetSpellLink(SpellName), VFQ[i][2] == 1 and "|cff00ff00included|r into rotation for using off of CD" or "|cffff0000excluded|r from rotation"))
                    end
                end
            end
        elseif Command ~= "CD" then
            SpellName = msg
            for i, v in pairs(VFQ) do
                if VFQ[i][1] == SpellName and IsPlayerSpell(VFQ[i][3]) then
                    if GetSpecialization() == 3 and i == 11 then FOFVFQ[1][2] = 0 end
                    if VFQ[i][2] == 1 then VFQ[i][2] = 0 VFQ[i][5] = 0 else VFQ[i][2] = 1 VFQ[i][5] = 1 end
                    print(" ")
                    print(string.format("%s has been %s.", GetSpellLink(SpellName), VFQ[i][2] == 1 and "|cff00ff00included|r into rotation for one use" or "|cffff0000excluded|r from rotation"))
                end
            end
        end
    end
    
    MONKATOWU = nil
    function MONKATOWU(CheckType, ID, CD, Index)
        if CheckType == "Buff" then
            if UnitBuffID("player", ID, "player") then
                if VFQ[Index][2] == 1 and not VFQ[Index][4] then
                    VFQ[Index][2] = 0
                    VFQ[Index][4] = true
                    VFQ[Index][5] = 0
                    print(" ")
                    print(string.format("%s has been |cffff0000excluded|r from rotation.",GetSpellLink(ID)))
                end
            else
                VFQ[Index][4] = false
            end
        elseif CheckType == "CD" then
            if GetSpellCD(ID) > CD then
                if VFQ[Index][2] == 1 and not VFQ[Index][4] then
                    if WindWalker then if FOFVFQ[1][2] == 1 and ID == 113656 then FOFVFQ[1][2] = 0 end end
                    VFQ[Index][2] = 0
                    VFQ[Index][4] = true
                    VFQ[Index][5] = 0
                    print(" ")
                    print(string.format("%s has been |cffff0000excluded|r from rotation.",GetSpellLink(ID)))
                end
            else
                VFQ[Index][4] = false
            end
        elseif CheckType == "HealingSpheres" then
            local CurrentCounterValue = select(4, UnitBuffID("player",ID)) or 0
            if VFQ[Index][4] ~= CurrentCounterValue then
                if VFQ[Index][4] < CurrentCounterValue and VFQ[Index][2] == 1 then
                    VFQ[Index][2] = 0
                    VFQ[Index][5] = 0
                    print(" ")
                    print(string.format("%s has been |cffff0000excluded|r from rotation.",GetSpellLink(VFQ[Index][3])))
                end
                VFQ[Index][4] = CurrentCounterValue
            end
        end
    end
	
    --===============--
    --== FUNCTIONS ==--
    --===============--
    
    -- Target validation
    
    MonkValidTarget = nil
    function MonkValidTarget()
    
        if UnitExists("target") and not UnitIsDeadOrGhost("target")
        and UnitCanAttack("player","target") and UnitAffectingCombat("player")
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Zen Meditation
        and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
        and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
            return true
        else
            return false
        end
        
    end
    
    --========================--
    --== Rotation functions ==--
    --========================--
    
    MonkTrackingFunc = nil
    function MonkTrackingFunc()
    
        -- Specialization changes tracking
        if LastMonkSpec ~= GetSpecialization() then WWFMONKWWBRMROTATIONPRESETS = false return true end
        
        -- RJW tracking
        if RV[1][2] == 0 and IsPlayerSpell(116847) then RV[1][2] = 2 return true
        elseif RV[1][2] == 2 and not IsPlayerSpell(116847) then RV[1][2] = 0 return true end
        
    end
    
    Pause = nil
    function Pause()
        
        local FoodBuff = false
        
        for i = 1, #Food do if UnitBuffID("player",Food[i]) then FoodBuff = true end end
        
        if IsShiftKeyDown() or IsLeftAltKeyDown() or IsMounted() or FoodBuff then return true end
        
    end
    
    MonkBossEvents = nil
    function MonkBossEvents()
        
        for i=1, 4 do
            
            if UnitExists("boss"..i) then
                
                MyHP = UnitHealth("player")*100/UnitHealthMax("player")
                BossID = tonumber(UnitGUID("boss"..i):sub(6,10), 16) 
                Mode = InstanceDifficulty()    
                
                if UnitExists("target") then TargetID = tonumber(UnitGUID("target"):sub(6,10), 16) else TargetID = 0 end
                
                -- use this over TargetLastTarget() its more reliable
                local name, realm = UnitName("target")
                if not realm then
                    if not name then
                        lasttargetname = "none"
                    else
                        lasttargetname = name
                    end
                else
                    lasttargetname = name.."-"..realm
                end
                    
                --========================================================================--
                --==================  Throne Of Thunder Boss Events ======================--
                --========================================================================--
                
                -- Jin Rokh
                
                if Mode == "HeroicRaid" and BossID == 69465 then -- Jin Rokh HM
                
                    if UnitDebuffID("player",138733) -- Ionization 
                    and not UnitDebuffID("player",138002) -- Fluidity
                    and not RangeCheck(6) then
                        if IsSpellKnown(122783) and GetSpellCD(122783) < 1 then -- Diffuse magic
                            if IsSpellAvailable(122783) then
                                oexecute("CastSpellByName(GetSpellInfo(122783))")
                            end
                        elseif IsSpellAvailable(115176) -- Dzen meditation
                        and select(7, UnitDebuffID("player",138733)) - GetTime() < 2 then
                            StopAttack()
                            ClearTarget()
                            oexecute("RunMacroText('/target @player')")
                            oexecute("CastSpellByName(GetSpellInfo(115176))")
                        end
                    end
                    
                end
                
                -- Horridon
                
                -- 4 gate, Hex of Confusion, stop attack
                if BossID == 68476 and UnitDebuffID("player",136513) then
                    oexecute("StopAttack()")
                    return true
                end
                
                if Mode == "HeroicRaid" and BossID == 68476 then -- Horridon HM
                    
			local DirehornSpirits = {	"��� ��������", -- �������
							"Espirito de Escornante", -- Portugues Brasileiro
							"Esprit de navrecorne", -- Francais
							"Espiritu de cuernoatroz", -- Espanol
							"Direhorn Spirit", -- English
							"Terrorhorngeist" -- Deutsch
						}
                    
                    if UnitDebuffID("player",140946) then -- Direhorn Spirit detection
                        if UnitExists("focus") and not UnitIsDeadOrGhost("focus") then -- focus exist
                            local FocusID = tonumber(UnitGUID("focus"):sub(6,10), 16)
                            if FocusID ~= 70688 then -- not a Direhorn Spirit
                            oexecute("ClearFocus()")
                            else -- Direhorn Spirit in focus
                                if IsSpellInRange(GetSpellInfo(115078),"focus") == 1 then
                                    if WindWalker and HaveGlyph(85698) then
                                        if IsSpellAvailable(123408) and UnitPower("player",12) >= 1 then
                                            oexecute("CastSpellByName(GetSpellInfo(123408),'focus')")
                                        end
                                    else
                                        if IsSpellAvailable(117952) then
                                            oexecute("CastSpellByName(GetSpellInfo(117952),'focus')")
                                        end
                                    end
                                end
                            end
                        else -- focus is clear
                            FocusNPC(DirehornSpirits)
                        end
                    end
                    
                end
                 
                -- Council Of Elders
                
                    if Mode == "HeroicRaid" and (BossID == 69078 or BossID == 69131 or BossID == 69132 or BossID == 69134) then -- COE HM
                    
                    if UnitDebuffID("player",137641) then -- SoulFragment
                        if UnitDebuffID("player",137650) then -- Shadowed Soul
                            if WindWalker then SScap = 9 else SScap = 0 end
                            if select(4, UnitDebuffID("player",137650)) > SScap then
                                members = { { Unit = "player" } }
                                if IsInRaid() then
                                    group = "raid"
                                elseif IsInGroup() then
                                    group = "party"
                                end
                                for i = 1, GetNumGroupMembers() do
                                    table.insert( members,{ Unit = group..i } )
                                end
                                for i = 1,#members do
                                    if not UnitIsUnit(members[i].Unit,"player") and not UnitDebuffID(members[i].Unit,137641) then -- SoulFragment
                                        local TargetSSS = select(4, UnitDebuffID(members[i].Unit,137650)) or 0 -- Shadowed Soul Stacks on target
                                        local PlayerSSS = select(4, UnitDebuffID("player",137650)) or 0 -- Shadowed Soul Stacks on player
                                        if not UnitDebuffID(members[i].Unit,137650) or TargetSSS < PlayerSSS then -- Shadowed Soul or Shadowed Soul Stacks
                                            if UnitGroupRolesAssigned(members[i].Unit) ~= "TANK"
                                            and UnitGroupRolesAssigned(members[i].Unit) ~= "HEALER"
                                            and not UnitIsDeadOrGhost(members[i].Unit) then
                                                oexecute("RunMacroText('/targetexact '.."..members[i].Unit..")")
                                                oexecute("RunMacroText('/click ExtraActionButton1')")                        
                                                if lasttargetname == "none" then
                                                    oexecute("ClearTarget()")
                                                else
                                                    oexecute("RunMacroText('/targetexact '"..lasttargetname..")")
                                                end
                                                return true
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                    
                end
                
                -- Tortos
                
                if Mode == "HeroicRaid" and BossID == 67977 then -- Tortos HM
                    
                	local Absorb = UnitDebuffID("player",137633)
                	local MyHP = UnitHealth("player")*100/UnitHealthMax("player")
            		local HummingCrystals = {	"������� ��������", -- �������
							"[Humming Crystal]", -- Portugues Brasileiro
							"Cristal bourdonnant", -- Francais
							"Cristal zumbante", -- Espanol
							"Humming Crystal", -- English
							"Summender Kristall" -- Deutsch
						}
                    
                    if MyHP > 50 and not Absorb then -- if no crystall shell effect
                        if UnitExists("focus") and not UnitIsDeadOrGhost("focus") then
                            local FocusID = tonumber(UnitGUID("focus"):sub(6,10), 16)
                            if FocusID ~= 69639 or (FocusID == 69639 -- not a Humming Crystal or not in rage
                            and IsSpellInRange(GetSpellInfo(117952),"focus") ~= 1) then
                                oexecute("ClearFocus()")
                            else -- Humming Crystal in focus
                                if WindWalker and HaveGlyph(85698) then
                                    if IsSpellAvailable(123408) and UnitPower("player", 12) >= 1 then
                                        oexecute("CastSpellByName(GetSpellInfo(123408),'focus')")
                                    end
                                else
                                    if IsSpellAvailable(117952) then
                                        oexecute("CastSpellByName(GetSpellInfo(117952),'focus')")
                                    end
                                end
                            end
                        else -- focus is clear
                            FocusNPC(HummingCrystals)
                        end
                    end
                    
                end
                
                --========================================================================--
                --=================  Siege Of Orgrimmar Boss Events ======================--
                --========================================================================--
                
                -- The Fallen Protectors --
                
                -- Desperate Measures
                if BossID == 71475 or BossID == 71479 or BossID == 71480 then
                    if (WindWalker or (BrewMaster and MyHP < 20)) and UnitDebuffID("player",143840) then
                        
                        members = { { Unit = "player" } }
                        if IsInRaid() then
                            group = "raid"
                           elseif IsInGroup() then
                               group = "party"
                        end
                        
                        for i = 1, GetNumGroupMembers() do
                            table.insert( members,{ Unit = group..i } )
                        end
                        
                        for i = 1,#members do
                            if not UnitIsUnit(members[i].Unit,"player") 
                            and not UnitDebuffID(members[i].Unit,144176)
                            and not UnitIsDeadOrGhost(members[i].Unit) then
                                if (UnitGroupRolesAssigned(members[i].Unit) ~= "TANK" and UnitGroupRolesAssigned(members[i].Unit) ~= "HEALER") then
                                    oexecute("RunMacroText('/targetexact '"..members[i].Unit..")")
                                    oexecute("RunMacroText('/click ExtraActionButton1')")
                                    if lasttargetname == "none" then oexecute("ClearTarget()")
                                    else oexecute("RunMacroText('/targetexact '"..lasttargetname..")")
                                    end
                                    return true
                                end
                            end
                        end
                    end
                end
                
                -- General Nazgrim --
                
                    -- Defensive Stance, stop attack
                    if BossID == 71515 and UnitExists("target") and WindWalker then
                        if UnitBuffID("target",143593) then
                            oexecute("StopAttack()")
                               return true
                        end
                     end
                     
                -- Paragons of the Klaxxi --
                
                -- Rikkal the Dissector Hard Mode: Debuff "Mutate"
                if UnitDebuffID("player",143337) and UnitExists("target") then -- Mutate
                    
                    TargetID = tonumber(UnitGUID("target"):sub(6,10), 16)
                    
                    if TargetID == 71578 then -- Amber Parasite
                        -- Roth UI, Standart UI
                        oexecute("RunMacroText('/click OverrideActionBarButton4')") -- Hunt
                        -- Elv UI
                        oexecute("RunMacroText('/click ElvUI_Bar1Button4')") -- Hunt
                    end
                    
                    if not ReCastScorpTimer then ReCastScorpTimer = 0 end
                    if (GetTime() - ReCastScorpTimer) > 1.15 then
                        
                        -- Roth UI, Standart UI
                        oexecute("RunMacroText('/click OverrideActionBarButton3')") -- Sting
                        oexecute("RunMacroText('/click OverrideActionBarButton2')") -- Swipe
                        oexecute("RunMacroText('/click OverrideActionBarButton1')") -- Claw
                        
                        -- Elv UI
                        oexecute("RunMacroText('/click ElvUI_Bar1Button3')") -- Sting
                        oexecute("RunMacroText('/click ElvUI_Bar1Button2')") -- Swipe
                        oexecute("RunMacroText('/click ElvUI_Bar1Button1')") -- Claw
                        
                        ReCastScorpTimer = GetTime()
                        return true
                        
                    end
                    
                end
                
                -- Rikkal the Dissector All Modes: Buff Mad Scientist
                if UnitBuffID("player",141857) then -- Mad Scientist
                    if UnitBuffID("player",143373) then -- you are scorpion
                        if UnitExists("target") then
                            if not ReCastScorpTimer then ReCastScorpTimer = 0 end
                            if (GetTime() - ReCastScorpTimer) > 1.15 then
                                
                                -- Roth UI, Standart UI
                                oexecute("RunMacroText('/click OverrideActionBarButton3')") -- Sting
                                oexecute("RunMacroText('/click OverrideActionBarButton2')") -- Swipe
                                oexecute("RunMacroText('/click OverrideActionBarButton4')") -- Fiery Tail
                                oexecute("RunMacroText('/click OverrideActionBarButton1')") -- Claw
                                
                                -- Elv UI
                                oexecute("RunMacroText('/click ElvUI_Bar1Button3')") -- Sting
                                oexecute("RunMacroText('/click ElvUI_Bar1Button2')") -- Swipe
                                oexecute("RunMacroText('/click ElvUI_Bar1Button4')") -- Fiery Tail
                                oexecute("RunMacroText('/click ElvUI_Bar1Button1')") -- Claw
                                
                                ReCastScorpTimer = GetTime()
                                return true
                            end
                        end
                    else
                        if Mode ~= "HeroicRaid" then
                            if UnitExists("target") and not UnitBuffID("player",143373) then
                                if UnitHealth("target")*100/UnitHealthMax("target") > 20 then
                                    oexecute("RunMacroText('/click ExtraActionButton1')")
                                end
                            end
                        end
                    end
                end -- end of scorpion case
                
            end -- end of boss case
        
        end -- end of cycle
        
    end
    
    --===========================================================--
    --========================= SAVES ===========================--
    --===========================================================--
    
    MonkSaveAbilities = nil
    function MonkSaveAbilities()
        
        -- Resources
        
        MyHP = UnitHealth("player")*100/UnitHealthMax("player")
        MyChi = UnitPower("player", 12)
        MyNRG = UnitPower("player", 3)
        
        if IsSpellKnown(115396) then MyExtraChi = 1 else MyExtraChi = 0 end -- Ascention
        
        if UnitExists("boss1") then BossID = tonumber(UnitGUID("boss1"):sub(6,10), 16) else BossID = 0 end
        if UnitExists("target") then TargetID = tonumber(UnitGUID("target"):sub(6,10), 16) else TargetID = 0 end
        if UnitExists("focus") then FocusID = tonumber(UnitGUID("focus"):sub(6,10), 16) else FocusID = 0 end
        
        -- Buffs & Debuffs
        
        ElusiveBrew, _, _, EBs = UnitBuffID("player", 128939, "EXACT|PLAYER")
        ElusiveBrewBuff, _, _, _, _, _, EBTimer = UnitBuffID("player", 115308, "EXACT|PLAYER")
        if ElusiveBrewBuff then EBTimer = EBTimer - GetTime() else EBTimer = 0 end
        if not EBs then EBs = 0 end
        
        -- Nimble Brew
        
        if IsSpellAvailable(137562) and not NBBL() then
            local EventIndex = C_LossOfControl.GetNumEvents()
            if EventIndex == nil then EventIndex = 0 end
            while (EventIndex > 0) do
                local _, _, Text = C_LossOfControl.GetEventInfo(EventIndex)
                if Text == LOSS_OF_CONTROL_DISPLAY_STUN or Text == LOSS_OF_CONTROL_DISPLAY_FEAR or Text == LOSS_OF_CONTROL_DISPLAY_ROOT or Text == LOSS_OF_CONTROL_DISPLAY_HORROR then
                    oexecute("CastSpellByName(GetSpellInfo(137562))")
                end
                EventIndex = EventIndex - 1
            end
        end
        
    if BrewMaster then
        
        -- Purifying Brew 3
        
        local Purifier,_,_,_,_,_,PurifierTimer = UnitBuffID("player",138237)
        if Purifier then PurifierTimer = PurifierTimer - GetTime() else PurifierTimer = 0 end
        
        if IsSpellAvailable(119582) and ((UnitDebuffID("player",124273) and (MyChi >= 1 or Purifier))
        or (PurifierTimer > 0 and PurifierTimer < 3 and (UnitDebuffID("player",124274) or UnitDebuffID("player",124275))))
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
        oexecute("CastSpellByName(GetSpellInfo(119582))")
            return true
        end
        
    end -- end of Brewmaster case
        
        -- Fortifying Brew
        
        if VFQ[FBindex][5] == 1 then MONKATOWU("Buff", 125174, 0, FBindex) end
        
        if VFQ[FBindex][2] == 1 and IsSpellAvailable(115203) and MyHP < FBHP and not SHBL()
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Dzen Meditation
        and (GetItemCount(5512,false,true) == 0 or (GetItemCount(5512,false,true) > 0 and GetItemCooldown(5512) > 1.5)) then
            oexecute("CastSpellByName(GetSpellInfo(115203))")
            return true
        end
        
        -- HealthStone
        
        if GetItemCount(5512,false,true) > 0
        and MyHP < HSHP
        and IsSpellAvailable(5512) and not SHBL()
        and GetItemCooldown(5512) == 0 and UnitAffectingCombat("player")
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
        oexecute("UseItemByName(5512)")
            return true
        end
        
    if BrewMaster then
        
        -- Guard
        
        if HaveGlyph(123401) then GuardID = 123402 else GuardID = 115295 end
        if IsSpellAvailable(GuardID) and MyChi >= 2 and not SHBL() then
            if BossID == 71466 and AggroCheck("player", "target") and TargetID == BossID then -- Iron Juggernaut
                local IJFV = select(4, UnitDebuffID("player", 144464)) or 0 -- Flame Vents stacks
                if IJFV >= 1 or MyHP <= 50 then
                    oexecute("CastSpellByName(GetSpellInfo(123402))")
                    return true
                end
            elseif AggroCheck("player", "target") then
                if HaveGlyph(123401) then -- Glyph of Guard
                oexecute("CastSpellByName(GetSpellInfo(123402))")
                    return true
                else
                    if MyHP <= GHP then
                        oexecute("CastSpellByName(GetSpellInfo(123402))")
                        return true
                    end
                end
            end
        end
        
        -- Elusive Brew
        
        if not UnitAffectingCombat("player") or not RikkalInjectionCount then RikkalInjectionCount = 0 end
        if not UnitAffectingCombat("player") or not InjectionCounter then InjectionCounter = 0 end
        if UnitExists("focus") then if FocusID == 71158 and UnitIsDeadOrGhost("focus") then ClearFocus() end end
        
        if IsSpellAvailable(115308) then
            if ((AggroCheck("player","target") and TargetID == 71158) 
            or (AggroCheck("player","focus") and FocusID == 71158)) then -- tanking Rikkal the Dissector
                
                if UnitExists("target") then if FocusID ~= 71158 and AggroCheck("player","target") and TargetID == 71158 then FocusUnit("target") end end
                
                if Mode == "HeroicRaid" then 
                    
                    if TargetID == 71158 then RikkalHP = UnitHealth("target")*100/UnitHealthMax("target")
                    elseif FocusID == 71158 then RikkalHP = UnitHealth("focus")*100/UnitHealthMax("focus")
                    else RikkalHP = 0
                    end
                    
                    if UnitCastingInfo("target") == GetSpellInfo(143340) or UnitCastingInfo("focus") == GetSpellInfo(143340) then -- Injection
                        
                        if GetTime() - InjectionCounter > 5 then
                            RikkalInjectionCount = RikkalInjectionCount + 1
                            InjectionCounter = GetTime()
                        end
                        
                        if RikkalInjectionCount == 1
                        or RikkalInjectionCount == 7
                        or (RikkalInjectionCount == 14 and RikkalHP > 33) then
                            if HaveGlyph(123401) then -- Glyph of Guard
                                oexecute("RunMacroText('/cancelaura '"..GetSpellInfo(123402)..")")
                            else
                                oexecute("RunMacroText('/cancelaura '"..GetSpellInfo(115295)..")")
                            end
                            CancelBuffByID(115308) -- Elusive Brew
                            oexecute("RunMacroText('/cancelaura '"..GetSpellInfo(145051)..")") -- 2pT16
                        else
                            if not ElusiveBrewBuff then
                                oexecute("CastSpellByName(GetSpellInfo(115308))")
                                return true
                            end
                        end
                        
                    end
                    
                else
                    
                    if UnitCastingInfo("target") == GetSpellInfo(143339) or UnitCastingInfo("focus") == GetSpellInfo(143339) then -- Injection
                        
                        oexecute("CastSpellByName(GetSpellInfo(115308))")
                        return true
                        
                    end
                    
                end
                
            else
                if (AggroCheck("player","target") or AggroCheck("player","focus")) and EBs >= EBsfu 
                and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
                    oexecute("CastSpellByName(GetSpellInfo(115308))")
                    return true
                end
            end
        end
        
        -- Healing Elixirs
        
        if IsSpellAvailable(122280) then
            local CE = UnitBuffID("player", 134563)
            if IsSpellAvailable(119582) and MyHP <= HEHP and CE and (MyChi >= 1 or Purifier) and not SHBL()
            and UnitAffectingCombat("player") and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
                oexecute("CastSpellByName(GetSpellInfo(119582))")
                return true
            end
        end
        
        -- Expel Harm
        
        function KSsituation() -- Keg Smash
            if MonkValidTarget() then
                if GetSpellCD(121253) <= 1 and IsSpellInRange(GetSpellInfo(121253), "target") == 1 then
                    return true
                else
                    return false
                end
            else
                return false
            end
        end
        
        if IsSpellAvailable(115072) and MyNRG >= 40 and not SHBL() and not KSsituation()
        and UnitAffectingCombat("player") and (MyHP <= 35
        or (MyHP > 35 and MyHP < EHHP and ((UnitBuffID("player", 129914) and MyChi < (2 + MyExtraChi))
        or (not UnitBuffID("player", 129914) and MyChi <= (2 + MyExtraChi)))))
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
        oexecute("CastSpellByName(GetSpellInfo(115072))")
            return true
        end
        
    end -- end of Brewmaster case
        
        -- Zen Sphere
        
        if UnitAffectingCombat("player") and IsSpellKnown(124081)
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Zen Meditation
        and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
        and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
            if IsSpellAvailable(124081) then
                if not UnitBuffID("player", 124081, "EXACT|PLAYER") then
                    oexecute("CastSpellByName(GetSpellInfo(124081), 'player')")
                    return true
                elseif UnitExists("focus") and UnitBuffID("player", 124081, "EXACT|PLAYER") then
                    if not UnitBuffID("focus", 124081, "EXACT|PLAYER") and UnitIsFriend("player", "focus")
                    and not UnitIsDeadOrGhost("focus") and UnitIsPlayer("focus")
                    and IsSpellInRange(GetSpellInfo(124081), "focus") == 1 then
                        oexecute("CastSpellByName(GetSpellInfo(124081), 'focus')")
                        return true
                    end
                end
            end
        end
        
    end
    
    Buff = nil
    function MonkBuff()
        
        if  not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
        and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
        and not (UnitCastingInfo("player") == GetSpellInfo(115178)) -- Resuscitate
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Zen Meditation 
            
        -- Self detox
            
            if DMV[1][2] == 0 then
                
                if DispelValidation("player") and IsSpellAvailable(115450) then    
                    oexecute("CastSpellByName(GetSpellInfo(115450), 'player')")
                end
                
        -- Group detox
            
            elseif DMV[1][2] == 1 then
                
                members = { { Unit = "player" } }
                
                if IsInRaid() then 
                    group = "raid"
                elseif IsInGroup() then 
                    group = "party"
                end
                
                for i = 1, GetNumGroupMembers() do
                    table.insert( members,{ Unit = group..i } )
                end
                
                if IsSpellAvailable(115450) then
                    
                    for i=1, #members do
                    
                        if DispelValidation(members[i].Unit)
                        and IsSpellInRange(GetSpellInfo(115450), members[i].Unit) == 1
                        and not UnitIsEnemy(members[i].Unit, "player") then
                            oexecute("CastSpellByName(GetSpellInfo(115450),"..members[i].Unit..")")
                        end
                    end
                    
                end
            
            end -- end of dispell mode case
            
        -- Check Raid Buff
            
            if RBMV[1][2] == 0 then -- on Player
                
                if WindWalker then
                
            -- 5% Crit check
                    
                    if IsSpellAvailable(116781) and not GetRaidBuffTrayAuraInfo(7) then
                        oexecute("CastSpellByName(GetSpellInfo(116781),'player')")
                        return true
                    end
                    
                end -- end of Windwalker case
                
            -- 5% stats check
                
                if IsSpellAvailable(115921) and not GetRaidBuffTrayAuraInfo(1) then
                    oexecute("CastSpellByName(GetSpellInfo(115921),'player')")
                    return true
                end
                
            elseif RBMV[1][2] == 1 then -- on Raid/Party
                
                -- Initialization
                
                members = { { Unit = "player" } }
                
                if IsInRaid() then 
                    group = "raid"
                elseif IsInGroup() then 
                    group = "party"
                end
                
                for i = 1, GetNumGroupMembers() do
                    table.insert( members,{ Unit = group..i } )
                end
                
                if WindWalker then
                
            -- 5% Crit check
                
                if IsSpellAvailable(116781) then
                    
                    for i=1, #members do
                            if not UnitIsDeadOrGhost(members[i].Unit)
                            and IsSpellInRange(GetSpellInfo(116781), members[i].Unit) == 1 then
                                local BuffList    = {
                                        116781,        -- monk
                                        17007,        -- feral druid
                                        61316,        -- mage
                                        1459,        -- mage
                                        126309,     -- hunters pet
                                        126373,     -- hunters pet
                                        24604         -- hunters pet
                                       }
                                local b = 0
                                for j=1, #BuffList do
                                    if UnitBuffID(members[i].Unit, BuffList[j]) then b = 1 end
                                end
                                if b == 0 then oexecute("CastSpellByName(GetSpellInfo(116781),"..members[i].Unit..")") end
                            end
                    end
                    
                end
                
                end -- end of Windwalker case
                
            -- 5% stats check
                
                if IsSpellAvailable(115921) then
                    
                    for i=1, #members do
                            if not UnitIsDeadOrGhost(members[i].Unit)
                            and IsSpellInRange(GetSpellInfo(115921), members[i].Unit) == 1 then
                                    local BuffList    = { 
                                        117666,        -- monk
                                        1126,        -- druid
                                        20217,        -- paladin
                                        90363         -- hunters pet
                                        }
                                    local b = 0
                                    for j=1, #BuffList do
                                        if UnitBuffID(members[i].Unit, BuffList[j]) then b = 1 end
                                    end
                                    if b == 0 then oexecute("CastSpellByName(GetSpellInfo(115921),"..members[i].Unit..")") end
                            end
                    end
                    
                end
                
            end
            
        end
        
    end
    
    --====================================================================--
    --========================= TOGGLED SPELLS ===========================--
    --====================================================================--
    
    function MonkToggled_Spells()
        
    if WindWalker then
        
        -- Touch of Karma
        
        if VFQ[ToKindex][5] == 1 then MONKATOWU("Buff", 125174, 0, ToKindex) end
        
        if VFQ[ToKindex][2] == 1 and IsSpellAvailable(122470) and UnitExists("target")
        and IsSpellInRange(GetSpellInfo(122470), "target") == 1 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
        oexecute("CastSpellByName(GetSpellInfo(122470),'target')")
            return true
        end
        
        -- Storm, Earth and Fire
        
        local CurrentSEFCounterValue = select(4, UnitBuffID("player",137639)) or 0
        
        if VFQ[SEFindex][4] ~= CurrentSEFCounterValue then
            VFQ[SEFindex][2] = 0
            VFQ[SEFindex][4] = CurrentSEFCounterValue
            print(" ")
            print(string.format("%s has been |cffff0000excluded|r from the rotation.",GetSpellLink(VFQ[SEFindex][3])))
            return true
        elseif IsSpellAvailable(137639) and VFQ[SEFindex][2] == 1 and VFQ[SEFindex][4] == CurrentSEFCounterValue
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
            if UnitExists("target") then
                if UnitCanAttack("player", "target") then
                    oexecute("CastSpellByName(GetSpellInfo(137639),'target')")
                    return true
                end
            end
        end
        
    end -- end of Windwalker case
        
    if BrewMaster then
        
        -- Statue of Black Ox
        
        if VFQ[SoBOindex][5] == 1 then MONKATOWU("CD", 115315, 15, SoBOindex) end
        
        if VFQ[SoBOindex][2] == 1 and IsSpellAvailable(115315)
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
            oexecute("CastSpellByName(GetSpellInfo(115315))")
            if SpellIsTargeting() then
                oexecute("CameraOrSelectOrMoveStart()")
                oexecute("CameraOrSelectOrMoveStop()")
            end
            return true
        end
        
        -- Leer of the Ox
        
        if VFQ[LotOindex][5] == 1 then MONKATOWU("CD", 115543, 5, LotOindex) end
        
        if VFQ[LotOindex][2] == 1 and IsSpellAvailable(115543)
        and UnitExists("target") and UnitCanAttack("player", "target") then
            oexecute("CastSpellByName(GetSpellInfo(115543),'target')")
            return true
        end
        
        -- Dizzying Haze
        
        if VFQ[DHazeindex][2] == 1 and IsSpellAvailable(115180)
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
            oexecute("CastSpellByName(GetSpellInfo(115180))")
            if SpellIsTargeting() then
                oexecute("CameraOrSelectOrMoveStart()")
                oexecute("CameraOrSelectOrMoveStop()")
            end
            return true
        end
        
    end -- end of Brewmaster case
        
        -- Diffuse magic
        
        if VFQ[DiffMindex][5] == 1 then MONKATOWU("Buff", 122783, 0, DiffMindex) end
        
        if VFQ[DiffMindex][2] == 1 and IsSpellAvailable(122783) 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(122783))")
            return true
        end
        
        -- Dzen meditation
        
        if VFQ[DzenMindex][5] == 1 then MONKATOWU("Buff", 115176, 0, DzenMindex) end
        
        if VFQ[DzenMindex][2] == 1 and IsSpellAvailable(115176) then
            oexecute("RunMacroText('/StopAttack')")
            oexecute("RunMacroText('/ClearTarget')")
            oexecute("RunMacroText('/target @player')")
            oexecute("CastSpellByName(GetSpellInfo(115176))")
            return true
        end
        
        -- Transcendence
        
        if VFQ[Transindex][5] == 1 then MONKATOWU("CD", 101643, 30, Transindex) end
        
        if VFQ[Transindex][2] == 1 and IsSpellAvailable(101643) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(101643))")
            return true
        end
        
        -- Transcendence: Transfer
        
        if VFQ[TransTindex][5] == 1 then MONKATOWU("CD", 119996, 10, TransTindex) end
        
        if VFQ[TransTindex][2] == 1 and IsSpellAvailable(119996) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(119996))")
            return true
        end
        
        -- Tiger Rush
        
        if VFQ[TRindex][5] == 1 then MONKATOWU("Buff", 116841, 0, TRindex) end
        
        if VFQ[TRindex][2] == 1 and IsSpellAvailable(116841) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(116841),'player')")
            return true
        end
        
        -- Leg Sweep
        
        if VFQ[LSindex][5] == 1 then MONKATOWU("CD", 119381, 30, LSindex) end
        
        if VFQ[LSindex][2] == 1 and IsSpellAvailable(119381) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(119381))")
            return true
        end
        
        -- Charging Ox Wave
        
        if VFQ[COWindex][5] == 1 then MONKATOWU("CD", 119392, 15, COWindex) end
        
        if VFQ[COWindex][2] == 1 and IsSpellAvailable(119392) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(119392))")
            return true
        end
        
        -- Paralysis
        
        if VFQ[Pindex][5] == 1 then  MONKATOWU("CD", 115078, 7, Pindex) end
        
        if VFQ[Pindex][2] == 1 and IsSpellAvailable(115078) 
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Dzen Meditation 
        and UnitExists("target") and UnitCanAttack("player", "target") then
            oexecute("CastSpellByName(GetSpellInfo(115078),'target')")
            return true
        end
        
        -- Ring of Peace
        
        if VFQ[RoPindex][5] == 1 then MONKATOWU("Buff", 116844, 0, RoPindex) end
        if GetSpellCD(116844) < 0.1 then IcanRecastRoP = true else IcanRecastRoP = false end
        
        if VFQ[RoPindex][2] == 1 and IsPlayerSpell(116844) and IcanRecastRoP
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(116844),'player')")
            return true
        end
        
        -- Invoke Xuen, White Tiger
        
        if VFQ[IXindex][5] == 1 then MONKATOWU("CD", 123904, 150, IXindex) end
        
        if VFQ[IXindex][2] == 1 and IsSpellAvailable(123904) and MonkValidTarget()
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation 
            oexecute("CastSpellByName(GetSpellInfo(123904),'target')")
            return true
        end
        
        -- Healing Spheres
        
        if VFQ[HSindex][5] == 1 then MONKATOWU("HealingSpheres", 124458, 0, HSindex) end -- ID of Healig Spheres Buff! Not Spell ID!
        
        if VFQ[HSindex][2] == 1 and IsSpellAvailable(115460)
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury 
        and not (UnitChannelInfo("player") == GetSpellInfo(131523)) then -- Dzen Meditation
            oexecute("CastSpellByName(GetSpellInfo(115460))")
            if SpellIsTargeting() then 
                oexecute("CameraOrSelectOrMoveStart()")
                oexecute("CameraOrSelectOrMoveStop()")
            end 
            return true
        end
        
    end
    
BrMRotation = nil
function BrMRotation()
    
if MonkValidTarget() then
    
    -- Resourses
    
    MyChi = UnitPower("player", 12)
    MyNRG = UnitPower("player", 3)
    MyMaxNRG = UnitPowerMax("player")
    MyNRGregen = select(2, GetPowerRegen("player"))
    TimeToMaxNRG = (MyMaxNRG - MyNRG) / MyNRGregen
    NRGforRJW = MyNRG + (MyNRGregen * GetSpellCD(116847))
    NRGforKS = MyNRG + (MyNRGregen * GetSpellCD(121253))
    MyHP = UnitHealth("player")*100/UnitHealthMax("player")
    
    if IsSpellKnown(115396) then MyExtraChi = 1 else MyExtraChi = 0 end -- Ascention
    
    -- Buffs, Debuffs and Timers
    
    if GetShapeshiftForm() == 1 then SotSO = true else SotSO = false end -- Stance of the Sturdy Ox
    
    TP, _, _, _, _, _, TPTimer = UnitBuffID("player",125359) -- Tiger Palm Buff
    Shuffle, _, _, _, _, _, ShuffleTimer = UnitBuffID("player",115307) -- Blackout Kick Buff
    RJW, _, _, _, _, _, RJWTimer = UnitBuffID("player", 116847, "EXACT|PLAYER")
    BoF, _, _, _, _, _, BoFTimer = UnitDebuffID("target", 123725, "player") -- Breath of Fire Debuff
    DH = UnitDebuffID("target", 123727, "player") -- Dizzying Haze Debuff
    EBs = select(4, UnitBuffID("player", 128939, "EXACT|PLAYER")) or 0 -- Elusive Brew
    
    TBT1, TBT2 = CTT() -- Trinkets Buff Timers
    BLT = CBLT() -- Bludlust timer
    
    -- Kill nil value
    
    if Shuffle then ShuffleTimer = ShuffleTimer - GetTime() else ShuffleTimer = 0 end
    if RJW then RJWTimer = RJWTimer - GetTime() else RJWTimer = 0 end
    if BoF then BoFTimer = BoFTimer - GetTime() else BoFTimer = 0 end
    if TP then TPTimer = TPTimer - GetTime() else TPTimer = 0 end
    
    -- Touch of Death
    
    if IsSpellAvailable(115080) and MyChi >= 3 and not UnitIsPlayer("target")
    and UnitHealth("target") < UnitHealthMax("player") then
        oexecute("CastSpellByName(GetSpellInfo(115080),'target')")
        return true
    end
    
    -- Grapple Weapon
    
    if VFQ[GWindex][5] == 1 then MONKATOWU("CD", 117368, 30, GWindex) end
    
    if VFQ[GWindex][2] == 1 and IsSpellAvailable(117368) then
        oexecute("CastSpellByName(GetSpellInfo(117368),'target')")
        return true
    end
    
    -- Engeneering Gloves Chant
    
    local EGCcd,_,EGC = GetInventoryItemCooldown("player", 10)
    
    if EGC == 1 and EGCcd == 0
    and (GetSpellCD(123904) > 150 or TBT1 > 9 or TBT2 > 9) then
        oexecute("UseInventoryItem(10)")
        return true
    end
    
    -- Racial
    
    if IsSpellKnown(33697) then -- Orc
        if VFQ[RI][5] == 1 then MONKATOWU("CD", 33697, 60, 15) end
        if VFQ[RI][2] == 1 and IsSpellAvailable(33697)
        and (GetSpellCD(123904) > 150 or TBT1 > 9 or TBT2 > 9) then
            CastSpellByName(GetSpellInfo(33697),"player")
            return true
        end
    end
    if IsSpellKnown(26297) then -- Troll
        if VFQ[RI][5] == 1 then MONKATOWU("CD", 26297, 60, 15) end
        if VFQ[RI][2] == 1 and IsSpellAvailable(26297) and BLT == 0
        and (GetSpellCD(123904) > 145 or TBT1 > 9 or TBT2 > 9) then
            oexecute("CastSpellByName(GetSpellInfo(26297),'player')")
            return true
        end
    end
    if IsSpellKnown(107079) then -- Pandaren
        if VFQ[RI][5] == 1 then MONKATOWU("CD", 107079, 60, 15) end
        if VFQ[RI][2] == 1 and IsSpellAvailable(107079) then
            oexecute("CastSpellByName(GetSpellInfo(107079),'target')")
            return true
        end
    end
    
    -- Chi Brew
    
    if IsSpellKnown(115399) then
        if not ReCastCBTimer then ReCastCBTimer = 0 end
        if IsSpellAvailable(115399) and GetTime() - ReCastCBTimer > 1.5
        and MyChi <= (2 + MyExtraChi) and TimeToMaxNRG >= 2 and EBs < 5 then
            ReCastCBTimer = GetTime()
            oexecute("CastSpellByName(GetSpellInfo(115399))")
            return true
        end
    end
    
    -- Chi Burst
    
    if IsSpellKnown(123986) then
        if IsSpellAvailable(123986) and MyChi < 3 and MyNRG < 40 and TP then
            oexecute("CastSpellByName(GetSpellInfo(123986),'target')")
            return true
        end
    end
    
    -- Chi Wave
    
    if IsSpellKnown(115098) then
        if IsSpellAvailable(115098) and MyChi < 3 and MyNRG < 40 and TP
        and IsSpellInRange(GetSpellInfo(115098), "target") == 1 then
            oexecute("CastSpellByName(GetSpellInfo(115098),'target')")
            return true
        end
    end
    
    -- Tiger Palm
    
    if IsSpellAvailable(100787) and (not TP or ((TPTimer < 3 or (MyNRG < 40 and MyChi < 3))
    and (RV[1][2] == 0 or RV[1][2] == 1
    or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))))) then 
        oexecute("CastSpellByName(GetSpellInfo(100787),'target')")
        return true
    end
    
    -- Breath of Fire
    
    if IsSpellAvailable(115181) and MyChi >= 3 and SotSO and not BoF and DH
    and ((RV[1][2] == 0 and ShuffleTimer > 18)
    or (RV[1][2] == 2 and ShuffleTimer > 9
    and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
        oexecute("CastSpellByName(GetSpellInfo(115181),'target')")
        return true
    end
    
    -- Blackout Kick
    
    if IsSpellAvailable(100784) and MyChi >= 3 and (RV[1][2] == 1 or RV[1][2] == 0
    or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
        oexecute("CastSpellByName(GetSpellInfo(100784),'target')")
        return true
    end
    
    -- Keg Smash
    
    if IsSpellAvailable(121253) and SotSO and (MyChi < (3 + MyExtraChi) or TimeToMaxNRG < 2) then
        oexecute("CastSpellByName(GetSpellInfo(121253),'target')")
        return true
    end
    
    -- Rushing Jade Wind
    
    if not RJW and MyNRG >= 40 and RV[1][2] == 2 and (GetSpellCD(121253) > 1 or not SotSO) then
        oexecute("CastSpellByName(GetSpellInfo(116847))")
        return true
    end
    
    -- Spinning Crane Kick
    
    if IsSpellAvailable(101546) and MyNRG >= 40 and RV[1][2] == 0
    and (GetSpellCD(121253) > 1 or not SotSO) then
        oexecute("CastSpellByName(GetSpellInfo(101546))")
        return true
    end
    
    -- Jab
    
    if IsSpellAvailable(100780) and (MyChi < 3 or TimeToMaxNRG < 2)
    and ((RV[1][2] == 1 and ((NRGforKS >= 80 and SotSO) or (MyNRG >= 40 and not SotSO)))
    or (RV[1][2] == 2 and NRGforRJW >= 80 and (GetSpellCD(121253) > 1 or not SotSO))) then
        oexecute("CastSpellByName(GetSpellInfo(100780),'target')")
        return true
    end
    
end -- end of target validation case
    
end -- end of BrM rotation function

WWRotation = nil
function WWRotation()
    
    -- out of combat (special for Chi Burst)
    
    if not UnitAffectingCombat("player") then BeginningOfTheFight = true end
    
    -- target check (for Chi Burst and Expel Harm in combat use)
    
    if not TargetTime then TargetTime = 0 end
    if not TargetTimer then TargetTimer = 0 end
    if TargetTime ~= 0 then TargetTimer = GetTime() - TargetTime end
    
    if not RangeTime then RangeTime = 0 end
    if not RangeTimer then RangeTimer = 0 end
    if RangeTime ~= 0 then RangeTimer = GetTime() - RangeTime end
    
    if UnitExists("target") and UnitCanAttack("player", "target") then
        
        if IsSpellInRange(GetSpellInfo(100787), "target") ~= 1 then
            if not RangeCheck then
                RangeTime = GetTime()
                RangeCheck = true
            end
        else
            RangeCheck = false
            RangeTimer = 0
            RangeTime = 0
        end
        
        if IsSpellInRange(GetSpellInfo(100787), "target") == 1 then
            TargetCheck = false
            TargetTimer = 0
            TargetTime = 0
        end
        
    else
        
        if not TargetCheck then
            TargetTime = GetTime()
            TargetCheck = true
        end
        
    end
    
    if TargetTimer > 7 or RangeTimer > 7 then
        WasNothingToAttack = true
        BeginningOfTheFight = true
    else
        WasNothingToAttack = false
    end
    
    -- Expel Harm
    
    MyHP = UnitHealth("player")*100/UnitHealthMax("player")
    
    if IsSpellAvailable(115072) and InstanceDifficulty() ~= "OutOfInstance"
    and ((not UnitAffectingCombat("player") and MyHP == 100) or (UnitAffectingCombat("player") and WasNothingToAttack))
    and not (IsActiveTrinket(TickingEbonDetonator) and UnitAffectingCombat("player"))
    and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Dzen Meditation
    and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
    and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
    and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
        oexecute("CastSpellByName(GetSpellInfo(115072),'player')")
        return true
    end
    
if MonkValidTarget() then
    
    -- Resourses
    
    MyChi = UnitPower("player", 12)
    MyNRG = UnitPower("player", 3)
    MyMaxNRG = UnitPowerMax("player")
    MyNRGregen = select(2, GetPowerRegen("player"))
    TimeToMaxNRG = (MyMaxNRG - MyNRG) / MyNRGregen
    NRGforRSK = MyNRG + (MyNRGregen * GetSpellCD(107428))
    NRGforRJW = MyNRG + (MyNRGregen * GetSpellCD(116847))
    MyHP = UnitHealth("player")*100/UnitHealthMax("player")
    
    if VFQ[11][2] == 1 then ChiSoftCap = 3 else ChiSoftCap = 2 end
    if IsSpellKnown(115396) then MyExtraChi = 1 else MyExtraChi = 0 end -- Ascention
    
    if IsSpellInRange(GetSpellInfo(107428), "target") == 1 then
        MeleeRange = true
    else
        MeleeRange = false
    end
    
    Range1 = IsSpellInRange(GetSpellInfo(115078), "target") -- 20 yards
    Range2 = IsSpellInRange(GetSpellInfo(117952), "target") -- 40 yards
    Range3 = IsSpellInRange(GetSpellInfo(115073), "target") -- 50 yards
    
    TargetID = tonumber(UnitGUID("target"):sub(6,10), 16)
    
    -- Buffs, Debuffs and Timers
    
    TigereyeBrewBuff, _, _, _, _, _, TeBTimer = UnitBuffID("player", 116740, "EXACT|PLAYER")
    TBS = select(4, UnitBuffID("player",125195)) or 0 -- Tigereye Brew Stacks
    RJW, _, _, _, _, _, RJWTimer = UnitBuffID("player", 116847, "EXACT|PLAYER")
    RSK, _, _, _, _, _, RSKTimer = UnitDebuffID("target",130320,"player") -- Rising Sun Kick Debuff
    TP, _, _, _, _, _, TPTimer = UnitBuffID("player",125359) -- Tiger Palm Buff
    BoKCB = UnitBuffID("player",116768) -- Blackout Kick CB Proc
    TPCB = UnitBuffID("player", 118864) -- Tiger Palm CB Proc
    FocusOfXuen = UnitBuffID("player",145024) -- 4 T16 bonus
    FoFCastTime = (4 / (1 + UnitSpellHaste("Player") / 100))
    
    TBT1, TBT2 = CTT() -- Trinket Buff Timer's
    BLT = CBLT() -- Bludlust timer
    
    -- Kill nil value
    
    if TigereyeBrewBuff then TeBTimer = TeBTimer - GetTime() else TeBTimer = 0 end
    if RSK then RSKTimer = RSKTimer - GetTime() else RSKTimer = 0 end
    if RJW then RJWTimer = RJWTimer - GetTime() else RJWTimer = 0 end
    if TP then TPTimer = TPTimer - GetTime() else TPTimer = 0 end
    if FocusOfXuen then FreeChi = 1 else FreeChi = 0 end
    
    --==========================================================--
    --========================== Brew ==========================--
    --==========================================================--
    
    -- Chi Brew
    
    if IsSpellKnown(115399) then
        
        if not ReCastCBTimer then ReCastCBTimer = 0 end
        
        if IsSpellAvailable(115399) and MeleeRange
        and not (UnitChannelInfo("player") == GetSpellInfo(113656)) -- Fists Of Fury
        and (GetTime() - ReCastCBTimer) > 1.5
        and ((TeBTimer > 2 and TimeToMaxNRG >= 2 and MyChi <= 2) or (not TigereyeBrewBuff and TBS < 10 
        and (((TBT1 > GetSpellCD(107428) + 1.5 or TBT2 > GetSpellCD(107428) + 1.5) and TimeToMaxNRG >= 2 and MyChi <= 2)
        or (BeginningOfTheFight and MyChi <= 3)))) then
            oexecute("CastSpellByName(GetSpellInfo(115399))")
            ReCastCBTimer = GetTime()
            return true
        end
        
    end
    
    -- Tigereye Brew
    
    if IsSpellAvailable(116740) and MeleeRange and TBVFQ[1][2] == 1 then
        
        if UnitExists("boss1") then
            
            local BossID = tonumber(UnitGUID("boss1"):sub(6,10), 16)
             local BossHP =  UnitHealth("boss1")*100/UnitHealthMax("boss1")
            
            if (BossID == 69465    -- Jin'Rokh
            or BossID == 69712    -- Ji-Kun
            or BossID == 69017)    -- Primordius
            and BossHP >= 10 then
                
                if BossID == 69465 then -- Jin'Rokh
                    
                    if not TigereyeBrewBuff and TBS >= 17 and GetSpellCD(107428) < 1 then
                        oexecute("CastSpellByName(GetSpellInfo(116740))")
                        BeginningOfTheFight = false
                    elseif UnitDebuffID("player", 138002) then -- Fluidity
                        if not TigereyeBrewBuff and TBS > 0 and GetSpellCD(107428) < 1 then
                            oexecute("CastSpellByName(GetSpellInfo(116740))")
                            BeginningOfTheFight = false
                        end
                    end
                    
                elseif BossID == 69712 then -- Ji-Kun
                    
                    if not TigereyeBrewBuff and TBS >= 17 and GetSpellCD(107428) < 1 then 
                        oexecute("CastSpellByName(GetSpellInfo(116740))")
                        BeginningOfTheFight = false
                    elseif UnitBuffID("player", 140741) then -- Feed
                        if not TigereyeBrewBuff and TBS > 0 and GetSpellCD(107428) < 1 then
                            oexecute("CastSpellByName(GetSpellInfo(116740))")
                            BeginningOfTheFight = false
                        end
                    end
                        
                elseif BossID == 69017 then -- Primordius
                    
                    if not TigereyeBrewBuff and TBS >= 17 and GetSpellCD(107428) < 1 then
                        oexecute("CastSpellByName(GetSpellInfo(116740))")
                        BeginningOfTheFight = false
                    else
                        if UnitDebuffID("player", 140546) then -- Full mutation
                            if not TigereyeBrewBuff and TBS > 0 and GetSpellCD(107428) < 1 then
                                oexecute("CastSpellByName(GetSpellInfo(116740))")
                                BeginningOfTheFight = false
                            end
                        end
                    end
                    
                end
                
            else -- not special or BossHP < 10%
                
                if not TigereyeBrewBuff and TBS > 0 
                and (RV[1][2] ~= 1 or (RV[1][2] == 1 and GetSpellCD(107428) < 1)) then
                    oexecute("CastSpellByName(GetSpellInfo(116740))")
                    BeginningOfTheFight = false
                end
                
            end
            
        else -- not a raid boss fight
            
            if not TigereyeBrewBuff and TBS > 0
            and (RV[1][2] ~= 1 or (RV[1][2] == 1 and GetSpellCD(107428) < 1)) then
                oexecute("CastSpellByName(GetSpellInfo(116740))")
                BeginningOfTheFight = false
            end
            
        end
        
    end
    
    --==================================================================--
    --======================== Single Rotation =========================--
    --==================================================================--
    
    if (RV[1][2] == 1 or RV[1][2] == 2) and MeleeRange then -- Single or Cleave
        
        -- Touch of Death
        
        if IsSpellAvailable(115080) and MyChi >= 3 and not UnitIsPlayer("target")
        and UnitHealth("target") < UnitHealthMax("player") then
            oexecute("CastSpellByName(GetSpellInfo(115080),'target')")
            return true
        end
        
        -- Disable
        
        if VFQ[Dindex][5] == 1 and UnitDebuffID("target", 116095) then
            VFQ[Dindex][2] = 0
            VFQ[Dindex][5] = 0
            print(" ")
            print(string.format("%s has been |cffff0000excluded|r from the rotation.",GetSpellLink(VFQ[Dindex][3])))
        end
        
        if VFQ[Dindex][2] == 1 and IsSpellAvailable(116095)
        and MyNRG >= 15 and not UnitDebuffID("target", 116095) then
            oexecute("CastSpellByName(GetSpellInfo(116095),'target')")
            return true
        end
        
        -- Grapple Weapon
        
        if VFQ[GWindex][5] == 1 then MONKATOWU("CD", 117368, 30, GWindex) end
        
        if VFQ[GWindex][2] == 1 and IsSpellAvailable(117368) then
            oexecute("CastSpellByName(GetSpellInfo(117368),'target')")
            return true
        end
        
        -- Engeneering Gloves Chant
        
        local EGCcd,_,EGC = GetInventoryItemCooldown("player", 10)
        
        if EGC == 1 and EGCcd == 0
        and (TeBTimer > 14 or GetSpellCD(123904) > 150 or TBT1 > 9 or TBT2 > 9) then
            oexecute("UseInventoryItem(10)")
            return true
        end
        
        -- Racial
        
        if IsSpellKnown(33697) then -- Orc
            if VFQ[RI][5] == 1 then MONKATOWU("CD", 33697, 60, 18) end
            if VFQ[RI][2] == 1 and IsSpellAvailable(33697)
            and (TeBTimer > 14 or GetSpellCD(123904) > 150 or TBT1 > 9 or TBT2 > 9) then
                oexecute("CastSpellByName(GetSpellInfo(33697),'player')")
                return true
            end
        end
        if IsSpellKnown(26297) then -- Troll
            if VFQ[RI][5] == 1 then MONKATOWU("CD", 26297, 60, 18) end
            if VFQ[RI][2] == 1 and IsSpellAvailable(26297) and BLT == 0
            and (TeBTimer > 10 or GetSpellCD(123904) > 145 or TBT1 > 9 or TBT2 > 9) then
                oexecute("CastSpellByName(GetSpellInfo(26297),'player')")
                return true
            end
        end
        if IsSpellKnown(107079) then -- Pandaren
            if VFQ[RI][5] == 1 then MONKATOWU("CD", 107079, 60, 18) end
            if VFQ[RI][2] == 1 and IsSpellAvailable(107079) then
                oexecute("CastSpellByName(GetSpellInfo(107079),'target')")
                return true
            end
        end
        
        -- Energizing Brew
        
        if IsSpellAvailable(115288)
        and ((RV[1][2] == 1 and TimeToMaxNRG > 5) or (RV[1][2] == 2 and NRGforRJW < 40)) then
            oexecute("CastSpellByName(GetSpellInfo(115288),'player')")
            return true
        end
	
        -- Rushing Jade Wind
        
        if RV[1][2] == 2 and MyNRG >= 40 then
            if not RJW and MyChi <= (3 + MyExtraChi) then
                oexecute("CastSpellByName(GetSpellInfo(116847))")
                return true
            end
        end
	        
        -- Tiger Palm buff
        
        if IsSpellAvailable(100787) and (MyChi >= 1 or TPCB) and (not TP or (TPTimer < 3 and GetSpellCD(107428) > 1)) then
            oexecute("CastSpellByName(GetSpellInfo(100787),'target')")
            return true
        end
        
        -- Rising Sun Kick
        
        if IsSpellAvailable(107428) and (MyChi + FreeChi) >= 2 then
            oexecute("CastSpellByName(GetSpellInfo(107428),'target')")
            return true
        end
        
        -- Fists Of Fury
        
        if VFQ[FoFindex][5] == 1 then MONKATOWU("CD", 113656, 5, FoFindex) end
        
        if VFQ[FoFindex][2] == 1 and IsSpellAvailable(113656) and (MyChi + FreeChi) >= 3 and not IsMoving()
        and ((not UnitBuffID("player", 115288) and TimeToMaxNRG > (FoFCastTime + 1)
        and TPTimer > (FoFCastTime + 1) and GetSpellCD(107428) > (FoFCastTime + 1)
        and (RV[1][2] == 1 or (RV[1][2] == 2 and RJWTimer > FoFCastTime))) or FOFVFQ[1][2] == 1) then
            oexecute("CastSpellByName(GetSpellInfo(113656),'target')")
            return true
        end
        
        -- Blackout Kick Chi cap
        
        if IsSpellAvailable(100784) and (MyChi + FreeChi) >= (3 + MyExtraChi) and GetSpellCD(107428) > 0.75 
        and (RV[1][2] == 1
        or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
            oexecute("CastSpellByName(GetSpellInfo(100784),'target')")
            return true
        end
        
        -- Chi Wave
        
        if IsSpellKnown(115098) then
            if IsSpellAvailable(115098) and MyChi < (3 + MyExtraChi) and TimeToMaxNRG >= 2
            and (RV[1][2] == 1 or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40))))
            and IsSpellInRange(GetSpellInfo(115098), "target") == 1 and GetSpellCD(107428) > 1 then
                oexecute("CastSpellByName(GetSpellInfo(115098),'target')")
                return true
            end
        end
        
        -- Chi Burst
        
        if IsSpellKnown(123986) then
            if IsSpellAvailable(123986) and MyChi < (3 + MyExtraChi) and TimeToMaxNRG >= 2
            and GetSpellCD(107428) > 1 and (RV[1][2] == 1
            or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
                oexecute("CastSpellByName(GetSpellInfo(123986),'target')")
                return true
            end
        end
        
        -- Blackout Kick CB Proc!
        
        if IsSpellAvailable(100784) and BoKCB    and (RV[1][2] == 1
        or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
            oexecute("CastSpellByName(GetSpellInfo(100784),'target')")
            return true
        end
        
        -- Blackout Kick
        
        if IsSpellAvailable(100784) and MyChi >= (ChiSoftCap - FreeChi) and NRGforRSK >= 40
        and GetSpellCD(107428) > 1 and (RV[1][2] == 1
        or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
            oexecute("CastSpellByName(GetSpellInfo(100784),'target')")
            return true
        end
        
        -- Tiger Palm CB Proc!
        
        if IsSpellAvailable(100787) and TPCB and (RV[1][2] == 1
        or (RV[1][2] == 2 and (RJWTimer >= 0.7 or (RJWTimer < 0.7 and NRGforRJW < 40)))) then
            oexecute("CastSpellByName(GetSpellInfo(100787),'target')")
            return true
        end
        
        -- Expel Harm
        
        if IsSpellAvailable(115072) and MyChi < (3 + MyExtraChi) and MyHP <= EHHP and not SHBL()
        and (RV[1][2] == 1 or (RV[1][2] == 2 and (RJWTimer > 3 or NRGforRJW >= 80))) then
            oexecute("CastSpellByName(GetSpellInfo(115072),'player')")
            return true
        end
        
        -- Jab
        
        if IsSpellAvailable(100780) and ((MyChi + FreeChi) < ChiSoftCap or TimeToMaxNRG < 2)
        and (RV[1][2] == 1 or (RV[1][2] == 2 and (RJWTimer > 3 or NRGforRJW >= 80))) then
            oexecute("CastSpellByName(GetSpellInfo(100780),'target')")
            return true
        end
        
    --==================================================================--
    --========================== AOE Rotation ==========================--
    --==================================================================--
    
    elseif RV[1][2] == 0 and MeleeRange then
        
        -- Touch of Death
        
        if IsSpellAvailable(115080) and MyChi >= 3 and not UnitIsPlayer("target")
        and UnitHealth("target") < UnitHealthMax("player") then 
            oexecute("CastSpellByName(GetSpellInfo(115080),'target')")
            return true
        end
        
        -- Check Tiger Palm Buff
        
        if IsSpellAvailable(100787) and MyChi >= 1 and (not TP or TPTimer < 3) then
            oexecute("CastSpellByName(GetSpellInfo(100787),'target')")
            return true
        end
        
        -- Check RSK Debuff
        
        if IsSpellAvailable(107428) and MyChi >= 2 and not RSK then 
            oexecute("CastSpellByName(GetSpellInfo(107428),'target')")
            return true
        end
        
        -- Engeneering Gloves Chant
        
        local EGCcd,_,EGC = GetInventoryItemCooldown("player", 10)
        
        if EGC == 1 and EGCcd == 0 then
            oexecute("UseInventoryItem(10)")
            return true
        end
        
        -- Spinning Crane Kick
        
        if IsSpellAvailable(101546) then
            oexecute("CastSpellByName(GetSpellInfo(101546),'target')")
            return true
        end
        
        -- Chi Burst
        
        if IsSpellKnown(123986) then
            if IsSpellAvailable(123986) and MyNRG < 40 then
                oexecute("CastSpellByName(GetSpellInfo(123986),'target')")
                return true
            end
        end
        
        -- Chi Wave
        
        if IsSpellKnown(115098) then
            if IsSpellAvailable(115098) and MyNRG < 40 then
                oexecute("CastSpellByName(GetSpellInfo(115098),'target')")
                return true
            end
        end
        
    end -- end of rotation

end -- end of target validation

end -- end of WW rotation function

print_Log = nil
function print_Log(text)
    text = text or " "
    if text ~= lasttext then lasttext = text print(text) end
end

function WWFMONKWWBRMROTATIONSETUP()
	
	if select(2, UnitClass("player")) ~= "MONK" then
		print_Log("Error: wrong class!")
		return true
	else
		if UnitLevel("player") < 90 then
			print_Log("Error: Low level!")
			return true
		else
			if GetSpecialization() == nil then
				print_Log("Error: wrong spec - none selected!")
				return true
			elseif GetSpecialization() == 1 then
				WindWalker = false
				BrewMaster = true
			elseif GetSpecialization() == 2 then
				print_Log("Error: Mistweaver spec is not supported!")
				return true
			elseif GetSpecialization() == 3 then
				WindWalker = true
				BrewMaster = false
			end
		end
	end
	
    --=========================--
    --== Windwalker Settings ==--
    --=========================--
    
    if WindWalker then
    
    Spec = "WW"
    
    -- 1) Localized Spell Name
    -- 2) Spell queue values: 1 - spell in queue, 0 - spell not in queue
    -- 3) Spell ID
    -- 4) MONKATOWU check values, all should be 0 or false by default
    -- 5) MONKATOWU value: 0 - cast a spell until it is canceled, 1 - cast a spell one time
    
    VFQ = nil
    VFQ = { {GetSpellInfo(122470), 0, 122470, false, 0}, -- [1] Touch of karma
        {GetSpellInfo(122783), 0, 122783, false, 0}, -- [2] Diffuse magic
        {GetSpellInfo(115176), 0, 115176, false, 0}, -- [3] Dzen meditation
        {GetSpellInfo(101643), 0, 101643, false, 0}, -- [4] Transcendence
        {GetSpellInfo(119996), 0, 119996, false, 0}, -- [5] Transcendence: Transfer
        {GetSpellInfo(116841), 0, 116841, false, 0}, -- [6] Tiger Rush
        {GetSpellInfo(119381), 0, 119381, false, 0}, -- [7] Leg Sweep
        {GetSpellInfo(119392), 0, 119392, false, 0}, -- [8] Charging Ox Wave
        {GetSpellInfo(115078), 0, 115078, false, 0}, -- [9] Paralysis
        {GetSpellInfo(116844), 0, 116844, false, 0}, -- [10] Ring of Peace
        {GetSpellInfo(113656), 0, 113656, false, 0}, -- [11] Fists of fury
        {GetSpellInfo(123904), 0, 123904, false, 0}, -- [12] Invoke Xuen
        {GetSpellInfo(116095), 0, 116095, false, 0}, -- [13] Disable
        {GetSpellInfo(117368), 0, 117368, false, 0}, -- [14] Grapple Weapon
        {GetSpellInfo(115203), 1, 115203, false, 0}, -- [15] Fortifying Brew
        {GetSpellInfo(115460), 0, 115460,     0, 0}, -- [16] Healing Spheres
        {GetSpellInfo(137639), 0, 137639,     0, 0}, -- [17] Storm, Earth and Fire
           } -- [18] Racials
    
    -- values for use:
    FBHP        =    25    -- Fortifying Brew
    HSHP        =    40    -- HealthStone
    EHHP        =    80    -- Expel Harm
    
    -- indexes:
    ToKindex    =    1    -- Touch of Karma
    DiffMindex  =    2    -- Diffuse magic
    DzenMindex  =    3    -- Dzen meditation
    Transindex  =    4    -- Transcendence
    TransTindex =    5    -- Transcendence: Transfer
    TRindex     =    6    -- Tiger Rush
    LSindex     =    7    -- Leg Sweep
    COWindex    =    8    -- Charging Ox Wave
    Pindex      =    9    -- Paralysis
    RoPindex    =    10   -- Ring of Peace
    FoFindex    =    11   -- Fists of fury
    IXindex     =    12   -- Invoke Xuen, White Tiger
    Dindex      =    13   -- Disable
    GWindex     =    14   -- Grapple Weapon
    FBindex     =    15   -- Fortifying Brew
    HSindex     =    16   -- Healing Spheres
    SEFindex    =    17   -- Storm, Earth and Fire
    RI          =    18   -- Racials
    
    SLASH_TIGEREYEBREWTOGGLE1 = "/TigereyeBrewToggle"
    function SlashCmdList.TIGEREYEBREWTOGGLE()
        if TBVFQ[1][2] == 1 then TBVFQ[1][2] = 0 else TBVFQ[1][2] = 1 end
        print(" ")
        print(string.format("%s has been %s.", GetSpellLink(GetSpellInfo(116740)), TBVFQ[1][2] == 1 and "|cff00ff00activated|r" or "|cffff0000deactivated|r"))        
    end
    
    SLASH_CASTFISTSOFFURY1 = "/castfistsoffury"
    function SlashCmdList.CASTFISTSOFFURY(msg)
        local CastType, Command = msg:match("^(%S*)%s*(.-)$")
        if CastType == "" then
            if VFQ[11][2] == 1 then VFQ[11][2] = 0 VFQ[11][5] = 0 FOFVFQ[1][2] = 0 else VFQ[11][2] = 1 VFQ[11][5] = 1 FOFVFQ[1][2] = 1 end
            print(" ")
            print(string.format("%s has been %s.", GetSpellLink(GetSpellInfo(113656)), VFQ[11][2] == 1 and "|cff00ff00included|r into rotation with high priority for one use" or "|cffff0000excluded|r from rotation"))
        elseif CastType == "CD" then
            if VFQ[11][2] == 1 then VFQ[11][2] = 0 VFQ[11][5] = 0 FOFVFQ[1][2] = 0 else VFQ[11][2] = 1 VFQ[11][5] = 0 FOFVFQ[1][2] = 1 end
            print(" ")
            print(string.format("%s has been %s.", GetSpellLink(GetSpellInfo(113656)), VFQ[11][2] == 1 and "|cff00ff00included|r into rotation with high priority for unlimited use" or "|cffff0000excluded|r from rotation"))
        end
    end
    
    -- BrM inactive commands:
    SLASH_AUTOTAUNT1 = "/AutoTauntToggle"
    function SlashCmdList.AUTOTAUNT()
    end
    
    end -- end of WW setttings
    
    --=========================--
    --== Brewmaster Settings ==--
    --=========================--
    
    if BrewMaster then
    
    Spec = "BrM"
    
    -- 1) Localized Spell Name
    -- 2) Spell queue values: 1 - spell in queue, 0 - spell not in queue
    -- 3) Spell ID
    -- 4) MONKATOWU check values, all should be 0 or false by default
    -- 5) MONKATOWU value: 0 - cast a spell until it is canceled, 1 - cast a spell one time
    
    VFQ = nil
    VFQ =  {{GetSpellInfo(122783), 0, 122783, false, 0}, -- [1] Diffuse magic
            {GetSpellInfo(115176), 0, 115176, false, 0}, -- [2] Dzen meditation
            {GetSpellInfo(101643), 0, 101643, false, 0}, -- [3] Transcendence
            {GetSpellInfo(119996), 0, 119996, false, 0}, -- [4] Transcendence: Transfer
            {GetSpellInfo(116841), 0, 116841, false, 0}, -- [5] Tiger Rush
            {GetSpellInfo(119381), 0, 119381, false, 0}, -- [6] Leg Sweep
            {GetSpellInfo(119392), 0, 119392, false, 0}, -- [7] Charging Ox Wave
            {GetSpellInfo(115078), 0, 115078, false, 0}, -- [8] Paralysis
            {GetSpellInfo(116844), 0, 116844, false, 0}, -- [9] Ring of Peace
            {GetSpellInfo(115315), 0, 115315, false, 0}, -- [10] Statue of Black Ox
            {GetSpellInfo(123904), 0, 123904, false, 0}, -- [11] Invoke Xuen
            {GetSpellInfo(117368), 0, 117368, false, 0}, -- [12] Grapple Weapon
            {GetSpellInfo(115203), 1, 115203, false, 0}, -- [13] Fortifying Brew
            {GetSpellInfo(115543), 0, 115543, false, 0}, -- [14] Leer of the Ox
            {GetSpellInfo(115180), 0, 115180, false, 0}, -- [15] Dizzying Haze
            {GetSpellInfo(115460), 0, 115460,     0, 0}, -- [16] Healing Spheres
            } -- [17] Racials
    
    -- values for use:
    FBHP          =    30   -- Fortifying Brew
    HSHP          =    40   -- HealthStone
    GHP           =    70   -- Guard
    HEHP          =    80   -- Healing Elixirs
    EHHP          =    90   -- Expel Harm
    
    EBsfu         =    5    -- Elusive Brew stacks
    
    -- indexes:
    DiffMindex    =    1    -- Diffuse magic
    DzenMindex    =    2    -- Dzen meditation
    Transindex    =    3    -- Transcendence
    TransTindex   =    4    -- Transcendence: Transfer
    TRindex       =    5    -- Tiger Rush
    LSindex       =    6    -- Leg Sweep
    COWindex      =    7    -- Charging Ox Wave
    Pindex        =    8    -- Paralysis
    RoPindex      =    9    -- Ring of Peace
    SoBOindex     =    10   -- Statue of Black Ox
    IXindex       =    11   -- Invoke Xuen, White Tiger
    GWindex       =    12   -- Grapple Weapon
    FBindex       =    13   -- Fortifying Brew
    LotOindex     =    14   -- Leer of the Ox
    DHazeindex    =    15   -- Dizzying Haze
    HSindex       =    16   -- Healing Spheres
    RI            =    17   -- Racials
    
    -- WW inactive commands:
    SLASH_TIGEREYEBREWTOGGLE1 = "/TigereyeToggle"
    function SlashCmdList.TIGEREYEBREWTOGGLE()
    end
    SLASH_CASTFISTSOFFURY1 = "/castfistsoffury"
    function SlashCmdList.CASTFISTSOFFURY()
    end
    SLASH_RANGEDROTATIONTOGGLE1 = "/RangedRotationToggle"
    function SlashCmdList.RANGEDROTATIONTOGGLE()
    end
    
    end -- end of BrM setttings
    
    --=====================--
    --== Common Settings ==--
    --=====================--
    
    -- Racials.
    if IsSpellKnown(33697) then VFQ[RI]= {} VFQ[RI][1] = GetSpellInfo(33697) VFQ[RI][2] = 0 VFQ[RI][3] = 33697 VFQ[RI][4] = false VFQ[RI][5] = 0 end -- Orc Racial
    if IsSpellKnown(26297) then VFQ[RI]= {} VFQ[RI][1] = GetSpellInfo(26297) VFQ[RI][2] = 0 VFQ[RI][3] = 26297 VFQ[RI][4] = false VFQ[RI][5] = 0 end -- Troll Racial
    if IsSpellKnown(107079) then VFQ[RI]= {} VFQ[RI][1] = GetSpellInfo(107079) VFQ[RI][2] = 0 VFQ[RI][3] = 107079 VFQ[RI][4] = false VFQ[RI][5] = 0 end -- Pandaren Racial
    
    -- Single Rotation Activated by default.
    RV = nil
    RV = { { "Rotation type", 1 } } -- 0 - AoE, 1 - Single, 2 - Cleave
    
    -- Buff Mode: 0 - self, 1 - group/raid.
    RBMV = nil
    RBMV = { { "Buff Mode", 0 } }
    
    -- Dispell Mode: 0 - self, 1 - group/raid.
    DMV = nil
    DMV = { { "Dispell Mode", 0 } }
    
    -- Highest priority FoF.
    FOFVFQ = nil
    FOFVFQ = { { "FoF", 0 } }    -- 1 - ON, 0 - OFF
    
    -- Tigereye Brew Toggle.
    TBVFQ = nil
    TBVFQ = { { "TB", 1 } }        -- 1 - ON, 0 - OFF
    
    TickingEbonDetonator = {
                105114,    -- 528 Raid Finder
                104865,    -- 540 Flexible
                102311,    -- 553 Normal
                105363,    -- 559 Normal Warforged
                104616,    -- 566 Heroic
                105612    -- 572 Heroic Warforged
                }
    
    Food = {
        148984,
        144961,    -- 300 main stat
        104935,    -- 300 agi / 450 stamina
        104922,    -- 275 main stat
        104934,    -- 275 agi / 415 stamina
        126535,    -- 250 main stat
        104235,    -- 250 agi / 375 stamina
        126502,    -- Great Banquet of the Oven
        126501,    -- Banquet of the Oven
        126496,    -- Great Banquet of the Wok
        126495,    -- Banquet of the Wok
        }
    
    Sleep_Time = 100
    Last_Activity_Time = 0
    
    LastMonkSpec = GetSpecialization()
    
    UIErrorsFrame:UnregisterEvent("UI_ERROR_MESSAGE")
    
    if GetShapeshiftForm("player") ~= 1 then oexecute("CastShapeshiftForm(1)") end
    
    if RV[1][2] == 1 then
	print(" ")
        print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." Single Rotation Activated|R")
    elseif RV[1][2] == 0 then
	print(" ")
        print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." AoE Rotation Activated|R")
    elseif RV[1][2] == 2 then
	print(" ")
        print("|CFF1CB619Rotation|R : |CFFFE8A0E"..Spec.." Cleave Rotation Activated|R")
    end
    
    WWFMONKWWBRMROTATIONPRESETS = true
    
end

function WWFMONKWWBRMOSROTATION()
    
    if not WWFMONKWWBRMROTATIONPRESETS then WWFMONKWWBRMROTATIONSETUP() end
    
    if not UnitIsDeadOrGhost("player") then
    
        if MonkTrackingFunc() then return end
        if Pause() then return end
        
      if Anti_Spam() then
        
        if MonkBossEvents() then return end
        if MonkSaveAbilities() then return end
        if MonkBuff() then return end
        if MonkToggled_Spells() then return end
        if BrewMaster then
            BrMRotation()
        elseif WindWalker then
            WWRotation()
        end
        
      end
      
    end
    
end