addonName, Rotations = ...
Rotations.monk = {}
Rotations.monk.ROTATION = function()

local playerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local energy = UnitPower("player", 3)
local chi = UnitPower("player", 12)
local spec = GetSpecialization() -- 1 Brew, 2 Weaver, 3 Walker

local hacer = oexecute -- clone a oexecute with new name
local oexecute = nil -- oexecute unregistered and nil
local mirar = oface -- clone a oexecute with new name
local oface = nil -- oexecute unregistered and nil
local enemigos = oenemy
local oenemy = nil

	if UnitExists("target") and not UnitIsDeadOrGhost("target")
        and UnitCanAttack("player","target") --and UnitAffectingCombat("player")
		and not UnitHasVehicleUI("player")
		and not IsMounted()
        --and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Zen Meditation
       -- and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
       -- and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
       -- and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
		then

		mirar("target")
		if spec == 1 then -- Maestro cervecero

			-- Expulsar dano
			-- Brebaje reconstituyente
			-- Brebaje esquivo
			-- Patada oscura
			-- Esfera zen

			-- Burla
			-- Estupor mareante

			-- Embate con barril
			-- Palma de tigre
			-- Patada oscura
			-- Atizar


			-- Defensa
			if GetSpellCooldown("Expulsar daño") == 0
			and energy > 70
			and playerHP < 100
			then
			hacer("CastSpellByName('Expulsar daño','player')")
			print("Expulsar daño")

			elseif GetSpellCooldown("Brebaje reconstituyente") == 0
			and playerHP < 50
			then
			hacer("CastSpellByName('Brebaje reconstituyente','player')")
			print("Brebaje reconstituyente")

			elseif GetSpellCooldown("Brebaje esquivo") == 0
			and select(4, UnitBuff("player","Brebaje esquivo")) ~= nil
			and select(4, UnitBuff("player","Brebaje esquivo")) >= 6
			then
			hacer("CastSpellByName('Brebaje esquivo','player')")
			print("Brebaje esquivo")

			elseif chi >= 2 then
			hacer("CastSpellByName('Patada oscura','target')")
			print("Patada oscura")

			elseif GetSpellCooldown("Esfera zen") == 0
			and playerHP < 90
			and not UnitBuff("player","Esfera zen")
			then
			hacer("CastSpellByName('Esfera zen','player')")
			print("Esfera zen")

			-- Tank
			elseif UnitThreatSituation("player","target") ~= 3
			and GetSpellCooldown("Burla") == 0 then
			mirar("target")
			hacer("CastSpellByName('Burla')")
			print("Burla")

			elseif UnitThreatSituation("player","target") ~= 3
			and GetSpellCooldown("Burla") ~= 0
			and not UnitDebuff("target","Estupor mareante") then
			mirar("target")
			hacer("CastSpellByName('Estupor mareante')")
			oclick("target")
			print("Estupor mareante")

			-- Ataque
			-- Separar para < 3 enemigos y 3 o mas enemigos
			elseif GetSpellCooldown("Embate con barril") == 0
			and chi <= 2
			then
			hacer("CastSpellByName('Embate con barril','target')")
			print("Embate con barril")

			elseif chi >=2
			and enemigos("player") >= 3
			and UnitBuff("player","Guardia")
			and not UnitDebuff("target","Aliento de fuego")
			then
			hacer("CastSpellByName('Aliento de fuego','target')")
			print("Aliento de fuego")

			elseif chi >= 1
			and not UnitBuff("player","Poder del tigre")
			then
			hacer("CastSpellByName('Palma del tigre','target')")
			print("Palma del tigre")

			elseif chi >= 2 then
			hacer("CastSpellByName('Patada oscura','target')")
			print("Patada oscura")

			elseif energy > 75 then
			hacer("CastSpellByName('Atizar','target')")
			print("Atizar")
			end

		end
	end


		if spec == 2 then -- Tejedor de niebla

			if not UnitBuff("player","Legado del Emperador")
			then
			hacer("CastSpellByName('Legado del Emperador','player')")
			print("Legado del Emperador")
			end

			if GetSpellCooldown("Brebaje reconstituyente") == 0
			and playerHP < 50
			then
			hacer("CastSpellByName('Brebaje reconstituyente','player')")
			print("Brebaje reconstituyente")
			end

			if UnitExists("target") and not UnitIsDeadOrGhost("target")
			and not UnitCanAttack("player","target") --and UnitAffectingCombat("player")
			and not UnitHasVehicleUI("player")
			and not IsMounted()
			--and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Zen Meditation
		    --and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
		    --and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
		    --and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
			then
				--mirar("target")
				if GetShapeshiftForm("player") ~= 1 then
				hacer("CastShapeshiftForm(1)")
				end

				if GetSpellCooldown("Expulsar daño") == 0
				and ((targetHP < 100 and targetHP > 75) or (targetHP == 100 and chi < 4))
				then
				hacer("CastSpellByName('Expulsar daño')")
				print("Expulsar daño")

				elseif GetSpellCooldown("Niebla renovadora") == 0
				and ((targetHP < 100 and targetHP > 85) or (targetHP == 100 and chi < 4))
				then
				hacer("CastSpellByName('Niebla renovadora')")
				print("Niebla renovadora")

				elseif GetSpellCooldown("Niebla reconfortante") == 0
				and targetHP < 100
				and IsCurrentSpell("Niebla reconfortante") == false
				then
				hacer("CastSpellByName('Niebla reconfortante')")
				print("Niebla reconfortante")

				elseif GetSpellCooldown("Niebla envolvente") == 0
				and UnitChannelInfo("player") == "Niebla reconfortante"
				and targetHP < 75
				and chi >=3
				then
				hacer("CastSpellByName('Niebla envolvente')")
				print("Niebla envolvente")

				elseif GetSpellCooldown("Oleada de niebla") == 0
				and UnitChannelInfo("player") == "Niebla reconfortante"
				and targetHP < 75
				and chi < 3
				then
				hacer("CastSpellByName('Oleada de niebla')")
				print("Oleada de niebla")

				elseif GetSpellCooldown("Ola de chi") == 0
				and targetHP < 100 and targetHP > 50
				then
				hacer("CastSpellByName('Ola de chi')")
				print("Ola de chi")


				end

			end

			if UnitExists("target") and not UnitIsDeadOrGhost("target")
			and UnitCanAttack("player","target") --and UnitAffectingCombat("player")
			and not UnitHasVehicleUI("player")
			and not IsMounted()
			--and not (UnitChannelInfo("player") == GetSpellInfo(131523)) -- Zen Meditation
			--and not (UnitCastingInfo("player") == GetSpellInfo(101643)) -- Set Trans position
			--and not (UnitCastingInfo("player") == GetSpellInfo(119996)) -- Transout
			--and not (UnitChannelInfo("player") == GetSpellInfo(113656)) then -- Fists Of Fury
			then
				mirar("target")
				if GetShapeshiftForm("player") ~= 2 then
				hacer("CastShapeshiftForm(2)")
				end

				if chi >= 1
				and not UnitBuff("player","Poder del tigre")
				then
				hacer("CastSpellByName('Palma del tigre','target')")
				print("Palma del tigre")

				elseif chi >= 2 then
				hacer("CastSpellByName('Patada oscura','target')")
				print("Patada oscura")

				elseif energy > 75
				and IsCurrentSpell("Atizar") == false
				then
				hacer("CastSpellByName('Atizar','target')")
				print("Atizar")
				end
			end
		end

end
