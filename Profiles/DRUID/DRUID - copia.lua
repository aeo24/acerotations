addonName, LURS = ...
LURS.druid = {}
LURS.druid.ROTATION = function()

--- Si estas variables las pongo fuera del frame no las toma.

local playerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local rage = UnitPower("player", 1)
local energy = UnitPower("player", 3)
local combopoints = GetComboPoints("player","target")
local shape = GetShapeshiftForm()
local lacerar = select(4, UnitDebuff("target","Lacerar")) or 0
	
	-- Automatico todas las formas

	if UnitIsDeadOrGhost("player") == nil and not UnitHasVehicleUI("player") and not IsMounted() then 
		
		if not UnitBuff("player", "Marca de lo Salvaje") and GetSpellCooldown("Marca de lo Salvaje") == 0 then
			print("Marca de lo salvaje")
			oexecute("CastSpellByName('Marca de lo Salvaje')")
			end
		
		if InCombatLockdown() then
		
			if playerHP < 40 and GetSpellCooldown("Poderío de Ursoc") == 0 then
			oexecute("CastSpellByName('Poderío de Ursoc')")
			end
		
			if not UnitBuff("player", "Resguardo de Cenarius") and GetSpellCooldown("Resguardo de Cenarius") == 0 then
			oexecute("CastSpellByName('Resguardo de Cenarius')")
			end
					
			if playerHP < 55 and GetSpellCooldown("Instintos de supervivenvia") == 0 then
			oexecute("CastSpellByName('Instintos de supervivenvia')") 
			end						
			
			if playerHP < 50 and GetSpellCooldown("Renovación") == 0 then
			oexecute("CastSpellByName('Renovación')") 
			end
			
			if playerHP < 45 and rage > 30 and GetSpellCooldown("Regeneración frenética") == 0 then
			oexecute("CastSpellByName('Regeneración frenética')") 
			end
			
			if UnitExists("Pet") 
			then oexecute("CastSpellByName('Hibernar')") end
			
			if UnitBuff("player", "Presteza de depredador") 
			then oexecute("CastSpellByName('Toque de sanación')") end
			
			if not UnitBuff("player", "Piel de corteza") and GetSpellCooldown("Piel de corteza") == 0 then
				oexecute("CastSpellByName('Piel de corteza')")
			end
			
			if not UnitBuff("player", "Trabazón con la naturaleza") and GetSpellCooldown("Trabazón con la naturaleza") == 0 then
				oexecute("CastSpellByName('Trabazón con la naturaleza')")
			end
			
			if UnitExists('target') then
			local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
			if name and not notInterruptible and GetSpellCooldown("Testarazo") == 0 then
				if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
					oexecute("SpellStopCasting()")
					oexecute("CastSpellByName('Testarazo')")
				end
			end
			end
			
		end
	end
	
	if InCombatLockdown() and not IsMoving()
		and UnitDebuff("player","Descarga de escarcha")-- Descarga de escarcha
		or UnitDebuff("player","Ventisca")-- Ventisca
		or UnitDebuff("player","Nova de escarcha")-- Nova de escarcha
		or UnitDebuff("player","Enfriado")-- Nova de escarcha
		or UnitDebuff("player","Disparo de conmoción")-- Disparo de conmoción
		or UnitDebuff("player","Seccionar")-- Seccionar
		or UnitDebuff("player","Raíces enredadoras")-- Raíces enredadoras
		or UnitDebuff("player","Veneno entorpecedor")-- Veneno entorpecedor
		or UnitDebuff("player","Telaraña")-- Telaraña
		or UnitDebuff("player","Heridas infectadas")-- Heridas infectadas
		or UnitDebuff("player","Descarga de pirofrío")-- Descarga de pirofrío
		or UnitDebuff("player","Agarrón de tentáculos de vacío")-- Tentáculos de vacío
		or UnitDebuff("player","Enraizado")-- Tentáculos de vacío
		or UnitDebuff("player","Por los pelos")-- Tentáculos de vacío
		or UnitDebuff("player","Atontado")-- Tentáculos de vacío
		then
			if GetShapeshiftForm() == 2 then
			oexecute("CastShapeshiftForm(2)")
			elseif GetShapeshiftForm() == 3 then
			oexecute("CastShapeshiftForm(3)")
			end
	end
	
	-- Automático en forma felina
  
	if UnitIsDeadOrGhost("player") == nil and shape == 3 and not UnitHasVehicleUI("player") then 
	
		if UnitExists("target") then
		
			if not UnitBuff("player", "Furia del tigre") and energy < 20 and GetSpellCooldown("Furia del tigre") == 0 and InCombatLockdown() then
				oexecute("CastSpellByName('Furia del tigre')")
			end		
		
			if not UnitBuff("player", "Rugido Salvaje") and GetSpellCooldown("Rugido Salvaje") == 0 then
			print("Rugido salvaje")
			oexecute("CastSpellByName('Rugido Salvaje')")
			end
		
		end
		
		if UnitExists('target')
		and InCombatLockdown()
		and energy  < 35
		and not UnitDebuff("target", "Armadura debilitada") 
		and GetSpellCooldown("Fuego feérico") == 0 
		then
		print("Fuego feérico")
		oexecute("CastSpellByName('Fuego feérico')")
		end
		
		
	end
		
	
-- Automatico en forma de oso
	
	if UnitIsDeadOrGhost("player") == nil and shape == 1 and not UnitHasVehicleUI("player") then 
	
			
		if InCombatLockdown()
		and not UnitBuff("player", "Defensa salvaje") 
		and rage >= 60 
		and select(2, GetSpellCooldown("Defensa salvaje")) == 0
		then
		print("Defensa salvaje")
		oexecute("CastSpellByName('Defensa salvaje')")
		end
		
		if InCombatLockdown()
		and rage > 90  and playerHP < 50
		and GetSpellCooldown("Enfurecer") == 0 
		and InCombatLockdown() then
		oexecute("CastSpellByName('Enfurecer')")
		end
		
		if UnitExists('target')
		and InCombatLockdown()
		and rage  < 60
		and not UnitDebuff("target", "Armadura debilitada") 
		and GetSpellCooldown("Fuego feérico") == 0 
		then
		print("Fuego feérico")
		oexecute("CastSpellByName('Fuego feérico')")
		end
		
		if InCombatLockdown()
		and not UnitBuff("player", "Enfurecer") 
		and rage < 50 
		and GetSpellCooldown("Enfurecer") == 0 
		and InCombatLockdown() then
		oexecute("CastSpellByName('Enfurecer')")
		end
		
		
		
	end
	
	-- BOTON 1
	
	if ActionButton1:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then
	
		oface("target")
	
		if GetShapeshiftForm() ~= 3 then
		oexecute("CastShapeshiftForm(3)")
		end 
	
		if UnitBuff("player", "Rey de la selva") then
		end
		
	
		if GetSpellCooldown("Acechar") == 0 and not UnitBuff("player", "Acechar") and not InCombatLockdown() then
		oexecute("CastSpellByName('Acechar')")
		
		
		end
		
		if UnitBuff("player", "Lanzamiento libre") 
		and not UnitDebuff("target", "Vapulear")
		and targetHP > 25
		then
		oexecute("CastSpellByName('Vapulear')")
		
		end
		
			if combopoints < 5 then
				oface("target")
				
				if IsSpellInRange("Devastar", "target") == 0 and IsSpellInRange("Carga salvaje", "target") == 1 and GetSpellCooldown("Carga salvaje") == 0 then
				oexecute("CastSpellByName('Carga salvaje')")
				end
				
				if  UnitExists('target') and UnitBuff("player", "Acechar") and not UnitFactionGroup("target") == nil and GetSpellCooldown("Abalanzarse") == 0 then
						oface("target")
						oexecute("CastSpellByName('Abalanzarse')")
						
						
					elseif  UnitExists('target') and UnitBuff("player", "Acechar") and GetSpellCooldown("Abalanzarse") == 0 then
						oface("target")
						oexecute("CastSpellByName('Devastar')")
						
					
					elseif UnitExists('target') and energy >= 35 and GetSpellCooldown("Arañazo") == 0 and not UnitDebuff("target", "Arañazo") then
						oface("target")
						oexecute("CastSpellByName('Arañazo')")
						
						
					elseif UnitExists('target') and energy >= 35 and obehind('target') == 0 and GetSpellCooldown("Destrozar") == 0 then
						oface("Destrozar")
						oexecute("CastSpellByName('Destrozar')")
						
					elseif UnitExists('target') and energy >= 40 and obehind('target') == 1 and GetSpellCooldown("Triturar") == 0 then
						oface("target")
						oexecute("CastSpellByName('Triturar')")
						print("Triturar")
						
					
					
				end
			
			end
			
			if combopoints == 5 then
			
				if not UnitDebuff("target", "Destripar") and targetHP > 35 and energy >= 30 then
				oface("target")
				oexecute("CastSpellByName('Destripar')")
				print("Destripar")
				
				elseif UnitDebuff("target", "Destripar") and UnitBuff("player", "Rugido Salvaje") and energy >= 35 and targetHP > 25 then
					oface("target")
					oexecute("CastSpellByName('Amputar')")
					print("Mordedura feroz")
					
				elseif UnitDebuff("target", "Destripar") and UnitBuff("player", "Rugido Salvaje") and energy >= 25 then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
					
				elseif UnitBuff("player", "Rugido Salvaje") then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
					
				end
				
			end
			
		
		
	end
	
	-- BOTON 2

	if ActionButton2:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then
	
		oface("target")
	
		if GetShapeshiftForm() ~= 1 then
		oexecute("CastShapeshiftForm(1)")
		end
	
		if UnitThreatSituation("player","target") ~= 3 and GetSpellCooldown("Bramido") == 0 then
		oface("target")
		oexecute("CastSpellByName('Bramido')")
		print("Bramido")
		end
		
	
		if IsSpellInRange("Carga salvaje", "target") == 1 and GetSpellCooldown("Carga salvaje") == 0 then
		oface("target")
		oexecute("CastSpellByName('Carga salvaje')")
		end
		
		if UnitExists('target') and IsSpellInRange("Abrazo de oso", "target") == 1 and GetSpellCooldown("Abrazo de oso") == 0 and not UnitDebuff("target","Azote poderoso") then
			oface("target")
			oexecute("CastSpellByName('Abrazo de oso')")
			
			print("Azote poderoso")
		end
		
		if UnitExists('target') and IsSpellInRange("Azote poderoso", "target") == 1 and GetSpellCooldown("Azote poderoso") == 0 and not not UnitDebuff("target","Abrazo de oso")then
			oface("target")
			oexecute("CastSpellByName('Azote poderoso')")
			
			print("Azote poderoso")
		end
		
		if UnitExists('target') and IsSpellInRange("Destrozar", "target") == 1 and GetSpellCooldown("Destrozar") == 0 then
			oface("target")
			oexecute("CastSpellByName('Destrozar')")
			
			print("Destrozar")
			
		elseif UnitExists('target') and GetSpellCooldown("Magullar") == 0 and UnitBuff("player", "Garras y dientes") and rage > 30 then
			
			print("Proc: Garras y dientes")	
			oexecute("CastSpellByName('Magullar')")
			
		elseif UnitExists('target') and not UnitDebuff("target","Vapulear") and GetSpellCooldown("Vapulear") == 0 and InCombatLockdown() then
			
			print("Vapulear")
			oexecute("CastSpellByName('Vapulear')")
						
		elseif UnitExists('target') and IsSpellInRange("Lacerar", "target") and lacerar < 3 and GetSpellCooldown("Lacerar") == 0 then
			
			print("Lacerar")
			oexecute("CastSpellByName('Lacerar')")
			
		elseif UnitExists('target') and GetSpellCooldown("Magullar") == 0 and rage == 100 then
			
			print("Dump rage")
			oexecute("CastSpellByName('Magullar')")
				
		end
		
	end
		
	
	-- BOTON 3
	
	if ActionButton3:GetButtonState() == 'PUSHED' then
	
		if shape == 1 or shape == 2 then

			if UnitExists('target') and GetSpellCooldown("Destrozar") == 0 then 
			oexecute("CastSpellByName('Destrozar')")
			end
			
			if GetSpellCooldown("Vapulear") == 0 then 
			oexecute("CastSpellByName('Vapulear')")
			end
			
			if GetSpellCooldown("Flagelo") == 0 then 
			oexecute("CastSpellByName('Flagelo')")
			end
			
		end
		
		if shape == 0 then
			if UnitExists('target') and GetSpellCooldown("Huracán") == 0 then 
			oinfo()
			oexecute("CastSpellByName('Huracán')")
			oclick("target")
			end
		end
		
	end  
	
	if ActionButton4:GetButtonState() == 'PUSHED' then
	
		print(GetShapeshiftForm())
		oexecute("CastShapeshiftForm(4)")
		oexecute("CastSpellByName('Rugido de estampida')")
		
				
	end
		
-- HP BAJO





	
end