addonName, LURS = ...
LURS.druid = {}
LURS.druid.ROTATION = function()

--- Si estas variables las pongo fuera del frame no las toma.

local spec = GetSpecialization() -- 1 Balance, 2 Feral, 3 Guardian, 4 Restoration
local PlayerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local eclipse = UnitPower("player", 8)
local rage = UnitPower("player", 1)
local energy = UnitPower("player", 3)
local combopoints = GetComboPoints("player","target")
local shape = GetShapeshiftForm()
local lacerar = select(4, UnitDebuff("target","Lacerar")) or 0
CastingTime = GetTime()

function GetDistance(unit1,unit2)
if not UnitExists(unit1) or not UnitExists(unit2) then return 0 end
local x1,y1,z1,rot1 = opos(unit1) 
local x2,y2,z2,rot2 = opos(unit2) 
return sqrt( (x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2 )
end

distance = GetDistance("player","target")

if TimePostCast == nil 
or CastingTime - TimePostCast > 0.5 then

	-- Automático todas las formas
	if UnitIsDeadOrGhost("player") == false 
	and not UnitHasVehicleUI("player") 
	and not IsMounted() then 
		
		if not UnitBuff("player", "Marca de lo Salvaje") 
		and not InCombatLockdown()
		and GetSpellCooldown("Marca de lo Salvaje") == 0 then
		print("Marca de lo salvaje")
		oexecute("CastSpellByName('Marca de lo Salvaje')")
		end
		
		if PlayerHP < 35
		and shape == 1
		and rage > 20
		then
		oexecute("CastSpellByName('Regeneración frenética')")
		print("Regeneración frenética")
		end
		
		if PlayerHP < 65
		and not shape == 1
		and UnitBuff("player","Rejuvenecimiento")
		and IsCurrentSpell("Toque de sanación") == false then
		oexecute("CastSpellByName('Toque de sanación')")
		print("Toque de sanación")
		end
		
		if PlayerHP < 85 
		and not UnitBuff("player","Rejuvenecimiento")
		and not shape == 1
		and GetSpellCooldown("Rejuvenecimiento") == 0 then
		oexecute("CastSpellByName('Rejuvenecimiento')")
		print("Rejuvenecimiento")
		end
		
		
		
		if IsMoving()
		and not IsIndoors()
		and not InCombatLockdown()
		and not UnitBuff("player","Carrerilla")
		and shape ~= 3 then
		oexecute("CastShapeshiftForm(3)")
		print("Forma de viaje")
		end
		
		if InCombatLockdown() then
			
			if spec == 1
			and shape ~= 4 then
			oexecute("CastShapeshiftForm(4)")
			end
			
			if spec == 2
			and shape ~= 2 then
			oexecute("CastShapeshiftForm(2)")
			end
			
			if spec == 3
			and shape ~= 1 then
			oexecute("CastShapeshiftForm(1)")
			end
		
			if PlayerHP < 40 and GetSpellCooldown("Poderío de Ursoc") == 0 then
			oexecute("CastSpellByName('Poderío de Ursoc')")
			end
		
			if PlayerHP < 75
			and not UnitBuff("player", "Resguardo de Cenarius") 
			and GetSpellCooldown("Resguardo de Cenarius") == 0 then
			oexecute("CastSpellByName('Resguardo de Cenarius')")
			end
			
			if PlayerHP < 75
			and not UnitBuff("player", "Piel de corteza") 
			and GetSpellCooldown("Piel de corteza") == 0 then
			oexecute("CastSpellByName('Piel de corteza')")
			end
					
			if PlayerHP < 55 
			and GetSpellCooldown("Instintos de supervivencia") == 0 then
			oexecute("CastSpellByName('Instintos de supervivencia')") 
			end			
			
			if PlayerHP < 50 and GetSpellCooldown("Renovación") == 0 then
			oexecute("CastSpellByName('Renovación')") 
			end
			
			if PlayerHP < 45 and rage > 30 and GetSpellCooldown("Regeneración frenética") == 0 then
			oexecute("CastSpellByName('Regeneración frenética')") 
			end
			
			if UnitExists("Pet") 
			then oexecute("CastSpellByName('Hibernar')") end
			
			if UnitBuff("player", "Presteza de depredador") 
			then 
			oexecute("SpellStopCasting()")
			oexecute("CastSpellByName('Toque de sanación')") end
			
			
			
			if not UnitBuff("player", "Trabazón con la naturaleza") and GetSpellCooldown("Trabazón con la naturaleza") == 0 then
			oexecute("CastSpellByName('Trabazón con la naturaleza')")
			end
			
			-- Interrupt
			if UnitExists('target') then
				local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
				if name and not notInterruptible and GetSpellCooldown("Testarazo") == 0 then
					if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
						oexecute("SpellStopCasting()")
						oexecute("CastSpellByName('Testarazo')")
						print("Interrumpido")
					end
				end
			end
			
			if UnitExists('target') 
			and spec == 1
			then
				local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
				if name and not notInterruptible and GetSpellCooldown("Rayo solar") == 0 then
					if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
						oexecute("SpellStopCasting()")
						oexecute("CastSpellByName('Rayo solar')")
						print("Interrumpido")
					end
				end
			end
			--
		end
	end 
	-- End of Automático
	
	if InCombatLockdown()
		and UnitDebuff("player","Descarga de escarcha")-- Descarga de escarcha
		or UnitDebuff("player","Ventisca")-- Ventisca
		or UnitDebuff("player","Nova de escarcha")-- Nova de escarcha
		or UnitDebuff("player","Enfriado")-- Nova de escarcha
		or UnitDebuff("player","Disparo de conmoción")-- Disparo de conmoción
		or UnitDebuff("player","Seccionar")-- Seccionar
		or UnitDebuff("player","Raíces enredadoras")-- Raíces enredadoras
		or UnitDebuff("player","Veneno entorpecedor")-- Veneno entorpecedor
		or UnitDebuff("player","Telaraña")-- Telaraña
		or UnitDebuff("player","Heridas infectadas")-- Heridas infectadas
		or UnitDebuff("player","Descarga de pirofrío")-- Descarga de pirofrío
		or UnitDebuff("player","Agarrón de tentáculos de vacío")-- Tentáculos de vacío
		or UnitDebuff("player","Enraizado")-- Tentáculos de vacío
		or UnitDebuff("player","Por los pelos")-- Tentáculos de vacío
		or UnitDebuff("player","Atontado")-- Tentáculos de vacío
		then
			if GetShapeshiftForm() == 1 then
			oexecute("RunMacroText('/cast !Forma de oso(Cambio de forma)")
			elseif GetShapeshiftForm() == 2 then
			oexecute("RunMacroText('/cast !Forma de felina(Cambio de forma)')")
			end
	end
	
	-- Automático en forma felina
  
	if UnitIsDeadOrGhost("player") == false 
	and shape == 2 
	and not UnitHasVehicleUI("player") 
	and UnitCanAttack("player","target") then 
		if UnitExists("target") then
			if not UnitBuff("player", "Furia del tigre") 
			and energy < 20 and GetSpellCooldown("Furia del tigre") == 0 
			and InCombatLockdown() then
			oexecute("CastSpellByName('Furia del tigre')")
			end		
		
			if not UnitBuff("player", "Rugido salvaje") 
			and GetSpellCooldown("Rugido salvaje") == 0 then
			print("Rugido salvaje")
			oexecute("CastSpellByName('Rugido salvaje')")
			end
		end
		
		if UnitExists('target')
		and InCombatLockdown()
		and energy  < 35
		and not UnitDebuff("target", "Armadura debilitada") 
		and GetSpellCooldown("Fuego feérico") == 0 
		then
		print("Fuego feérico")
		oexecute("CastSpellByName('Fuego feérico')")
		end		
	end
		
	
	-- Automático en forma de oso
	
	if UnitIsDeadOrGhost("player") == false
	and not UnitHasVehicleUI("player") 	
	and shape == 1 then 
	
		if InCombatLockdown()
		and not UnitBuff("player", "Defensa salvaje") 
		and rage >= 60 
		and select(2, GetSpellCooldown("Defensa salvaje")) == 0
		then
		print("Defensa salvaje")
		oexecute("CastSpellByName('Defensa salvaje')")
		end
		
		if UnitExists('target')
		and InCombatLockdown()
		and rage  < 60
		and not UnitDebuff("target", "Armadura debilitada") 
		and GetSpellCooldown("Fuego feérico") == 0 
		then
		print("Fuego feérico")
		oexecute("CastSpellByName('Fuego feérico')")
		end
	end
	
	if UnitExists("target")
	and not UnitHasVehicleUI("player") 
	and not UnitIsDeadOrGhost("player")
	and not UnitIsDeadOrGhost("target")
	then
	
		-- Balance
		
		if spec == 1
		and UnitCanAttack("player","target")		
		and distance <= 40
		then
			if shape ~= 4 then
			oexecute("CastShapeshiftForm(4)")
			print("")
			
			elseif oenemy ("target") < 3 then
			
				if UnitBuff("player","Pico lunar") then
				oface("target")
				oexecute("CastSpellByName('Fuego lunar')")
				print("Pico lunar proc")
				
				elseif UnitBuff("player","Pico solar") then
				oface("target")
				oexecute("CastSpellByName('Fuego solar')")
				print("Pico solar proc.")
								
				elseif eclipse <= 0
				and not UnitDebuff("target","Fuego lunar") then
				oface("target")
				oexecute("CastSpellByName('Fuego lunar')")
				print("")
							
				elseif eclipse > 0
				and not UnitDebuff("target","Fuego solar") then
				oface("target")
				oexecute("CastSpellByName('Fuego solar')")
				print("")
							
				elseif UnitBuff("player","Potenciación lunar") 
				and UnitCastingInfo("player") == nil then
				oface("target")
				oexecute("CastSpellByName('Fuego estelar')")
				print("Potenciación lunar proc.")
							
				elseif UnitBuff("player","Potenciación solar") 
				and UnitCastingInfo("player") == nil then
				oface("target")
				oexecute("CastSpellByName('Cólera')")
				print("Potenciación solar")
						
				elseif not (UnitBuff("player","Potenciación lunar") or UnitBuff("player","Potenciación solar"))
				and GetSpellCooldown("Oleada de estrellas") == 0 then
				oface("target")
				oexecute("CastSpellByName('Oleada de estrellas')")
				print("")
						
				elseif IsSpellInRange("Cólera", "target") == 1 and GetSpellCooldown("Cólera") == 0 then
				oface("target")
				oexecute("CastSpellByName('Cólera')")
				print("")
				end
				
			elseif (oenemy("target") >=3 or oenemy("target") <=5) then
				if eclipse <= 0
				and not UnitDebuff("target","Fuego lunar") then
				oface("target")
				oexecute("CastSpellByName('Fuego lunar')")
				print("")
							
				elseif eclipse > 0 
				and not UnitDebuff("target","Fuego solar") then
				oface("target")
				oexecute("CastSpellByName('Fuego solar')")
				print("")
							
				elseif UnitBuff("player","Potenciación lunar") then
				oface("target")
				oexecute("CastSpellByName('Fuego estelar')")
				print("")
							
				elseif UnitBuff("player","Potenciación solar") then
				oface("target")
				oexecute("CastSpellByName('Cólera')")
				print("")
						
				elseif not (UnitBuff("player","Potenciación lunar") or UnitBuff("player","Potenciación solar"))
				and GetSpellCooldown("Lluvia de estrellas") == 0 then
				oface("target")
				oexecute("CastSpellByName('Lluvia de estrellas')")
				print("")
						
				elseif IsSpellInRange("Cólera", "target") == 1 and GetSpellCooldown("Cólera") == 0 then
				oface("target")
				oexecute("CastSpellByName('Cólera')")
				print("")
				end
			elseif oenemy("target") > 5
			and IsCurrentSpell("Huracán") == false
			then
				oexecute("CastSpellByName('Huracán')")
				oclick("target")
			end
			
			
		end -- End of Balance
	
		-- Feral
		
		if spec == 2 
		and UnitCanAttack("player","target")		
		then
		
			if UnitCanAttack("player", "target") == true --ActionButton1:GetButtonState() == 'PUSHED'
			and not UnitIsDeadOrGhost("target")		
			and not UnitHasVehicleUI("player") then
			
				oface("target")
				-- unroot
				--if shape ~= 2 then
				--oexecute("CastShapeshiftForm(2)")
				--end 
			
				--if UnitBuff("player", "Rey de la selva") then
				--end
				
				if GetSpellCooldown("Acechar") == 0 
				and not UnitBuff("player", "Acechar") 
				and not InCombatLockdown() then
				oexecute("CastSpellByName('Acechar')")
				print("Acechar")
				end
				
				if UnitBuff("player", "Lanzamiento libre")
				and distance <= 8
				and not UnitDebuff("target", "Vapulear")
				and targetHP > 25
				then
				oexecute("CastSpellByName('Vapulear')")
				print("Vapulear")
				end
				
				if combopoints < 5 then
					oface("target")
					if (distance >= 8 and distance <= 25)
					and GetSpellCooldown("Carga salvaje") == 0 then
					oexecute("CastSpellByName('Carga salvaje')")
					print("Carga salvaje")
					end
					
					if  UnitExists('target')
					and distance <= 5
					and energy >= 35 and GetSpellCooldown("Arañazo") == 0 
					and not UnitDebuff("target", "Arañazo") then
					oface("target")
					oexecute("CastSpellByName('Arañazo')")						
						
					elseif UnitExists('target')
					and distance <= 5				
					and energy >= 40 
					and GetSpellCooldown("Triturar") == 0 then
					oface("target")
					oexecute("CastSpellByName('Triturar')")
					print("Triturar")					
					end
				
				end
					
				if combopoints == 5 then
				
					if InCombatLockdown()
					and not UnitBuff("player", "Rugido salvaje") then
					oexecute("CastSpellByName('Rugido salvaje')")
				
					elseif not UnitDebuff("target", "Destripar") 
					and targetHP > 25 
					and energy >= 30 then
					oface("target")
					oexecute("CastSpellByName('Destripar')")
					print("Destripar")
					
					elseif UnitDebuff("target", "Destripar") 
					and energy > 50
					and targetHP > 25 then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					elseif UnitDebuff("target", "Destripar")
					and energy >= 25 
					and targetHP <= 25 then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					else
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					end
				end
			end
		end
		
		-- Guardian 
		if spec == 3 
		and UnitCanAttack("player","target")		
		then
			if GetShapeshiftForm() ~= 1 then
			oexecute("CastShapeshiftForm(1)")
			end
			
			if PlayerHP < 90
			and UnitBuff("player","Sueño de Cenarius")  then
			oexecute("CastSpellByName('Toque de sanación')")
			print("Cenarius Proc.")
			end
		
			if UnitThreatSituation("player","target") ~= 3
			and IsSpellInRange("Bramido", "target") == 1
			and GetSpellCooldown("Bramido") == 0 then
			oface("target")
			oexecute("CastSpellByName('Bramido')")
			print("Bramido")
						
			elseif UnitExists('target') 
			and IsSpellInRange("Carga salvaje", "target") == 1 
			and GetSpellCooldown("Carga salvaje") == 0 
			and not UnitDebuff("target","Carga salvaje") then
			oface("target")
			oexecute("CastSpellByName('Carga salvaje')")
			print("Carga salvaje")
			end
			
			if oenemy("player") > 2 then -- > 2 enemies
				
				if UnitExists('target') 
				and distance <= 8.5
				and not UnitDebuff("target","Vapulear") 
				and GetSpellCooldown("Vapulear") == 0 
				and InCombatLockdown() then
				print("Vapulear")
				oexecute("CastSpellByName('Vapulear')")
				
				elseif UnitExists('target') 
				and IsSpellInRange("Destrozar", "target") == 1 
				and GetSpellCooldown("Destrozar") == 0 then
				oface("target")
				oexecute("CastSpellByName('Destrozar')")
				print("Destrozar")
				
				elseif UnitExists('target')
				and IsSpellInRange("Magullar", "target") == 1
				and GetSpellCooldown("Magullar") == 0
				and UnitBuff("player", "Garras y dientes") 
				and rage > 30 then
				print("Proc: Garras y dientes")	
				oexecute("CastSpellByName('Magullar')")
				
				elseif UnitExists('target') 
				and IsSpellInRange("Magullar", "target") == 1
				and GetSpellCooldown("Magullar") == 0
				and rage == 100 then
				print("Dump rage")
				oexecute("CastSpellByName('Magullar')")
				
				elseif oenemy("player") > 2
				and distance <= 8
				and GetSpellCooldown("Vapulear") == 0 
				and InCombatLockdown() then
				print("Vapulear")
				oexecute("CastSpellByName('Vapulear')")					
				end
			end -- > 2 enemies	
			
			if oenemy("player") <=2 then -- > 2 enemies
				if UnitExists('target') 
				and IsSpellInRange("Destrozar", "target") == 1 
				and GetSpellCooldown("Destrozar") == 0 
				then
				oface("target")
				oexecute("CastSpellByName('Destrozar')")
				print("Destrozar")
					
				elseif UnitExists('target') 
				and GetSpellCooldown("Magullar") == 0 
				and UnitBuff("player", "Garras y dientes") 
				and rage > 30 
				then
				print("Proc: Garras y dientes")	
				oexecute("CastSpellByName('Magullar')")
					
				elseif UnitExists('target')
				and distance <= 8
				and not UnitDebuff("target","Vapulear") 
				and GetSpellCooldown("Vapulear") == 0 
				and InCombatLockdown() 
				then
				print("Vapulear")
				oexecute("CastSpellByName('Vapulear')")

				elseif UnitExists('target') 
				and IsSpellInRange("Lacerar", "target") == 1 
				and lacerar < 3 and GetSpellCooldown("Lacerar") == 0 
				then
				print("Lacerar")
				oexecute("CastSpellByName('Lacerar')")
					
				elseif UnitExists('target') 
				and GetSpellCooldown("Magullar") == 0 
				and rage == 100 
				then
				print("Dump rage")
				oexecute("CastSpellByName('Magullar')")
				end
			end	
		end
		-- End of guardian
		
		if spec == 4 then
			-- Support
			if UnitCanAttack("player", "target") == false --ActionButton1:GetButtonState() == 'PUSHED'
			and not UnitIsDeadOrGhost("target")		
			and not UnitHasVehicleUI("player") then
			oface("target")
			
				if GetSpellCooldown("Presteza de la naturaleza") == 0 then
				oexecute("CastSpellByName('Presteza de la naturaleza')")
				end
				
				if targetHP < 75
				and not UnitBuff("player", "Resguardo de Cenarius") 
				and GetSpellCooldown("Resguardo de Cenarius") == 0 then
				oexecute("CastSpellByName('Resguardo de Cenarius')")
				end
			
				if not UnitBuff("player","Armonía")
				then
					if GetSpellCooldown("Alivio presto") == 0 then
					oexecute("CastSpellByName('Alivio presto')")
					elseif not UnitBuff("target","Recrecimiento")
					and UnitBuff("player","Lanzamiento libre")
					and GetSpellCooldown("Recrecimiento") == 0 then
					oexecute("CastSpellByName('Recrecimiento')")
					else
					oexecute("CastSpellByName('Toque de sanación')")
					end
				print("Armonia Buff")
				end
			
				if targetHP < 35
				and not UnitBuff("target","Recrecimiento")
				and UnitBuff("player","Lanzamiento libre")
				and GetSpellCooldown("Recrecimiento") == 0 then
				oexecute("CastSpellByName('Recrecimiento')")
				print("Recrecimiento")
				end
			
				if targetHP < 85
				and (UnitBuff("target","Rejuvenecimiento") or UnitBuff("target","Rejuvenecimiento"))
				and GetSpellCooldown("Alivio presto") == 0 then
				oexecute("CastSpellByName('Alivio presto')")
				print("Alivio presto")
				end
			
				if targetHP < 85
				and not UnitBuff("target","Rejuvenecimiento")
				and GetSpellCooldown("Rejuvenecimiento") == 0 then
				oexecute("CastSpellByName('Rejuvenecimiento')")
				print("Rejuvenecimiento")
				end
				
				if targetHP < 85
				and not UnitBuff("player","Crecimiento salvaje")
				and GetSpellCooldown("Crecimiento salvaje") == 0 then
				oexecute("CastSpellByName('Crecimiento salvaje')")
				print("Crecimiento salvaje")
				end
				
				if targetHP < 85
				and not UnitBuff("target","Flor de vida")
				and GetSpellCooldown("Flor de vida") == 0 then
				oexecute("CastSpellByName('Flor de vida')")
				print("Flor de vida")
				end

				if targetHP < 85
				and select(1, GetTotemInfo(1)) == false 
				and GetSpellCooldown("Champiñón salvaje") == 0 then
				oexecute("CastSpellByName('Champiñón salvaje')")
				print("Champiñón salvaje")
				end
				
				if UnitBuff("player", "Lanzamiento libre")
				and distance <= 8
				and not UnitDebuff("target", "Vapulear")
				and targetHP > 25
				then
				oexecute("CastSpellByName('Vapulear')")
				print("Vapulear")
				end
				
				if combopoints < 5 then
					oface("target")
					if (distance >= 8 and distance <= 25)
					and GetSpellCooldown("Carga salvaje") == 0 then
					oexecute("CastSpellByName('Carga salvaje')")
					print("Carga salvaje")
					end
					
					if  UnitExists('target')
					and distance <= 5
					and energy >= 35 and GetSpellCooldown("Arañazo") == 0 
					and not UnitDebuff("target", "Arañazo") then
					oface("target")
					oexecute("CastSpellByName('Arañazo')")						
						
					elseif UnitExists('target')
					and distance <= 5				
					and energy >= 40 
					and GetSpellCooldown("Triturar") == 0 then
					oface("target")
					oexecute("CastSpellByName('Triturar')")
					print("Triturar")					
					end
				
				end
					
				if combopoints == 5 then
				
					if InCombatLockdown()
					and not UnitBuff("player", "Rugido salvaje") then
					oexecute("CastSpellByName('Rugido salvaje')")
				
					elseif not UnitDebuff("target", "Destripar") 
					and targetHP > 25 
					and energy >= 30 then
					oface("target")
					oexecute("CastSpellByName('Destripar')")
					print("Destripar")
					
					elseif UnitDebuff("target", "Destripar") 
					and energy > 50
					and targetHP > 25 then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					elseif UnitDebuff("target", "Destripar")
					and energy >= 25 
					and targetHP <= 25 then
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					else
					oface("target")
					oexecute("CastSpellByName('Mordedura feroz')")
					print("Mordedura feroz")
						
					end
				end
			end
		end
		-- End of Resto
	end
TimePostCast = GetTime()	
end
end
