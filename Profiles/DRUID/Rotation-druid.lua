addonName, Rotations = ...
Rotations.druid = {}
Rotations.druid.ROTATION = function()
	RotationTickDruid()
	return false

end

Druid= {}
Druid.InWildMushroom = {}
Druid.lockoutTime = {}
local lastSpellCasted = nil
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
Druid.dispelMagicCC = {
115001,        -- Remorseless Winter
2637,        -- Hibernate
110698,        -- Hammer of Justice (Paladin)
--117526,        -- Binding Shot
3355,        -- Freezing Trap
1513,        -- Scare Beast
118271,        -- Combustion Impact
44572,        -- Deep Freeze
--31661,        -- Dragon's Breath
118,        -- Polymorph
61305,        -- Polymorph: Black Cat
28272,        -- Polymorph: Pig
61721,        -- Polymorph: Rabbit
61780,        -- Polymorph: Turkey
28271,        -- Polymorph: Turtle
82691,        -- Ring of Frost
11129,         -- Combustion
--123393,        -- Breath of Fire (Glyph of Breath of Fire)
115078,        -- Paralysis
105421,        -- Blinding Light
115752,        -- Blinding Light (Glyph of Blinding Light)
105593,        -- Fist of Justice
853,        -- Hammer of Justice
119072,        -- Holy Wrath
20066,        -- Repentance
10326,        -- Turn Evil
64044,        -- Psychic Horror
8122,        -- Psychic Scream
113792,        -- Psychic Terror (Psyfiend)
9484,        -- Shackle Undead
118905,        -- Static Charge (Capacitor Totem)
5782,        -- Fear
118699,        -- Fear
5484,        -- Howl of Terror
6789,        -- Mortal Coil
30283,        -- Shadowfury
104045,        -- Sleep (Metamorphosis)
115268,        -- Mesmerize (Shivarra)
113092,        -- Frost Bomb
6358,
51514,339,4167       -- Hex
}


function Druid.avoidSpellInturupt()

	for _,etarget in pairs(enemyTargets) do

		local _,eClass,_ = UnitClass(etarget)

		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
		for _, v in ipairs(polys) do
			if GetSpellInfo(v) == spellName then
			local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
			local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
			local castTime = endCast - startCast
			local currentPercent = timeSinceStart / castTime * 100000
				if GetUnitDistance(etarget) < 35  and InLineOfSight(etarget) == 1 then
					if currentPercent >76 then
					print("Attempting to shift polymorph....")
					AceStopSpellCasting()
					AceRunMacroText("/cast !Travel Form")
					end
					return true
				end
			end
		end
	end

if not dfooframe then
	dfooframe = CreateFrame("Frame")
    end
	local interruptID =     {
    [102060] = true,    --Disrupting Shout
    [106839] = true,    --Skull Bash
    [80964] = true,        --Skull Bash
    [115781] = true,    --Optical Blast
    [116705] = true,    --Spear Hand Strike
    [1766] = true,         --Kick
    [19647] = true,     --Spell Lock
    [2139] = true,         --Counterspell
    [47476] = true,        --Strangulate
    [47528] = true,     --Mind Freeze
    [57994] = true,     --Wind Shear
    [6552] = true,         --Pummel
    [96231] = true,     --Rebuke
    [31935] = true,        --Avenger's Shield
    [34490] = true,     --Silencing Shot
    [147362] = true     --Counter shot
    }

	local SIN_PlayerGUID = UnitGUID("player")
    dfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	dfooframe:RegisterEvent("LOSS_OF_CONTROL_UPDATE");
	dfooframe:RegisterEvent("LOSS_OF_CONTROL_ADDED");
	dfooframe:RegisterEvent("UPDATE_SHAPESHIFT_FORM")
	dfooframe:RegisterEvent("PLAYER_ENTERING_WORLD");
	dfooframe:SetScript("OnEvent", function(self, event,args, type, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
            --COmbat Log Unfiltered
			if type == "SPELL_CAST_SUCCESS" and destGUID == SIN_PlayerGUID and interruptID[spellID] then
                local isProtected =  select(9, UnitCastingInfo('player')) or select(8, UnitChannelInfo('player'))
                    if not isProtected and not HaveBuff('player', 96267) then --96267 = priest's cast immunity
						AceStopSpellCasting()
						print("Attempting to fake cast"..spellID)
                        AceStopSpellCasting()
                    end
            end
			if type == "SPELL_CAST_SUCCESS" and sourceGUID == SIN_PlayerGUID then
				lastSpellCasted =  spellID
			end



			if type == "SPELL_CAST_FAILED" and sourceGUID == SIN_PlayerGUID then
				if  extraSpellID ~= "Can't do that while moving" and  extraSpellID ~= "You are dead" and  extraSpellID ~= "Ability is not ready yet" and  extraSpellID ~= "Not yet recovered" then
					print("Failed to cast: "..GetSpellInfo(spellID).." "..extraSpellID)
				end
			end

			if type == "SPELL_HEAL"  and sourceGUID == SIN_PlayerGUID and spellID == 81269 then
				if spellID == 81269 then
				Druid.InWildMushroom[destGUID] = GetTime();
				end
			end

			--Other Events
			if ( event == "LOSS_OF_CONTROL_ADDED" or event == "LOSS_OF_CONTROL_UPDATE" ) then
				local eventIndex = C_LossOfControl.GetNumEvents()
				local locType, spellID, text, iconTexture, startTime, timeRemaining, duration, lockoutSchool, priority, displayType = C_LossOfControl.GetEventInfo(eventIndex);
				if locType == 'SILENCE' or locType == 'SCHOOL_INTERRUPT' or locType == 'STUN_MECHANIC' and not HaveBuff('player',5487) and not HaveBuff('player',33891) and spellID ~= 78675 then
					Druid.lockoutTime.endTime = startTime + duration
					Druid.lockoutTime.active = true
					AceRunMacroText('/cast !Bear Form')
					--[[if playerHealth < 60 and not HaveBuff('player',53480) then
						AceCastSpellByName(22812)
					end]]--
				end
			end
			if ( event == "UPDATE_SHAPESHIFT_FORM") then
				 if GetShapeshiftFormID() == nil or  GetShapeshiftFormID() == nil then
					if morph ~= nil then morph(0,0,56) end
				 end
			end
			if ( event == "PLAYER_ENTERING_WORLD") then
				 table.empty(Druid.InWildMushroom)
				-- table.empty(Ace.deathTrack)
				 --table.empty(Ace.damageTable)
			end

    end);
	return false
end

function getPlayerId(unit)

local guid = UnitGUID(unit)
guid  = guid:sub(11, 19)
return '0x'..guid

end


function HasSR(var1)
local sr = {
            127538,
            52610
}
            for i=1,#sr do
                        local hasSR = HaveBuff(var1,sr[i])

                        if hasSR then
                                    return true,hasSR
                        end
            end
            return false
end




function Druid.dispellRotation(target,asap)

if HaveBuff('player',5487) then return end

local targetHealth = CalculateHP(target)

local InterruptSpells = { 2637,1513 }

local polys = {118,28271,28272,61721,61780,126819,161354,161355,161372,51514}

	if not enemyTargets
	then
		enemyTargets = {"target","arena1","arena2","arena3","arena4","arena5"}
	end

	for i=1,GetNumGroupMembers() do
    if UnitInRange("party"..i)

     and HaveDebuff("party"..i,{51514,80240},0)
    and InLineOfSight("party"..i) == 1 and isSpellAvailable(2782)
    then
	AceCastSpellByName(tostring(GetSpellInfo(2782)),"party"..i)
       end
    end


	local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		for _,dispell in pairs(Druid.dispelMagicCC) do
			if HaveDebuff(groupType..i, dispell) and isSpellAvailable(88423) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40  and (not HaveDebuff(groupType..i,30108) or (group.low < 2 and group.veryLow  == 0 )) and asap == true then
				 print("Attempting to dispell "..GetSpellInfo(dispell))
				 AceCastSpellByName(tostring(GetSpellInfo(88423)),groupType..i)
			end
		end
    end

	for _,etarget in pairs(enemyTargets) do

		local _,eClass,_ = UnitClass(etarget)


		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
			for _, v in ipairs(healingInterrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				if not HaveBuff("player", 5215) and InLineOfSight(etarget) == 1 and currentPercent > 25  and targetHealth < 70
					then
						--print(GetSpellInfo(v) )
						AceCastSpellByName(GetSpellInfo(106839),etarget)
					end
				end
			end
		local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
			for _, v in ipairs(channelInturrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and currentPercent > 15  and GetUnitDistance(etarget) < 40 and targetHealth < 70 then
						AceCastSpellByName(GetSpellInfo(106839),etarget)
					end
				end
			end
	end

end

function Druid.survivalRotation(lowestPlayer)

 local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))
 local specid = select(1, GetSpecializationInfo(GetSpecialization()))
 local catform = 783
 if specid == 103 then catform = 768 end
		if talentTierSeven == 21653 then
			catform = 171745
		end

	if HaveDebuff("PLAYER",aRoots()) and not HaveBuff('player',5487)  and not HaveBuff('player',33891) then
		AceRunMacroText("/cast !"..GetSpellInfo(catform))
	end


	local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

	if playerHealth < 70 and UnitAffectingCombat('player') and  GetItemCooldown(5512) == 0 then
				   AceRunMacroText('/use Healthstone')
	end


	if CalculateHP(lowestPlayer) < 90 and HaveBuff("player",69369) then
	print("Casting Healing touch on "..UnitName(lowestPlayer))
	AceCastSpellByName(GetSpellInfo(5185),lowestPlayer)
	end


	if CalculateHP(lowestPlayer) < 75 and  isSpellAvailable(102351) then
	AceCastSpellByName(GetSpellInfo(102351),lowestPlayer)
	end


	if playerHealth < 50 and UnitAffectingCombat("player") and  GetSpellCooldown(61336)==0  then
	AceCastSpellByName(GetSpellInfo(61336))
	end

	if CalculateHP(lowestPlayer) < 60 and  isSpellAvailable(108238) then
	AceCastSpellByName(GetSpellInfo(108238),lowestPlayer)
	end

end
function preHot(force)

if IsLeftShiftKeyDown() or force then
	local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do

	 local target = groupType..i

		--Rejuv--------------
		local finish = 0
		local _,_,_,_,_,_,rej,_,_=UnitBuff(target,GetSpellInfo(774),"","player");
		if rej ~= nil then
			 finish = (rej -GetTime())
		end

		local finishGerm = 0
		local _,_,_,_,_,_,rejGerm,_,_=UnitBuff(target,GetSpellInfo(155777),"","player");
		if rejGerm ~= nil then
			 finishGerm = (rejGerm -GetTime())
		end
		local distance = GetUnitDistance(target) or 50
		--print('Rejuv '..finish)
		--print('RejuvGerm '..finishGerm)
		if  (not HaveBuff(target,774,5,'player') or finish < 4) and distance < 40 and isSpellAvailable(774)    then
			AceCastSpellByName(GetSpellInfo(774),target)
		end

		--Rejuv(Germ)
		if isSpellAvailable(774) and distance < 40 and (not HaveBuff(target,155777,3,'player') or finishGerm < 4 ) then
			AceCastSpellByName(GetSpellInfo(774),target)
		end

	end
end
end

function shouldGenesis()

local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
	local playerCount = 0
	local target = groupType..i
	local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
		if HaveBuff(target,774,5,'player') and targetHealth < 60 and not HaveBuff(target,145518)  then
			playerCount = playerCount + 1
		end
		if playerCount > 2 then
			return true
		else
			return false
		end
	end
end
function getLifeBloomUnitHP()

local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		local playerCount = 0
		local target = groupType..i
		if HaveBuff(target,53563,'player') then
			return target
		else
			return nil
		end
	end
	return nil
end

function druidAttemptArenaKill()
arenaTargets = {"arena1","arena2","arena3","arena4","arena5"}
	for _, target in ipairs(arenaTargets) do
		local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
		if targetHealth < 25 and InLineOfSight(target) == 1 and GetUnitDistance(target) < 40 and not UnitIsDeadOrGhost(target) then
			Druid.damageRotation(target)
		end
	end
end

function slowMelee()
arenaTargets = {"arena1","arena2","arena3","arena4","arena5","target"}
	for _, etarget in ipairs(arenaTargets) do
		if not HaveDebuff(etarget, 102355) and isSpellAvailable(102355) and InLineOfSight(etarget) == 1 and not UnitIsDeadOrGhost(etarget) and GetUnitDistance(etarget) < 10 and IsMeleeDps(etarget) and not HaveBuff(etarget, 1044) and not HaveBuff(etarget,114028) and not HaveBuff(etarget,46924) and not HaveBuff(etarget,96268) and UnitIsEnemy('player',etarget) then
							AceCastSpellByName(tostring(GetSpellInfo(102355)),etarget)
		end
	end
end

--Healing FUnctions-------------------------------------------------------
function rejuvTarget(target,targetHealth,germ)

	local distance = GetUnitDistance(target) or 50
	local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))
	if targetHealth < 99  and  not HaveBuff(target,774,3,'player') or (HaveBuff('player', 114108,8) and targetHealth < 85 ) or HaveBuff('player', 114108,3) and isSpellAvailable(774)  and distance < 40 then
		AceCastSpellByName(GetSpellInfo(774),target)
	end

	------------------------------------------------
	--Rejuv(Germ)
	if talentTierSeven == 21651 and  targetHealth < 99 and isSpellAvailable(774) and not HaveBuff(target,155777,3,'player') and germ  and distance < 40 then
		AceCastSpellByName(GetSpellInfo(774),target)
	end

end
function natureSwiftness(target,targetHealth)

	--Ns HT
	if targetHealth < 35 or ( HaveBuff('player',114108) and targetHealth < 50 ) and isSpellAvailable(132158) and not HaveBuff('player',132158) then
			AceCastSpellByName(GetSpellInfo(132158))
	end

	if HaveBuff('player',132158) then
		--cancelForm()
		AceCastSpellByName(GetSpellInfo(8936),target)
	end
end

function ironBark(target,targetHealth)
	--IronBark
	if targetHealth < 80 and isSpellAvailable(102342) then
			AceCastSpellByName(GetSpellInfo(102342),target)
	end
end

function cenerionWard(target,targetHealth)
--Cenerion Ward
	if targetHealth < 70 and isSpellAvailable(102351)  then
		AceCastSpellByName(GetSpellInfo(102351),target)
	end
end

function wildGrowth(target)
	--WG
	if group.low > 2 and isSpellAvailable(48438) and not HaveBuff(target,48438,1,'player') and not MeleeAroundPlayer()  then
		AceCastSpellByName(GetSpellInfo(48438),target)
	end
end

function forceOfNature(target,targetHealth)
	--FoN
	if targetHealth < 35 and isSpellAvailable(102693) and  GetSpellCharges(102693) > 0 then
		AceCastSpellByName(GetSpellInfo(102693),target)
	end
end

function genesis(target,targetHealth)

	local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))
	local finish = 0
	local _,_,_,_,_,_,rej,_,_=UnitBuff(target,GetSpellInfo(774),"","player");
	if rej ~= nil then
		 finish = (rej -GetTime())
	end

	local finishGerm = 0
	local _,_,_,_,_,_,rejGerm,_,_=UnitBuff(target,GetSpellInfo(155777),"","player");
	if rejGerm ~= nil then
		 finishGerm = (rejGerm -GetTime())
	end

	--Genesis
	if shouldGenesis() or (targetHealth < 40 and finish > 10 and  (finishGerm > 10 or talentTierSeven ~= 21651) and not HaveBuff(target,145518))  and isSpellAvailable(145518) then
		AceCastSpellByName(GetSpellInfo(145518),target)
	end
end
function swiftmendGenesis(target,targetHealth)
	if targetHealth < 50 and not isSpellAvailable(18562) and isSpellAvailable(145518)   and (HaveBuff(target,155777) and HaveBuff(target,774)) and not HaveBuff('player',5487) then
		cancelForm()
		AceCastSpellByName(GetSpellInfo(145518),target)
	end
end
function bearHeal(playerHealth,force)
	if HaveBuff('player',5487) and (HaveBuff('player',774,2,'player') or HaveBuff('player',155777,2,'player')) then
		if UnitPower('player') < 1 then
			AceRunMacroText("/cast !Bear Form")
			return true
		else
			AceRunMacroText("/cast Frenzied Regeneration")
			return true
		end
	else
		if not HaveBuff('player',33891) and not HaveBuff('player',774,2,'player') and not HaveBuff('player',155777,2,'player') and force then
			AceRunMacroText("/cancelform")
			return false
		end
	end

end

function lifeBloom(target,targetHealth)
	--LB
	local lbUnit = getLifeBloomUnitHP() or target;
	local lbHealth =  100 * UnitHealth(lbUnit) / UnitHealthMax(lbUnit)


	if targetHealth < 99 and isSpellAvailable(33763) and (lbHealth - targetHealth > 20 or UnitIsUnit(lbUnit,target)) and not HaveBuff(target,33763,2,'player') and  lastSpellCasted ~= 33763 then --(abs(lbHealth - targetHealth) > 20 or UnitIsUnit(lbUnit,target))
	--if targetHealth < 99 and isSpellAvailable(33763) and not HaveBuff(target,33763,2 ,'player') and (lbHealth - targetHealth > 10) then
		AceCastSpellByName(GetSpellInfo(33763),target)
	end
end

function wildMushroom(target,targetHealth)
	--Wild Mushroom
	if targetHealth < 95 and isSpellAvailable(88747) and (GetTime() - (Druid.InWildMushroom[UnitGUID(target)] or 0) > 3) and not HaveBuff('player',5487) then
		Druid.InWildMushroom[UnitGUID(target)] =GetTime()
		AceCastSpellByName(GetSpellInfo(88747))
		AceRunMacroText("/script ClickArea('"..target.."')")
	end
end

function swiftMend(target,targetHealth)
	--SWiftmend
	if targetHealth < 85 and isSpellAvailable(18562) and (HaveBuff(target,8936) or HaveBuff(target,774)) and not HaveBuff('player',5487) then
		cancelForm()
		AceCastSpellByName(GetSpellInfo(18562),target)
	end
end

function markOfTheWild()
	if not HaveBuff('player',1126)  then
			AceCastSpellByName(GetSpellInfo(1126),'player')
	end
end

function bearFormMelee(target,targetHealth)
	if MeleeAroundPlayer() and playerHealth < 99 and
	HaveBuff('player',774,2,'player') and HaveBuff('player',155777,2,'player') and not HaveBuff('player',5487) and not HaveBuff('player',33891) then
		AceCastSpellByName(5487)
	end
end
function forceRegrowth(target,targetHealth)
	--OverRide Regrowth
	if IsLeftAltKeyDown() or (HaveBuff('player',114108) and not MeleeAroundPlayer() and targetHealth < 85 and not HaveBuff('player',5487)) and isSpellAvailable(8936) and not canInturrupt()  then
			AceCastSpellByName(8936,target)
	end
end
function regrowthFake(target,targetHealth)
	--Regrowth?
	if targetHealth > 80 and isSpellAvailable(8936) and not HaveBuff('player',5487)  then
		AceCastSpellByName(GetSpellInfo(8936),target)
		C_Timer.After(0.50, function()
			local spellName, _, _, _, startCast, endCast, _, _, _ = UnitCastingInfo('player')
			if spellName == 'Regrowth' and canInturrupt()  then AceStopSpellCasting() end
		end)
	end
end

function defaultRegrowth(target,targetHealth)
	--Regrowth?
	if targetHealth < 99 and isSpellAvailable(8936) and not HaveBuff('player',5487)  then
		AceCastSpellByName(GetSpellInfo(8936),target)
	end
end
function cycloneFake(target,targetHealth)
	--Regrowth?d
	if targetHealth > 60 and isSpellAvailable(33786) and canInturrupt()  then
		AceCastSpellByName(GetSpellInfo(33786),TargetNearestEnemy())
		C_Timer.After(0.50, function()
			local spellName, _, _, _, startCast, endCast, _, _, _ = UnitCastingInfo('player')
			if spellName == 'Cyclone' and canInturrupt()  then AceStopSpellCasting() end
		end)
	end
end

function checkHarmony(target)
	if not HaveBuff('player',100977) and not HaveBuff('player',5487)  and not UnitAffectingCombat('player') then
			AceCastSpellByName(GetSpellInfo(8936),target)
	end
end
function tranquility()
	if not MeleeAroundPlayer() and group.veryLow > 2 then
		AceCastSpellByName(GetSpellInfo(740),target)
	end
end
function natureVigil(targetHealth)
	if targetHealth < 40 and isSpellAvailable(124974) then
		AceCastSpellByName(GetSpellInfo(124974),nil)
	end
end
function cancelForm()
	if not HaveBuff('player',33891) and not HaveBuff('player',5487) then
		AceRunMacroText("/cancelform")
	end
end
function treeForm(targetHealth)
	if targetHealth < 30 and not HaveBuff('player',33891) and isSpellAvailable(33891) then
	AceRunMacroText("/cancelform")
		AceCastSpellByName(GetSpellInfo(33891),nil)
	end
end
-------END Healing Functions---------------------
function Druid.healingRotation(target,target2)


local dist = GetUnitDistance(target)
local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local totemSpell = 8921
--targetHealth = 36

if isSpellAvailable(102355) and  HaveBuff('player',5487) then totemSpell = 102355 end

if dist == nil then dist = 50 end

if dist > 40 or UnitIsEnemy('player',target) or UnitIsDeadOrGhost(target)  or not InLineOfSight(target) == 1 then
	return
end

if specid == 105 then


	preHot(false)
	natureVigil(targetHealth)
	natureSwiftness(target,targetHealth)
	forceOfNature(target,targetHealth)
	treeForm(targetHealth)
	if Druid.lockoutTime.active == true then
		bearHeal(playerHealth,false)
		if Druid.lockoutTime.endTime < GetTime() then
			Druid.lockoutTime.active = false
		end
		return
	end
	--bearFormMelee(target,targetHealth)

	if bearHeal(playerHealth,true) == true then return true end

	forceRegrowth(target,targetHealth)
	slowMelee()
	cycloneFake(target,targetHealth)
	checkHarmony(target)
	lifeBloom(target,targetHealth)
	rejuvTarget(target,targetHealth,false)
	swiftMend(target,targetHealth)

	if playerHealth > 35 and targetHealth > 20 then
		Druid.dispellRotation(target,true)
	end

	rejuvTarget(target,targetHealth,true)
	regrowthFake(target,targetHealth)
	wildMushroom(target,targetHealth)
	regrowthFake(target,targetHealth)
	wildGrowth(target,targetHealth)
	genesis(target,targetHealth)




	if targetHealth > 30 then
		druidAttemptArenaKill()
	end

	tranquility()
	markOfTheWild()

	defaultRegrowth(target,targetHealth)
	--if target2 ~= nil then
	--local targetHealth2 = 100 * UnitHealth(target2) / UnitHealthMax(target2)
	preHot(true)
	--end
end

end
function Druid.damageRotation(target)
 local targetHp = 100 * UnitHealth(target) / UnitHealthMax(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
 local specid = select(1, GetSpecializationInfo(GetSpecialization()))
 local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))

 if specid == 105  then

	if HaveBuff('player', 108294) then
		if GetComboPoints("player", target) > 4 and UnitPower("player") >=50
		then
			--print('FB')
			--AceCastSpellByName(tostring(GetSpellInfo(22568)))
		end

		if IsUsableSpell(5221) and GetComboPoints("player", target) <= 4
		then
			--print('s/m')
			 -- AceCastSpellByName(GetSpellInfo(5221))
		end
	else

	if not HaveDebuff(target,8921) and isSpellAvailable(8921) then
		AceCastSpellByName(GetSpellInfo(8921),target)
	end
	if isSpellAvailable(5176)  and HaveDebuff(target,8921) then
		AceCastSpellByName(GetSpellInfo(5176),target)
	end

	end


 end
 if specid == 103 then

		local finish = 0;
		local _,_,_,_,_,_,SR,_,_=UnitBuff("player","Savage Roar","","");
		if SR ~= nil then
		 finish = (SR -GetTime())
		end
		local catform = 768
		if talentTierSeven == 21653 then
			catform = 171745
		end
		Druid.dispellRotation(target)

		if playerHealth < 95 and isSpellAvailable(774) and not HaveBuff('player',774) then
			AceCastSpellByName(GetSpellInfo(774),target)
		end


		if UnitAffectingCombat("player") and (not UnitBuff("player","Cat Form") and not UnitBuff("player","Bear Form")) then
			--print('CF')
			AceCastSpellByName(GetSpellInfo(catform))
		end

		if not UnitAffectingCombat("player")
		and not UnitBuff("player","Travel Form")
		and not UnitBuff("player","Incarnation: King of the Jungle")
		and not HaveBuff("player",5215)
		then
		--print('stelathgo')
		AceCastSpellByName(GetSpellInfo(5215))
		end


		local powerType, powerToken, altR, altG, altB = UnitPowerType("player")

		if UnitPower("player") < 35
		and (IsSpellInRange(GetSpellInfo(5221),"target") == 1 )
		and powerType == 3
		then
		--print('TF')
		AceCastSpellByName(GetSpellInfo(5217)) end



		if not HasSR('player') and not HaveBuff("player",5215)
		and GetComboPoints("player", "target") <= 3
		and finish < 3
		then
		--print('SR')
		 AceCastSpellByName(GetSpellInfo(52610))
		end



		if GetComboPoints("player", target) > 4 and UnitDebuff(target,"Rip","","player")
		and UnitPower("player") >=50
		then
		--print('FB')
		AceCastSpellByName(tostring(GetSpellInfo(22568)),target)
		end

		--Rip
		local finishRT = nil;
		local _,_,_,_,_,_,RT,_,_=UnitDebuff("target","Rip","","player");
		if RT ~= nil then
		 finishRT = (RT -GetTime())
		end
		if GetComboPoints("player", target) > 4 and not UnitDebuff(target,"Rip","","player") and (finishRT == nil or finishRT < 3)
		and targetHp > 20
		then
		--print('rip')
		AceCastSpellByName(GetSpellInfo(1079),target)
		end


		--Rake
		local finishRA = nil;
		local _,_,_,_,_,_,RA,_,_=UnitDebuff(target,"Rake","","player");
		if RA ~= nil then
		 finishRA = (RA -GetTime())
		end
		if not HaveDebuff(target,1822,2,"PLAYER")
		and GetComboPoints("player", target) < 5  and (finishRA == nil or finishRA < 3)
		then
		--print('rake')
		AceCastSpellByName(GetSpellInfo(1822),target)
		end



		 --Shred/Mangle
		if IsUsableSpell(5221) and GetComboPoints("player",target) <= 4
		then
		--print('s/m')
		  AceCastSpellByName(GetSpellInfo(5221),target)
		end
end
if specid == 102 then

local direction = GetEclipseDirection()

if not HaveDebuff(target,8921) and isSpellAvailable(8921) and direction == 'moon' then
	AceCastSpellByName(GetSpellInfo(8921),target)
end

if not HaveDebuff(target,164815) and isSpellAvailable(164815) and direction == 'sun' then
	AceCastSpellByName(GetSpellInfo(164815),target)
end

if HaveBuff('player',164547) then
	AceCastSpellByName(GetSpellInfo(2912),target)
end

if HaveBuff('player',164545) then
	AceCastSpellByName(GetSpellInfo(5176),target)
end

if isSpellAvailable(78674) then
	AceCastSpellByName(GetSpellInfo(78674),target)
end


if direction == 'moon' then
	AceCastSpellByName(GetSpellInfo(2912),target)
else
	AceCastSpellByName(GetSpellInfo(5176),target)
end

end

end
function cancelCC()
	local tars = {"arena1","arena2","arena3","arena4","arena5","target"}
	local spellName, _, _, _, startCast, endCast, _, _, _ = UnitCastingInfo('player')
	if spellName == 'Cyclone' or spellName == 'Entangling Roots' then
		for _, etarget in ipairs(tars) do
			if HaveBuff(114028,etarget) then
				AceStopSpellCasting()
			end
		end
	end
end
function RotationTickDruid()

local _,UnitClass=UnitClass("player")
	if UnitClass~="DRUID" then
	return
	end
local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")
local lowestPlayer,lowestTank = GetGroupInfo()
local secondLowestPlayer,secondLowestTank

if lowestPlayer[2] then secondLowestPlayer = lowestPlayer[2].Unit end



local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v) end

local specid = select(1, GetSpecializationInfo(GetSpecialization()))

--if morph ~= nil then	morph(0,0,56) end
stealthFinder(8921)
cancelCC()
local avoid = Druid.avoidSpellInturupt()

if GetKeyState(4) then 
local targetHealth = 100 * UnitHealth(lowestPlayer[1].Unit) / UnitHealthMax(lowestPlayer[1].Unit)
	ironBark(lowestPlayer[1].Unit,targetHealth)
end
if GetKeyState(HEALING_KEY)  and  not HaveBuff("player",5215) and  avoid == false then

		Druid.survivalRotation(lowestPlayer[1].Unit)
		Druid.healingRotation(lowestPlayer[1].Unit,secondLowestPlayer)
end

	if GetKeyState(DAMAGE_KEY)  and hasTarget and InLineOfSight('target') == 1 then
		Druid.damageRotation('target')
	end
end



