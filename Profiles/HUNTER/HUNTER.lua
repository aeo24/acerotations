addonName, LURS = ...
LURS.hunter = {}
LURS.hunter.ROTATION = function()
	
--- Si estas variables las pongo fuera del frame no las toma.

local playerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local focus = UnitPower("player", 2)
local SpellRange = LibStub("SpellRange-1.0")
local inRange = SpellRange.IsSpellInRange("Golpe de tormenta", "target")
local solo1PDS = 0
local weakesttargetHP = 0
local nexttargetHP = UnitHealth("target")
local victimaEncontrada = 0
local victima = 0
local victimaHP = 0
Time = GetTime()


-- BOTON 1
	
	if ActionButton1:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then
				
		if GetSpellCooldown("Trampa de hielo") == 0 then
		oinfo("target")
		oface("target")
		oexecute("CastSpellByName('Trampa de hielo')")
		oclick("target")
		print("3. Trampa de hielo")

		end
		
		if GetSpellCooldown("Disparo de conmoción") == 0 then
			oface("target")
			oexecute("CastSpellByName('Disparo de conmoción')")
			print("3. Disparo de conmoción")
		end		
			
						
		
		
		
	end
	
	-- BOTON 2

	if ActionButton2:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") and UnitExists("target") then
	
		if LastActionTime == nil or Time - LastActionTime > 0.500 then
		
		LastActionTime = GetTime()
		
			if PDSTime == nil 
			or Time - PDSTime > 1.6 then
		
					if focus >= 15
					and targetHP > 20
					and UnitDebuff("target","Marca del cazador")
					and not UnitDebuff("target","Picadura de serpiente")
					then
					oface("target")
					oexecute("CastSpellByName('Picadura de serpiente')")
					print("2. Picadura de serpiente")
					PDSTime = GetTime()
					end
			end
		
			if targetHP <= 20
			and focus >= 40
			and GetSpellCooldown("Disparo mortal") == 0 then
			oface("target")
			oexecute("SpellStopCasting()")
			oexecute("CastSpellByName('Disparo mortal')")
			print("3. Disparo mortal")
			
			elseif focus >= 30
			and IsSpellInRange("Disparo arcano","target") == 1
			and GetSpellCooldown("Disparo arcano") == 0 
			and not UnitDebuff("target","Marca del cazador") then
			oface("target")
			oexecute("PetAttack('target')")
			oexecute("CastSpellByName('Disparo arcano')")
			
			print("1. Disparo arcano")
					
			elseif UnitExists("pet")			
			and focus >= 40	
			and GetSpellCooldown("Matar") == 0 then
			oface("target")
			oexecute("CastSpellByName('Matar')")
			print("3. Matar")
			
			elseif GetSpellCooldown("Disparo de conmoción") == 0 then
			oface("target")
			oexecute("CastSpellByName('Disparo de conmoción')")
			print("3. Disparo de conmoción")
						
			elseif focus < 30
			and GetSpellCooldown("Disparo firme") == 0 
			and IsCurrentSpell("Disparo firme") == nil then
			oface("target")
			oexecute("CastSpellByName('Disparo firme')")
			
			print("3. Disparo firme")			
			
			elseif focus >= 55
			and IsSpellInRange("Veneno de viuda","target") == 1
			and GetSpellCooldown("Veneno de viuda") == 0
			and UnitDebuff("target","Picadura de serpiente")
			and not UnitDebuff("target","Veneno de viuda") then
			oface("target")
			oexecute("CastSpellByName('Veneno de viuda')")
			
			elseif focus >= 40
			and IsSpellInRange("Disparo arcano","target") == 1
			and GetSpellCooldown("Disparo arcano") == 0  then
			oface("target")
			oexecute("PetAttack('target')")
			oexecute("CastSpellByName('Disparo arcano')")

			end
					
		
		end	
		
	end
		
	
	-- BOTON 3
	
	if ActionButton3:GetButtonState() == 'PUSHED' then
	
		if GetSpellCooldown("Trampa explosiva") == 0 then
			oinfo("target")
			oface("target")
			oexecute("CastSpellByName('Trampa explosiva')")
			oclick("target")
			print("3. Trampa explosiva")
		
		end
		
	end  
	
	if ActionButton4:GetButtonState() == 'PUSHED' then
	
		print(GetShapeshiftForm())
		oexecute("CastShapeshiftForm(4)")
		oexecute("CastSpellByName('Rugido de estampida')")
		
				
	end
		
-- HP BAJO
	
	if UnitIsDeadOrGhost("player") == nil and not UnitHasVehicleUI("player") and not IsMounted() then 
	
		if playerHP < 50 
		and GetSpellCooldown("Ofrenda de los naaru") == 0 then
		print("Ofrenda de los naaru")
		oexecute("CastSpellByName('Ofrenda de los naaru')")
		end
		
	end	


-- Automatico

	if UnitExists("pet") and UnitAffectingCombat("pet") then
		if UnitHealth("pet")/UnitHealthMax("pet") <0.60 and not UnitBuff("pet","Aliviar mascota") then
		oexecute("CastSpellByName('Aliviar mascota')")
		end
	end
	
	if UnitIsDeadOrGhost("player") == nil 
	and not UnitHasVehicleUI("player")
	and UnitAffectingCombat("player")
	and not IsMounted() then 
	
		if select(4, UnitBuff("player", "Frenesí")) == 5
		then
		print("Enfocar fuego")
		oexecute("CastSpellByName('Enfocar Fuego')")
		end
			
		if UnitExists('target') then
			local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
			if name and not notInterruptible and GetSpellCooldown("Contradisparo") == 0 and IsCurrentSpell("Contradisparo") == nil then
				if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
					oexecute("SpellStopCasting()")
					oexecute("CastSpellByName('Contradisparo')")
					print("SILENCIADO!")
				end
			end
		end
		
		
		  
	end
end