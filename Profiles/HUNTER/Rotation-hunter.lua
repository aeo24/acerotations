addonName, Rotations = ...
Rotations.hunter = {}
Rotations.hunter.ROTATION = function()
	RotationTickHunter()
	return false

end
Hunter = {}
oX = 0;
oY = 0;
oZ = 0;
function PredictTrapPosition2(unit)
    unit = unit or 'target'
	local framerate = GetFramerate();
	local guid = UnitGUID(unit)
    local x1, y1, _,_,_ = GetUnitPosition(unit)

	local startTime = GetTime()
    C_Timer.After(.1, function()
        if UnitExists(unit) then
			local x2, y2,z,_,_ = GetUnitPosition(unit)
			local px, py,_,_,_ = GetUnitPosition('player')

			local endTime = GetTime()
			local dx,dy,dz
			dx = (x2-x1) / (endTime - startTime)
			dy = (y2-y1) / (endTime - startTime)
			dz = 0
			local distance = math.sqrt((x2 - px)^2 + (y2 - py)^2)
			local flightTime  = distance / 25 * cos((30 * 3.1459) /180)

			local angle = atan2(x2-x1, y2-y1)
			--local x = x2 + cos(angle) * trapSpeed
			--local y = y2 + sin(angle) * trapSpeed
			local x = x2 + dx * flightTime
			local y = y2 + dy * flightTime

			local newz = z;
			while InLineOfSight(x,y,z,x,y,newz,4) == 0 do
				newz = newz - 5;
			end
			RunMacroText("/script ClickPosition("..x..","..y..","..newz..")")
		end
    end)

end


function Hunter.FreezingTrap(target)
	if not HaveDebuff(target,33786) and UnitExists(target) and checkDR(target,1499) < 2 then --and UnitIsEnemy('player',target) then
		AceCastSpellByName(tostring(GetSpellInfo(1499)))
		PredictTrapPosition2(target)
	end
end

function Hunter.ExplosiveTrap(target)
	if not HaveDebuff(target,33786) and  UnitExists(target) and  UnitIsEnemy('player',target) then
		AceCastSpellByName(tostring(GetSpellInfo(13813)))
		PredictTrapPosition2(target)
	end
end

function Hunter.BindingShot(target)
	if not HaveDebuff(target,33786) and  UnitExists(target) and checkDR(target,109248) < 2 then
		AceCastSpellByName(tostring(GetSpellInfo(109248)))
		 AceRunMacroText("/script ClickArea('"..target.."')")
	end
end
function Hunter.avoidSpellInturupt()

if not hfooframe then
	hfooframe = CreateFrame("Frame")
    end
	local interruptID =     {
    [102060] = true,    --Disrupting Shout
    [106839] = true,    --Skull Bash
    [80964] = true,        --Skull Bash
    [115781] = true,    --Optical Blast
    [116705] = true,    --Spear Hand Strike
    [1766] = true,         --Kick
    [19647] = true,     --Spell Lock
    [2139] = true,         --Counterspell
    [47476] = true,        --Strangulate
    [47528] = true,     --Mind Freeze
    [57994] = true,     --Wind Shear
    [6552] = true,         --Pummel
    [96231] = true,     --Rebuke
    [31935] = true,        --Avenger's Shield
    [34490] = true,     --Silencing Shot
    [147362] = true     --Counter shot
    }

	local SIN_PlayerGUID = UnitGUID("player")
    hfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    hfooframe:SetScript("OnEvent", function(self, event, _, type, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)

    end);
end
function chooseInturrupt(etarget)
	local start,dur,_ = GetSpellCooldown(147362)
	local counterCD = start + dur - GetTime()

	local startF,durF,_ = GetSpellCooldown(1499)
	local freezeCD = startF + durF - GetTime()

	if isSpellAvailable(147362) then
		AceCastSpellByName(GetSpellInfo(147362),etarget)
	end
	if counterCD < 20 and counterCD > 0 and  isSpellAvailable(1499) and checkDR(etarget,1499) < 2 then
		AceCastSpellByName(tostring(GetSpellInfo(1499)))
					AceRunMacroText("/script ClickArea('"..etarget.."')")
	end
	if counterCD < 20 and counterCD > 0 and freezeCD < 10 and freezeCD > 0 and  isSpellAvailable(13813) then
		AceCastSpellByName(tostring(GetSpellInfo(13813)))
					AceRunMacroText("/script ClickArea('"..etarget.."')")
	end
end
function Hunter.dispellRotation(target)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')



local purgeList = {
1044, -- Hand of Freedom
6940, -- Hand of Sacrifice
69369, -- Predatory Swiftness
12472, -- Icy Veins
1022, -- Hand of Protection
11426, -- Ice Barrier
20925, -- Sacred Shield
--114250, -- Selfless Healer
17, -- Power Word: Shield
152118, --CoW
12043,
132158,
16188,
110909,
6346,
974,774,155777,61295,124682,115151
}


 local polys = {118,28271,28272,61721,61780,126819,161354,161355,161372,33786,5782,118699,51514,80240,20066} -- and cyclone

	if not enemyTargets
	then
		enemyTargets = {"target","arena1","arena2","arena3","arena4","arena5","focus"}
	end

   for _,etarget in pairs(enemyTargets) do

	if HaveBuff(etarget, 8177) or HaveBuff(etarget,41538) then
		AceCastSpellByName(tostring(GetSpellInfo(2649),etarget))
	end
	local start,dur,_ = GetSpellCooldown(147362)
	local counterCD = start + dur - GetTime()
	local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
	for _, v in ipairs(healingInterrupt) do
	   if GetSpellInfo(v) == spellName and canInterrupt == false then
			local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
			local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
			local castTime = endCast - startCast
			local currentPercent = timeSinceStart / castTime * 100000
			if InLineOfSight(etarget) == 1 and currentPercent > 25  and GetUnitDistance(etarget) < 40 and targetHealth < 70  then
				chooseInturrupt(etarget)
			end
	   end
	end
	for _, v in ipairs(polys) do
	   if GetSpellInfo(v) == spellName and canInterrupt == false then
			local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
			local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
			local castTime = endCast - startCast
			local currentPercent = timeSinceStart / castTime * 100000
			if InLineOfSight(etarget) == 1 and currentPercent > 25  and GetUnitDistance(etarget) < 40 then
				AceCastSpellByName(GetSpellInfo(147362),etarget)
			end
	   end
	end

	local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
	for _, v in ipairs(channelInturrupt) do
		if GetSpellInfo(v) == spellName and canInterrupt == false then
			local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
			local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
			local castTime = endCast - startCast
			local currentPercent = timeSinceStart / castTime * 100000
			if InLineOfSight(etarget) == 1 and currentPercent > 15  and GetUnitDistance(etarget) < 40 and targetHealth < 70 then
					chooseInturrupt(etarget)
				end
			end
	end

	if HaveBuff(etarget, 1022) or HaveBuff(etarget,6940) and isSpellAvailable(19801) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) then
		AceCastSpellByName(tostring(GetSpellInfo(19801),etarget))
	end

	for _,buff in pairs(purgeList) do
		if HaveBuff(etarget, buff) and isSpellAvailable(19801) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 40 and UnitIsEnemy('player',etarget) and targetHealth < 50 then
			AceCastSpellByName(tostring(GetSpellInfo(19801),etarget))
		end
	end
 end
end

function Hunter.survivalRotation()
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

if not HaveBuff('player',77769) then
	AceCastSpellByName(GetSpellInfo(77769))
end

if playerHealth < 70 and UnitAffectingCombat('player') and  isSpellAvailable(5512) then
				   AceRunMacroText('/use Healthstone')
end


end

function Hunter.damageRotation()

local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')

local focus = UnitPower('player')

local specid = select(1, GetSpecializationInfo(GetSpecialization()))

if IsChanneling() then
	return
end

if isSpellAvailable(3044) and focus > 60 then
	findTotems(3044)
end

if not HaveBuff('player',5118) then
	AceCastSpellByName(GetSpellInfo(5118))
end
	if specid == 255 then

--Cast Black Arrow  on cooldown.

if isSpellAvailable(3674) then
	AceCastSpellByName(GetSpellInfo(3674))
end
AceRunMacroText("/petattack")
--Cast Explosive Shot  on cooldown

if isSpellAvailable(53301) then
	AceCastSpellByName(GetSpellInfo(53301))
end

--Use A Murder of Crows or Stampede , depending on your talent choice.
if isSpellAvailable(131894) then
	AceCastSpellByName(GetSpellInfo(131894))
end

--Use Dire Beast , if you have chosen this talent.
if isSpellAvailable(120679) then
	AceCastSpellByName(GetSpellInfo(120679))
end
if isSpellAvailable(109259) then
	AceCastSpellByName(GetSpellInfo(109259))
end
if isSpellAvailable(120360) then
	AceCastSpellByName(GetSpellInfo(120360))
end
--Use Barrage or Glaive Toss, depending on your talent choice.
if isSpellAvailable(117050) then
	AceCastSpellByName(GetSpellInfo(117050))
end
--Cast Arcane Shot to dump your Focus or refresh serpent sting
if isSpellAvailable(3044) and focus > 60 or not HaveDebuff('target',87935,4,'player') or (HaveBuff('player',109306) and focus > 35) then
	AceCastSpellByName(GetSpellInfo(3044))
end
--Cast Cobra Shot Icon Cobra Shot to generate Focus.
if isSpellAvailable(77767) and focus < 60 then
	AceCastSpellByName(GetSpellInfo(77767))
end
end

if specid == 253 then

AceRunMacroText("/petattack")
local start, duration, enabled = GetSpellCooldown(19574);
local BWtimeleft = (start + duration - GetTime())

--focusfire frenzy > 4
 local _,_,_,frenzyCount,_,_,_,_,_=UnitBuff('player',GetSpellInfo(19623),"","");
 if frenzyCount == nil then frenzyCount = 0 end
 if (isSpellAvailable(82692) and frenzyCount > 4 and (isSpellAvailable(19574) or BWtimeleft > 30 )) or (HaveBuff('player',19574) and frenzyCount > 2) and not HaveBuff('player',82692) then
  if isSpellAvailable(19574) then
	AceCastSpellByName(GetSpellInfo(19574))
  end
	AceCastSpellByName(GetSpellInfo(82692))
 end

--Use Kill Shot when under 20%.
if isSpellAvailable(53351) and targetHealth < 21 then
	AceCastSpellByName(GetSpellInfo(53351))
end



if HaveBuff('player',19574) then
	if isSpellAvailable(26297) then
		AceRunMacroText("/cast Berserking")
	end
	--Use A Murder of Crows or Stampede , depending on your talent choice.
	if isSpellAvailable(131894) then
		AceCastSpellByName(GetSpellInfo(131894))
	end
	--Dire Beast
	if isSpellAvailable(120679) then
		AceCastSpellByName(GetSpellInfo(120679))
	end


end

--Use Kill Command.
if isSpellAvailable(34026) then
	AceCastSpellByName(GetSpellInfo(34026))
end





if isSpellAvailable(136) and not HaveBuff('pet', 136) and targetHealth > 40 and not HaveBuff('pet',19574) then
  AceCastSpellByName(GetSpellInfo(136))
 end

--Barrage
if isSpellAvailable(120360) and BWtimeleft > 20 then
	AceCastSpellByName(GetSpellInfo(120360))
end
--[[if isSpellAvailable(120360) then
	AceCastSpellByName(GetSpellInfo(120360))
end
if isSpellAvailable(109259) then
	AceCastSpellByName(GetSpellInfo(109259))
end]]--
--Use Barrage or Glaive Toss, depending on your talent choice.

--Cast Arcane Shot to dump your Focus or refresh serpent sting
if isSpellAvailable(3044) and (focus > 60 and not isSpellAvailable(120360)) or (HaveBuff('player',109306) and focus > 35) then
	AceCastSpellByName(GetSpellInfo(3044))
end
--Cast Cobra Shot Icon Cobra Shot to generate Focus.
if isSpellAvailable(77767) and focus < 60 then
	AceCastSpellByName(GetSpellInfo(77767))
end

end

end

function RotationTickHunter()

local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")

local _,UnitClass=UnitClass("player")
if UnitClass~="HUNTER" then
	return
end


if IsLeftShiftKeyDown() then
local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v) end
end

	if GetKeyState(DAMAGE_KEY) and hasTarget and InLineOfSight('target') == 1 then
		Hunter.survivalRotation()
		Hunter.avoidSpellInturupt()
		Hunter.dispellRotation()
		Hunter.damageRotation()
	end
end



