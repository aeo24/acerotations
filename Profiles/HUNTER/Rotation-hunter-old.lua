addonName, Rotations = ...
Rotations.hunter = {}
Rotations.hunter.ROTATION = function()
	RotationTickHunter()
	return false

end
Hunter = {}
function getGCD()
	  local _, playerClass = UnitClass("player")

                if playerClass == "DEATHKNIGHT" then
                        return 52375
                elseif playerClass == "DRUID" then
                        return 774
                elseif playerClass == "HUNTER" then
                        return 56641
                elseif playerClass == "MAGE" then
                        return 1459
                elseif playerClass == "PALADIN" then
                        return 85256
                elseif playerClass == "PRIEST" then
                        return 2050
                elseif playerClass == "ROGUE" then
                        return 1752
                elseif playerClass == "SHAMAN" then
                        return 45284
                elseif playerClass == "WARLOCK" then
                        return 980
                elseif playerClass == "WARRIOR" then
                        return 1715
                elseif playerClass == "MONK" then
                        return 100780
                else
                        return 0
                end
 end
function isSpellAvailable(SpellID)
	local GCDSpell=getGCD()
	if GetSpellCooldown ~= nil then
		local GCDstartTime,GCDduration=GetSpellCooldown(GCDSpell)
		local startTime,duration,enabled=GetSpellCooldown(SpellID)
		local _,_,_,spellCost=GetSpellInfo(SpellID)
		local spellUsable=IsUsableSpell(SpellID)
		SpellAvailable="false"
		if startTime~=nil and GCDstartTime~=nil then
			local timeLeft=startTime+duration-GetTime()
			local GCDtimeLeft=GCDstartTime+GCDduration-GetTime()
			if GCDtimeLeft<=0 then
				if timeLeft<=.25 then
					if spellUsable~=nil then
						SpellAvailable="true"
					end
				end
			else
				if timeLeft<=GCDtimeLeft+0.25 then
					if spellUsable~=nil then
						SpellAvailable="true"
					end
				end
			end
			else
				SpellAvailable="false"
			end
		else
			SpellAvailable="false"
		end
		--print(GetSpellInfo(SpellID))
		--print(SpellAvailable)
		if SpellAvailable==nil or SpellAvailable=="false" then
			return false
		else
			return true
		end
end

function spellReady(spell)
                if IsSpellKnown(spell) and IsUsableSpell(spell) and isSpellAvailable(spell) then
                                return true
                end
                return false;
end




function HaveBuff(UnitID,SpellID,TimeLeft,Filter)
  if not TimeLeft then TimeLeft = 0 end
  if type(SpellID) == "number" then SpellID = { SpellID } end
  for i=1,#SpellID do
    local spell, rank = GetSpellInfo(SpellID[i])
    if spell then
      local buff = select(7,UnitBuff(UnitID,spell,rank,Filter))
      if buff and ( buff == 0 or buff - GetTime() > TimeLeft ) then return true end
      end
  end
end


function HaveDebuff(UnitID,SpellID,TimeLeft,Filter)
  if not TimeLeft then TimeLeft = 0 end
  if type(SpellID) == "number" then SpellID = { SpellID } end
  for i=1,#SpellID do
    local spell, rank = GetSpellInfo(SpellID[i])
    if spell then
      local debuff = select(7,UnitDebuff(UnitID,spell,rank,Filter))
      if debuff and ( debuff == 0 or debuff - GetTime() > TimeLeft ) then return true end
     end
  end
end


function Hunter.dispellRotation(target)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')

local healingInterrupt = {
 5185, -- healing touch
 8936, -- regrowth
 50464, -- nourishB
 331, -- healing wave
 1064, -- Chain Heal        (cast)
 77472, -- Greater Healing Wave    (cast)
 8004, -- Healing Surge        (cast)
 73920, -- Healing Rain        (cast)
 8936, -- Regrowth        (cast)
 2061, -- Flash Heal        (cast)
 2060, -- Greater Heal        (cast)
 5185, -- Healing Touch        (cast)
 596, -- Prayer of Healing    (cast)
 19750, -- Flash of Light    (cast)
 635,8004,47540,115175,33076,85673,8936 -- Holy Light        (cast)

}

local purgeList = {
1044, -- Hand of Freedom
6940, -- Hand of Sacrifice
69369, -- Predatory Swiftness
12472, -- Icy Veins
1022, -- Hand of Protection
11426, -- Ice Barrier
20925, -- Sacred Shield
--114250, -- Selfless Healer
17, -- Power Word: Shield
152118, --CoW
12043,
132158,
16188,
110909,
6346,
974,774,155777
}

	if not enemyTargets
	then
		enemyTargets = {"target","arena1","arena2","arena3","arena4","arena5"}
	end



	for _,etarget in pairs(enemyTargets) do

		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
		for _, v in ipairs(healingInterrupt) do
			if GetSpellInfo(v) == spellName and canInterrupt == false then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				if InLineOfSight(etarget) == 1 and currentPercent > 25  and targetHealth < 70 and isSpellAvailable(147362)  then
						--print(GetSpellInfo(v) )
						if isSpellAvailable(147362)  then
						AceCastSpellByName(GetSpellInfo(147362),etarget)
						end
						print(isSpellAvailable(1499))
						if isSpellAvailable(1499) and not isSpellAvailable(147362)  then
								 AceCastSpellByName(tostring(GetSpellInfo(1499)))
								 AceRunMacroText("/script ClickArea('"..etarget.."')")
						end
				end
			end
		end

		for _,buff in pairs(purgeList) do
			if HaveBuff(etarget, buff) and isSpellAvailable(19801) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) and playerHealth > 50 then
				 AceCastSpellByName(tostring(GetSpellInfo(19801),etarget))
			end
		end
	end

end




function Hunter.survivalRotation()
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')


end

function Hunter.damageRotation()

local focus = UnitPower('player')
--Cast Black Arrow  on cooldown.

	if isSpellAvailable(3674) then
		AceCastSpellByName(GetSpellInfo(3674))
	end

	--Cast Explosive Shot  on cooldown

	if isSpellAvailable(53301) then
		AceCastSpellByName(GetSpellInfo(53301))
	end

	--Use A Murder of Crows or Stampede , depending on your talent choice.
	if isSpellAvailable(131894) then
		AceCastSpellByName(GetSpellInfo(131894))
	end

	--Use Dire Beast , if you have chosen this talent.
	if isSpellAvailable(120679) then
		AceCastSpellByName(GetSpellInfo(120679))
	end
	--Use Barrage or Glaive Toss, depending on your talent choice.
	if isSpellAvailable(117050) then
		AceCastSpellByName(GetSpellInfo(117050))
	end
	--Cast Arcane Shot to dump your Focus or refresh serpent sting
	if isSpellAvailable(3044) and focus > 70 or not HaveDebuff('target',87935,4,'player') then
		AceCastSpellByName(GetSpellInfo(3044))
	end
	--Cast Cobra Shot Icon Cobra Shot to generate Focus.
	if isSpellAvailable(77767) and focus < 70 then
		AceCastSpellByName(GetSpellInfo(77767))
	end

	--focusfire frenzy > 4
	local _,_,_,frenzyCount,_,_,_,_,_=UnitBuff(target,GetSpellInfo(19623),"","");
	if frenzyCount == nil then frenzyCount = 0 end
	if isSpellAvailable(82692) and frenzyCount > 4 then
		AceCastSpellByName(GetSpellInfo(82692))
	end

	if isSpellAvailable(136) and not HaveBuff('pet', 136) then
		AceCastSpellByName(GetSpellInfo(136))
	end

end


function RotationTickHunter()

local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")

local _,UnitClass=UnitClass("player")
if UnitClass~="HUNTER" then
	return
end

if IsLeftShiftKeyDown() then
local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v) end
end

	if GetKeyState('3') and hasTarget and InLineOfSight('target') == 1 then
		Hunter.survivalRotation()
		Hunter.dispellRotation('target')
		Hunter.damageRotation()
	end
end



