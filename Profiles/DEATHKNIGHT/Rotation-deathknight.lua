addonName, Rotations = ...
Rotations.deathknight = {}
Rotations.deathknight.ROTATION = function()
	RotationTickDeathKnight()
	return false

end
DeathKnight = {}



function RuneCheck()
	local FrostRune = 0
	local UnholyRune = 0
	local BloodRune = 0
	local DeathRune = 0
	for i=1, 6 do
	  if GetRuneType(i) == 1 and select(1,GetRuneCooldown(i)) + select(2,GetRuneCooldown(i)) - GetTime() < 1 then
		 BloodRune = BloodRune + 1
	  end
	  if GetRuneType(i) == 2 and select(1,GetRuneCooldown(i)) + select(2,GetRuneCooldown(i)) - GetTime() < 1 then
		 UnholyRune = UnholyRune + 1
	  end
	  if GetRuneType(i) == 3 and select(1,GetRuneCooldown(i)) + select(2,GetRuneCooldown(i)) - GetTime() < 1 then
		 FrostRune = FrostRune + 1
	  end
	  if GetRuneType(i) == 4 and select(1,GetRuneCooldown(i)) + select(2,GetRuneCooldown(i)) - GetTime() < 1 then
		 DeathRune = DeathRune + 1
	  end
	end
	return BloodRune, UnholyRune, FrostRune, DeathRune
end



function DeathKnight.avoidSpellInturupt()

if not dkfooframe then
	dkfooframe = CreateFrame("Frame")
    end
	local interruptID =     {
    [102060] = true,    --Disrupting Shout
    [106839] = true,    --Skull Bash
    [80964] = true,        --Skull Bash
    [115781] = true,    --Optical Blast
    [116705] = true,    --Spear Hand Strike
    [1766] = true,         --Kick
    [19647] = true,     --Spell Lock
    [2139] = true,         --Counterspell
    [47476] = true,        --Strangulate
    [47528] = true,     --Mind Freeze
    [57994] = true,     --Wind Shear
    [6552] = true,         --Pummel
    [96231] = true,     --Rebuke
    [31935] = true,        --Avenger's Shield
    [34490] = true,     --Silencing Shot
    [147362] = true     --Counter shot
    }

	local SIN_PlayerGUID = UnitGUID("player")
    dkfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    dkfooframe:SetScript("OnEvent", function(self, event, _, type, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)

    end);
end

function DeathKnight.dispellRotation(target,asap)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

local targetHealth = CalculateHP(target)

local healingInterrupt = {
 5185, -- healing touch
 8936, -- regrowth
 50464, -- nourishB
 331, -- healing wave
 1064, -- Chain Heal        (cast)
 77472, -- Greater Healing Wave    (cast)
 8004, -- Healing Surge        (cast)
 73920, -- Healing Rain        (cast)
 8936, -- Regrowth        (cast)
 2061, -- Flash Heal        (cast)
 2060, -- Greater Heal        (cast)
 5185, -- Healing Touch        (cast)
 596, -- Prayer of Healing    (cast)
 19750, -- Flash of Light    (cast)
 635,8004,47540,115175,33076,85673,8936 -- Holy Light        (cast)

}

local purgeList = {
1044, -- Hand of Freedom
6940, -- Hand of Sacrifice
69369, -- Predatory Swiftness
12472, -- Icy Veins
1022, -- Hand of Protection
11426, -- Ice Barrier
20925, -- Sacred Shield
--114250, -- Selfless Healer
17, -- Power Word: Shield
152118, --CoW
12043,
132158,
16188,
110909,
6346,
974,774,155777
}

local asapList = {12043,
132158,
16188, 1022,6940}

local channelInturrupt = {
1120, -- Drain Soul		(channeling cast)
12051, -- Evocation		(channeling cast)
115294, -- Mana Tea		(channeling cast)
115175, -- Soothing Mist	(channeling cast)
64843, -- Divine Hymn		(channeling cast)
64901,47540,755 -- Hymn of Hope		(channeling cast)

 }

local InterruptSpells = { 2637,1513 }

local polys = {118,28271,28272,61721,61780,126819,161354,161355,161372,33786,2637,1513}

	if not enemyTargets
	then
		enemyTargets = {"target","arena1","arena2","arena3","arena4","arena5"}
	end
	for _,etarget in pairs(enemyTargets) do
		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
			for _, v in ipairs(healingInterrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				if InLineOfSight(etarget) == 1 and currentPercent > 25  and targetHealth < 70 and isSpellAvailable(47528)
					then
						--print(GetSpellInfo(v) )
						AceCastSpellByName(GetSpellInfo(47528),etarget)
					end
				end
			end

			local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
			for _, v in ipairs(channelInturrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				if InLineOfSight(etarget) == 1 and currentPercent > 15  and targetHealth < 70 and isSpellAvailable(47528)
					then
						--print(GetSpellInfo(v) )
						AceCastSpellByName(GetSpellInfo(47528),etarget)
					end
				end
			end

			for _,buff in pairs(purgeList) do
			if HaveBuff(etarget, buff) and isSpellAvailable(45477) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) and HaveBuff('player',59052) then
				 AceCastSpellByName(tostring(GetSpellInfo(45477),etarget))
			end

			if HaveBuff(etarget, 1022) or HaveBuff(etarget,6940) and isSpellAvailable(45477) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) then
				 AceCastSpellByName(tostring(GetSpellInfo(45477),etarget))
			end

			for _, v in ipairs(polys) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then

				if InLineOfSight(etarget) == 1 and isSpellAvailable(77606) then
						print(GetSpellInfo(v) )
						AceCastSpellByName(GetSpellInfo(77606),etarget)
					end
				end
			end

		end
	end

end

function DeathKnight.survivalRotation()
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
local runicPower  = UnitPower("player");
if runicPower == nil then runicPower = 0 end

if playerHealth < 70 and UnitAffectingCombat('player') and  isSpellAvailable(5512) then
				   AceRunMacroText('/use Healthstone')
	end

 local specId = select(1, GetSpecializationInfo(GetSpecialization()))


if playerHealth > 60 and not HaveBuff('player',48266) and specId  == 251  then
	AceCastSpellByName(GetSpellInfo(48266))
end
if playerHealth > 60 and not HaveBuff('player',48265) and specId  == 252  then
	AceCastSpellByName(GetSpellInfo(48265))
end

if playerHealth < 60 and not HaveBuff('player',48263) then
	AceCastSpellByName(GetSpellInfo(48263))
end

if playerHealth > 75 and  HaveBuff('player',119975) then
	CancelUnitBuff("player", GetSpellInfo(119975))
end


if playerHealth < 60 and not HaveBuff('player',119975) and runicPower > 15 then
	AceCastSpellByName(GetSpellInfo(119975))
end

if playerHealth < 30 and isSpellAvailable(49998) and UnitIsEnemy('player','target') and UnitAffectingCombat('player') then
	AceCastSpellByName(GetSpellInfo(49998),'target')
end


end
function checkRunes()
local BloodRunes, UnholyRunes, FrostRunes, DeathRunes = RuneCheck()
local total = 0;
if BloodRunes == 0 then
	total = total +1;
end
if FrostRunes == 0 then
	total = total +1;
end
if DeathRunes == 0 then
	total = total +1;
end
if UnholyRunes == 0 then
	total = total +1;
end
return total
end
function DeathKnight.damageRotation()

local runicPower  = UnitPower("player");
local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')
if runicPower == nil then runicPower = 0 end

local BloodRunes, UnholyRunes, FrostRunes, DeathRunes = RuneCheck()

 local specId = select(1, GetSpecializationInfo(GetSpecialization()))

 if specId  == 251 then

--Cast Soul Reaper Icon Soul Reaper
--only when the target is below 35% health.
if targetHealth < 35 and isSpellAvailable(130735) then
	AceCastSpellByName('Soul Reaper','target')
end


if isSpellAvailable(123693) and checkRunes() > 2 and UnitAffectingCombat('player') then
	AceCastSpellByName(GetSpellInfo(123693))
end
--Cast Defile Icon Defile.
if isSpellAvailable(152280) and UnitIsEnemy('player','target') and UnitAffectingCombat('player') then
	AceCastSpellByName(GetSpellInfo(152280))
	AceRunMacroText("/script ClickArea('target')")
end
if isSpellAvailable(115989) and EnemysAroundUnit('player') > 0  then
	AceCastSpellByName(GetSpellInfo(115989))
end

if not HaveDebuff('target',45524) and isSpellAvailable(45524) then
	AceCastSpellByName(GetSpellInfo(45524))
end

--Cast Howling Blast Icon Howling Blast when you have a Rime Icon Rime proc.
if HaveBuff('player',59057) or HaveBuff('player',59052)  and isSpellAvailable(49184) then
	AceCastSpellByName(GetSpellInfo(49184))
end

--Keep your diseases (Frost Fever Icon Frost Fever and Blood Plague Icon Blood Plague) up at all times.
--Apply Frost Fever Icon Frost Fever with Howling Blast Icon Howling Blast, and apply Blood Plague Icon Blood Plague with Plague Strike Icon Plague Strike. In the case of Plague Strike, make sure that you use an Unholy rune for it, and not a Death rune (if need be, wait slightly for an Unholy rune to become available).
if not HaveDebuff('target',55095,3,'player')  or not HaveDebuff('target',55078,3,'player')  and  not HaveDebuff('target',152281,0,'player') and isSpellAvailable(77575) then
	AceCastSpellByName(GetSpellInfo(77575))
end


if not HaveDebuff('target',55095,3,'player') and not HaveDebuff('target',152281,0,'player')  and isSpellAvailable(49184) then
	AceCastSpellByName(GetSpellInfo(49184))
end

if not HaveDebuff('target',55078,3,'player') and  not HaveDebuff('target',152281,0,'player') and isSpellAvailable(45462) then
	AceCastSpellByName(GetSpellInfo(45462))
end

--Cast Frost Strike Icon Frost Strike if:
--you are about to reach maximum runic power or
--you have a Killing Machine Icon Killing Machine proc.
if HaveBuff('player',51128) and isSpellAvailable(49020) then
	AceCastSpellByName(GetSpellInfo(49020))
end

if runicPower > 76 or (checkRunes() > 3 and runicPower > 30 ) then
	AceCastSpellByName(GetSpellInfo(45529))
	AceCastSpellByName(GetSpellInfo(49143))
end

if  isSpellAvailable(49020) or (BloodRunes == 2 and  UnholyRunes == 2 and DeathRunes == 2) then
	AceCastSpellByName(GetSpellInfo(49020))
end



if not HaveBuff('player',57330) then
	AceCastSpellByName(GetSpellInfo(57330))
end

end

if specId == 252 then

if targetHealth < 45 and isSpellAvailable(130735) then
	AceCastSpellByName('Soul Reaper','target')
end


if not HaveDebuff('target',55078,3,'player') and  not HaveDebuff('target',152281,5,'player') and isSpellAvailable(45462) then
	AceCastSpellByName(GetSpellInfo(45462))
end

--Keep your diseases (Frost Fever Icon Frost Fever and Blood Plague Icon Blood Plague) up at all times.
--Apply Frost Fever Icon Frost Fever with Howling Blast Icon Howling Blast, and apply Blood Plague Icon Blood Plague with Plague Strike Icon Plague Strike. In the case of Plague Strike, make sure that you use an Unholy rune for it, and not a Death rune (if need be, wait slightly for an Unholy rune to become available).
if not HaveDebuff('target',55095,3,'player') or not HaveDebuff('target',152281,0,'player')  and isSpellAvailable(85948) then
	AceCastSpellByName(GetSpellInfo(85948))
end

if not HaveDebuff('target',55095,3,'player')  or not HaveDebuff('target',55078,3,'player')  and  not HaveDebuff('target',152281,0,'player') and isSpellAvailable(77575) then
	AceCastSpellByName(GetSpellInfo(77575))
end

if not HaveDebuff('target',45524) and isSpellAvailable(45524) then
	AceCastSpellByName(GetSpellInfo(45524))
end

if isSpellAvailable(63560) then
	AceCastSpellByName(GetSpellInfo(63560))
end

if not HaveBuff('pet',63560) or runicPower > 90 and isSpellAvailable(47541) then
	AceCastSpellByName(GetSpellInfo(47541))
end



if BloodRunes > 0 or FrostRunes > 0 then
	AceCastSpellByName(GetSpellInfo(85948))
end

if DeathRunes > 0 or UnholyRunes > 0 then
	AceCastSpellByName(GetSpellInfo(55090))
end
if HaveBuff('player',49530) then
	AceCastSpellByName(GetSpellInfo(47541))
end

if isSpellAvailable(47541) and  GetSpellCooldown(63560) > 2 then
	AceCastSpellByName(GetSpellInfo(47541))
end

end


--Use your Unholy Runes on Obliterate Icon Obliterate.

--Use your Death and Frost runes on Howling Blast Icon Howling Blast, leaving at least one spare rune for each Unholy rune you currently have so that you can cast Obliterate Icon Obliterate.


end

function RotationTickDeathKnight()
local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")

local _,UnitClass=UnitClass("player")
if UnitClass~="DEATHKNIGHT" then
	return
end

if IsLeftShiftKeyDown() then
local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v) end
end


	if GetKeyState('3') and hasTarget and InLineOfSight('target') then
		DeathKnight.dispellRotation('target',false)
		DeathKnight.avoidSpellInturupt()
		DeathKnight.survivalRotation()
		DeathKnight.damageRotation()
	end
end



