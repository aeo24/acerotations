addonName, Rotations = ...
Rotations.rogue = {}
Rotations.rogue.ROTATION = function()
RotationTickRogue()
	return false

end
Rogue = {}
Rogue.openerType = 'None'
Rogue.isOpening = false
Rogue.isBursting = false
Rogue.evis = false
Rogue.rupture = false
Rogue.dfa = false
function chooseInturrupt(etarget)
	local start,dur,_ = GetSpellCooldown(1766)
	local kickCD = start + dur - GetTime()
	local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')

	if isSpellAvailable(1766) then
		AceCastSpellByName(GetSpellInfo(1766),etarget)
	elseif kickCD < 13 and kickCD > 0 and  isSpellAvailable(1776) and checkDR(etarget,1776) < 2 then
		AceCastSpellByName(GetSpellInfo(1776),etarget)
	elseif kickCD < 13 and kickCD > 0 and  isSpellAvailable(408) and GetComboPoints("player", 'target') > 2 and targetHealth < 30 and checkDR(etarget,408) < 2 then
		AceCastSpellByName(GetSpellInfo(408),etarget)
	end
end


function Rogue.dispellRotation(lowestTarget,secondLowestPlayer)

	--[[local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		--print(UnitName('raid'..i))
		for _,dispell in pairs(dispelMagicCC) do
			if HaveDebuff(groupType..i, dispell) and isSpellAvailable(77130) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40  and (not HaveDebuff(groupType..i,30108) or IsLeftShiftKeyDown() or HaveBuff('player',642)) then
				 --print("Attempting to dispell "..GetSpellInfo(dispell))
				 --AceCastSpellByName(tostring(GetSpellInfo(77130)),groupType..i)
			end
		end
		for _,dispell in pairs(allRoots) do
			if HaveDebuff(groupType..i, dispell) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40 then
				 if isSpellAvailable(77130) then
					--print("Attempting to dispell "..GetSpellInfo(dispell))
					--AceCastSpellByName(tostring(GetSpellInfo(77130)),groupType..i)
				 end
			end
		end
    end]]--
	for _,etarget in pairs(enemyTargets) do

		local distance = GetUnitDistance(etarget)
		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
		for _, v in ipairs(polys) do
			if GetSpellInfo(v) == spellName and InLineOfSight(etarget) ==1 then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				AceStopSpellCasting()
				if distance < 6  and currentPercent > 30 then
					chooseInturrupt(etarget)
				end
			end
		end


		for _, v in ipairs(healingInterrupt) do
			if GetSpellInfo(v) == spellName and canInterrupt == false then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				if InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 6 and currentPercent > 25  then
					if distance < 8  and currentPercent > 30 then
						chooseInturrupt(etarget)
						print("Inturrupted: "..GetSpellInfo(v) )
					end
				end
			end
		end
		local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
			for _, v in ipairs(channelInturrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and currentPercent > 15  and  distance < 6   then
							chooseInturrupt(etarget)
					end
				end
			end
	end
end


function Rogue.avoidSpellInturupt(lowestPlayer,secondLowestPlayer)

	local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	for _,etarget in pairs(enemyTargets) do
		if InLineOfSight(etarget) == 1 and isSpellAvailable(1856) and HaveDebuff(etarget,2094) and not UnitAffectingCombat(etarget) and GetUnitDistance(etarget) < 6 then
			print("Attempting blind sap..")
			AceRunMacroText("/stopattack")
			if isSpellAvailable(6770) and HaveBuff('player',1784) or HaveBuff('player',1856) or HaveBuff('player',51713)  then
				AceCastSpellByName(GetSpellInfo(6770),target)
			end
			if isSpellAvailable(1856) and not HaveBuff('player',1784) then
				AceCastSpellByName(GetSpellInfo(1856),target)
			end
		end
	end


	if not dfooframe then
		dfooframe = CreateFrame("Frame")
	end
	
	dfooframe:RegisterEvent("UNIT_DIED");
	dfooframe:RegisterEvent("PLAYER_TARGET_CHANGED");
	local SIN_PlayerGUID = UnitGUID("player")
    dfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	dfooframe:SetScript("OnEvent", function(self, event, args, type, _, sourceGUID, sourceName,sourceFlags, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
    if event == "UNIT_DIED"  and destGUID == UnitGUID('target')  or  event == "PLAYER_TARGET_CHANGED"  then
		print("Target died: Resetting opener values.")
		Rogue.openerType = 'None'
		Rogue.isOpening = false
		Rogue.isBursting = false
		Rogue.rupture = false
	end
		if type == "SPELL_CAST_SUCCESS" and sourceGUID == SIN_PlayerGUID  then
				
				if spellID == 2098 and (Rogue.isBursting ==true or 	Rogue.isOpening == true) then
					Rogue.evis = true
				elseif  spellID == 137619 then
					Rogue.evis = false
				elseif  spellID == 1943 and Rogue.isOpening == true then	
					Rogue.rupture = true
				elseif spellID == 152150 and Rogue.isOpening == true then
					Rogue.dfa = true
					if specid ==261 then
						Rogue.openerType = 'None'
						Rogue.isOpening = false
						Rogue.rupture = false
					end
				elseif spellID == 32645 and Rogue.isOpening == true then
					if specid ==259 then
						Rogue.openerType = 'None'
						Rogue.isOpening = false
						Rogue.rupture = false
					end
				end		
						
		end
				
	
	
	end);
end
function shouldGarrotte(target)
	local _,class,_ = UnitClass(target)
	local id = GetInspectSpecialization(target)
	return class == 'MAGE'
	 or (class == "DEATHKNIGHT")
	 or (class == "PALADIN" and (id == 70))
	 or (class == "SHAMAN" and (id == 263 ))
end

function softOpenerSub(target,targetHealth)
	Rogue.openerType = 'Soft'
	Rogue.isOpening = true
	--premedSD(target,targetHealth)
	--sliceAndDice(target,targetHealth,false)

	--if(isStealth()) then
	if shouldGarrotte(target) then
		Garotte(target,targetHealth)
	else
		
		cheapShot(target,targetHealth)
	end
		
	--end
	if HaveDebuff(target,1943,3,'player') and not HaveDebuff(target,1833,1) then
		kidneyShot(target,targetHealth,true,3)
	end
	if HaveDebuff(target,408) or GetCooldownTime(408) > 15 then 
		if isSpellAvailable(137619) then
			AceCastSpellByName(GetSpellInfo(137619),target)
		end
	end
	if HaveDebuff(target,137619) and GetComboPoints("player", target) > 4 then
		deathFromAbove(target,targetHealth)
	end 
	cheapShot(target,targetHealth)
	if Rogue.rupture == false and (HaveDebuff(target,1833) or HaveDebuff(target,703) or GetComboPoints("player", target) > 4)  or com then
		rupture(target,targetHealth,false,1,1)
	end
	cheapShot(target,targetHealth)
	if not isStealth() then
		backStab(target,targetHealth)
		hemorage(target,targetHealth)
	end



end
function hardOpenerSub(target,targetHealth)	
	Rogue.openerType = 'Hard'
	Rogue.isOpening = true
	--premedSD(target,targetHealth)
	--sliceAndDice(target,targetHealth,false)

	if not HaveBuff('player',51713) then
		if shouldGarrotte(target) then
				Garotte(target,targetHealth)
		else	
				cheapShot(target,targetHealth)
		end
		cheapShot(target,targetHealth)
		
		if Rogue.rupture == false and (HaveDebuff(target,1833) or HaveDebuff(target,703) or GetComboPoints("player", target) > 4) then
			rupture(target,targetHealth,false,1,1)
		end
		--backStab(target,targetHealth)
		--hemorage(target,targetHealth)
	end
	cheapShot(target,targetHealth)
	ambush(target,targetHealth)

		--kidneyShot(target,targetHealth,true)
		--if not HaveDebuff(target,137619) and UnitPower('player') > 60 then 
		--	eviscerateMFD(target,targetHealth)
		--end
		if HaveDebuff(target,408) or GetCooldownTime(408) > 15 then 
			if isSpellAvailable(137619) then
				AceCastSpellByName(GetSpellInfo(137619),target)
			end
		end
		if HaveDebuff(target,137619) and GetComboPoints("player", target) > 4 then
			deathFromAbove(target,targetHealth)
		end 
		if HaveDebuff(target,1943,3,'player') then
			shadowDance(target,targetHealth,false)
		end
		if HaveBuff('player',51713) then
			kidneyShot(target,targetHealth,true)
		end
		--[[]if Ace.lastCast == 152150 then
					Rogue.openerType = 'None'
					Rogue.isOpening = false
					Rogue.rupture = false
		end]]--
end
function hardOpenerAss(target,targetHealth)
	Rogue.openerType = 'Hard'
	Rogue.isOpening = true
	if(isStealth() or HaveBuff('player',108208,2) ) then
		if shouldGarrotte(target) then
			Garotte(target,targetHealth)
		else
			
			cheapShot(target,targetHealth)
		end
		cheapShot(target,targetHealth)
		
		if Rogue.rupture == false and (HaveDebuff(target,1833) or HaveDebuff(target,703) or GetComboPoints("player", target) > 4) then
			rupture(target,targetHealth,false,1,1)
		end
	end
	cheapShot(target,targetHealth)
	dispatch(target,targetHealth)
	mutilate(target,targetHealth)
	if HaveDebuff(target,408) or GetCooldownTime(408) > 15 then 
			if isSpellAvailable(79140) then
				AceCastSpellByName(GetSpellInfo(79140),target)
			end
			if isSpellAvailable(137619) then
				AceCastSpellByName(GetSpellInfo(137619),target)
			end
		--end
		--DFA
		if isSpellAvailable(152150) and HaveDebuff(target,137619) and UnitPower('player') > 49 then
			AceCastSpellByName(GetSpellInfo(152150),target)
		end

		--Vanish
		if isSpellAvailable(1856) and Rogue.dfa == true and HaveDebuff(target,137619) then
			AceCastSpellByName(GetSpellInfo(1856),target)
		else
			if GetCooldownTime(1856) > 10 then
				Rogue.isBursting = false
			end
		end
		
		if isSpellAvailable(32645) and HaveBuff('player',170882) and UnitPower('player') > 34  then
			--finisher
			AceCastSpellByName(GetSpellInfo(32645),target)
			Rogue.isBursting = false
			Rogue.dfa = false
		end
	end	
		if HaveDebuff(target,1943,3,'player') then
			kidneyShot(target,targetHealth,true)
		end
end

function softOpenerAss(target,targetHealth)
	Rogue.openerType = 'Soft'
	Rogue.isOpening = true

	if(isStealth()) then
		if shouldGarrotte(target) then
			Garotte(target,targetHealth)
		else
			
			cheapShot(target,targetHealth)
		end
	end
	rupture(target,targetHealth,false,2)
	dispatch(target,targetHealth)
	mutilate(target,targetHealth)
	if HaveDebuff(target,1943,3,'player') then
		kidneyShot(target,targetHealth,true)
	end
		if HaveDebuff(target,408) then 
			Rogue.openerType = 'None'
			Rogue.isOpening = false
		end
			sliceAndDice(target,targetHealth)
end
function Rogue.survivalRotation()

	local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

	if playerHealth < 70 and UnitAffectingCombat('player') and  GetItemCooldown(5512) == 0 then
				   AceRunMacroText('/use Healthstone')
	end

	--feint
	if not HaveBuff('player',1966) and isSpellAvailable(1966) and playerHealth < 75 then
			AceCastSpellByName(GetSpellInfo(1966),nil)
	end
	--recup
	if not HaveBuff('player',73651) and isSpellAvailable(73651) and playerHealth < 75 and GetComboPoints("player", target) > 4 and (HaveBuff('player',5171) or specid == 259) and not IsLeftAltKeyDown() then
			AceCastSpellByName(GetSpellInfo(73651),nil)
	end
	--evasion
	if not HaveBuff('player',5277) and isSpellAvailable(5277) and playerHealth < 60 then
			AceCastSpellByName(GetSpellInfo(5277),nil)
	end
	--cloak
	if not HaveBuff('player',31224) and isSpellAvailable(31224) and playerHealth < 25 then
			AceCastSpellByName(GetSpellInfo(31224),nil)
	end
	--COmbat Readiness
	if not HaveBuff('player',74001) and isSpellAvailable(74001) and not isSpellAvailable(5277) and playerHealth < 70 and MeleeAroundPlayer() then
			AceCastSpellByName(GetSpellInfo(74001),nil)
	end

end
function isStealth()
	return HaveBuff('player',1784) or HaveBuff('player',108208) or HaveBuff('player',115191) or HaveBuff('player',1856)
end
function cheapShot(target,targetHealth)
--CS
	if(GetComboPoints("player", target) < 4) then
		if  isSpellAvailable(1833) and (HaveBuff('player',1784) or HaveBuff('player',108208) or HaveBuff('player',51713))  and not HaveDebuff(target,1833) and not HaveDebuff(target,408)  and checkDR(target,1833) < 1 then
				AceCastSpellByName(GetSpellInfo(1833),target)
		end
	end
end
function Garotte(target,targetHealth)
--Garotte
	if isSpellAvailable(703) and HaveBuff('player',1784) or HaveBuff('player',108208) and not HaveDebuff(target,703) or HaveBuff('player',51713) and not IsMeleeDps(target) and GetComboPoints("player", target) < 4 then
			AceCastSpellByName(GetSpellInfo(703),target)
	end
end
function ambush(target,targetHealth)
	--ambush
	if isSpellAvailable(8676) and  HaveBuff('player',108208) or HaveBuff('player',51713) and GetComboPoints("player", target) < 5 then
			AceCastSpellByName(GetSpellInfo(8676),target)
	end
end
function dispatch(target,targetHealth)
--Dispatch
	if targetHealth < 35 or HaveBuff('player',121152) and isSpellAvailable(111240) and GetComboPoints("player", target) < 5 then
		AceCastSpellByName(GetSpellInfo(111240),target)
	end
end
function sliceAndDice(target,targetHealth,force)
--S&D
	if isSpellAvailable(5171) and not HaveBuff('player',5171,2) and GetComboPoints("player", target) > 1 and not force and  targetHealth > 20 then
			AceCastSpellByName(GetSpellInfo(5171),nil)
	end
end
function kidneyShot(target,targetHealth,force,comboPoints)
--ALT KS
if comboPoints == nil then comboPoints = 4 end
	if isSpellAvailable(408) and GetComboPoints("player", target) > comboPoints and force == true  and checkDR(target,408) < 2 then
			AceCastSpellByName(GetSpellInfo(408),target)
	end
end
function rupture(target,targetHealth,force,comboPoints,finish)

	if comboPoints == nil then comboPoints = 4 end
	if finish == nil then finish = 8 end
	local finishRT = 0;
	local _,_,_,_,_,_,RT,_,_=UnitDebuff("target","Rupture","","player");
	if RT ~= nil then
		finishRT = (RT -GetTime())
	end
	if isSpellAvailable(1943) and finishRT < finish and GetComboPoints("player", target) > comboPoints  and targetHealth > 20 and not force and Ace.lastCast ~= 137619 then
			AceCastSpellByName(GetSpellInfo(1943),target)
	end
end
function envenom(target,targetHealth, force)
--envenom
	local energy = UnitPower('player')
	if isSpellAvailable(152150)and GetCooldownTime(79140) > 20 and GetComboPoints("player", target) > 4 and  not force and energy > 70 then
			AceCastSpellByName(GetSpellInfo(152150),target)
	elseif isSpellAvailable(32645) and GetComboPoints("player", target) > 4 and  not force and energy > 80 then
			AceCastSpellByName(GetSpellInfo(32645),target)
	end
end
function mutilate(target,targetHealth)
--Multilate
	if isSpellAvailable(1329) and targetHealth > 35  and GetComboPoints("player", target) < 5 then
			AceCastSpellByName(GetSpellInfo(1329),target)
	end
end
function poisions()
--Poisions
	if not HaveBuff('player',2823) and isSpellAvailable(2823) and not UnitAffectingCombat('player') then
		AceCastSpellByName(GetSpellInfo(2823),nil)
	end

	if not HaveBuff('player',108211) and isSpellAvailable(108211) and not UnitAffectingCombat('player') then
		AceCastSpellByName(GetSpellInfo(108211),nil)
	end
end
function revealingStrike(target,targetHealth)
	if not HaveDebuff(target,84617,6,'player') and isSpellAvailable(84617) and GetComboPoints("player", target) < 5 then
		AceCastSpellByName(GetSpellInfo(84617),target)
	end
end
function sinisterStrike(target,targetHealth)
	if isSpellAvailable(1752) and not HaveBuff('player',1784) and GetComboPoints("player", target) < 5 then
		AceCastSpellByName(GetSpellInfo(1752),target)
	end
end
function eviscerate(target,targetHealth, force)
		if isSpellAvailable(152150)and GetCooldownTime(51713) > 15 and GetComboPoints("player", target) > 4 and  not force  then
			AceCastSpellByName(GetSpellInfo(152150),target)
	    elseif isSpellAvailable(2098) and GetComboPoints("player", target) > 4 and  not force  then
				AceCastSpellByName(GetSpellInfo(2098),target)
		end
end
function deathFromAbove(target,targetHealth, force)
		if isSpellAvailable(152150) and GetComboPoints("player", target) > 4  then
			AceCastSpellByName(GetSpellInfo(152150),target)
		end
end
function eviscerateMFD(target,targetHealth)
		if isSpellAvailable(2098) and GetComboPoints("player", target) > 4 and UnitPower('player') > 35 then
				AceCastSpellByName(GetSpellInfo(2098),target)
		end
end
function shadowDance(target,targetHealth,setFlag)
	if isSpellAvailable(51713) then
			AceCastSpellByName(GetSpellInfo(51713),target)
			Rogue.isBursting = setFlag
	end
end

function danceBurst(target,targetHealth)
	
	if HaveBuff('player',51713) then
		if HaveBuff('player',51713,7) then
			if shouldGarrotte(target) then
				Garotte(target,targetHealth)
			else	
				cheapShot(target,targetHealth)
			end
		end
	--	if HaveDebuff(target,91023) then
		ambush(target,targetHealth)
	--	end
		--kidneyShot(target,targetHealth,true)
		if not HaveDebuff(target,137619) and UnitPower('player') > 59 then 
			eviscerateMFD(target,targetHealth)
		end
		if Rogue.evis == true then 
			if isSpellAvailable(137619) then
					AceCastSpellByName(GetSpellInfo(137619),target)
			end
		end
		if HaveDebuff(target,137619) and GetComboPoints("player", target) > 4 then
			deathFromAbove(target,targetHealth)
		end 
		if GetSpellCooldown(137619) > 55 and GetSpellCooldown(152150) > 15 then 
			eviscerate(target,targetHealth)
		end
	else	
		Rogue.isBursting = false
	end
end

function openTarget(target,targetHealth)
	if shouldGarrotte(target) then
		Garotte(target,targetHealth)
	else
		cheapShot(target,targetHealth)
	end
	cheapShot(target,targetHealth)
end
--/script print( UnitInfront('player','target'))
--//script print( GetUnitPosition('target'))
function hemorage(target,targetHealth)
	if isSpellAvailable(16511) and UnitInfront(target, 'player') and GetComboPoints("player", target) < 5 then
			AceCastSpellByName(GetSpellInfo(16511),target)
	end
end
function backStab(target,targetHealth)
	if isSpellAvailable(53) and not UnitInfront(target, 'player') and GetComboPoints("player", target) < 5 then
			AceCastSpellByName(GetSpellInfo(53),target)
	end
end
function cheapshotFocus()
	if UnitExists('focus') and UnitIsEnemy('focus','player') and GetUnitDistance('focus') < 6 then
		cheapShot('focus',100)
	end
end
function premedSD(target,targetHealth)
	if isSpellAvailable(14183) and HaveBuff('player',1784) and GetUnitDistance(target) < 20 and UnitReaction("player", target) <= 4 then
			AceCastSpellByName(GetSpellInfo(14183))
	end
end
function shouldPoolEnergy(pool)
	if UnitPower('player') < pool then
		return true
	else
		return false
	end
end
function burstDamage(target,targetHealth)

		--KS/CS
		
		Rogue.isBursting = true
	
		local specid = select(1, GetSpecializationInfo(GetSpecialization()))
		local finisher = 2098
		if specid ==  259 then
			finisher = 32645
		end
		local stealth = HaveBuff('player',1784)
		local stuns = {408,1833}
		local stunSpell = stealth and 1833 or 408
		local start,dur,_ = GetSpellCooldown(152150)
		local DFACD = start + dur - GetTime()
		if shouldPoolEnergy(50) then
			return
		end

		--if not HaveDebuff(target,stuns) and isSpellAvailable(stunSpell) and checkDR(target,stunSpell) < 2  then
		--	AceCastSpellByName(GetSpellInfo(stunSpell),target)
		--end
		--if  HaveDebuff(target,stuns) then
		--VEndetta/Mark for death
			if isSpellAvailable(79140) then
				AceCastSpellByName(GetSpellInfo(79140),target)
			end
			if isSpellAvailable(137619) then
				AceCastSpellByName(GetSpellInfo(137619),target)
			end
		--end
		--DFA
		if isSpellAvailable(152150) and HaveDebuff(target,137619) and UnitPower('player') > 49 then
			AceCastSpellByName(GetSpellInfo(152150),target)
		end

		--Vanish
		if isSpellAvailable(1856) and DFACD > 1 and HaveDebuff(target,137619) then
			AceCastSpellByName(GetSpellInfo(1856),target)
		else
			if GetCooldownTime(1856) > 10 then
				Rogue.isBursting = false
			end
		end
		
		if isSpellAvailable(finisher) and HaveBuff('player',170882) and UnitPower('player') > 34  then
			--finisher
			AceCastSpellByName(GetSpellInfo(finisher),target)
			Rogue.isBursting = false
		end
end
function Rogue.openRotation(target)

	local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
	Rogue.dispellRotation(target,nil)
	if specid == 260 then --combat

	elseif specid == 259 then --Assassination333333333

	if HaveBuff('player',1784)  and Rogue.openerType == 'None'  then
		print(Rogue.openerType)	
		if IsLeftShiftKeyDown() then
			hardOpenerAss(target,targetHealth)
		else
			softOpenerAss(target,targetHealth)
		end
	end	
		if Rogue.openerType == 'Hard' then
			hardOpenerAss(target,targetHealth)
		elseif Rogue.openerType == 'Soft' then
			softOpenerAss(target,targetHealth)
		end
	elseif specid == 261 then --Sub
	print(Rogue.openerType)
		if HaveBuff('player',1784)  and Rogue.openerType == 'None'  then
		if not HaveBuff('player',5171) then return end
			if IsLeftShiftKeyDown() then
				hardOpenerSub(target,targetHealth)
			else
				softOpenerSub(target,targetHealth)
			end
		end	
		if Rogue.openerType == 'Hard' then
			hardOpenerSub(target,targetHealth)
		elseif Rogue.openerType == 'Soft' then
			softOpenerSub(target,targetHealth)
		end
	end
end
function Rogue.damageRotation(target)

	if HaveBuff('player',1784) or HaveBuff('player',1856) then
		Rogue.openerType = 'None'
		Rogue.isOpening = false
		Rogue.isBursting = false
		Rogue.rupture = false
	end	
	local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
	Rogue.dispellRotation(target,nil)
	
	if specid == 260 then --combat
		if IsLeftShiftKeyDown()  then
			--Burst
			burstDamage(target,targetHealth)
		else
			openTarget(target,targetHealth)
			cheapshotFocus()
			Rogue.survivalRotation()
			if not isStealth()  then
				revealingStrike(target,targetHealth)
				sinisterStrike(target,targetHealth)
				sliceAndDice(target,targetHealth)
				kidneyShot(target,targetHealth,IsLeftAltKeyDown())
				eviscerate(target,targetHealth,IsLeftAltKeyDown())
			end
		end
	elseif specid == 259 then --Assassination333333333
		if IsLeftShiftKeyDown() then
			--Burst
			burstDamage(target,targetHealth)
		else
			if Rogue.isBursting == false then	
				kidneyShot(target,targetHealth,IsLeftAltKeyDown())
				Rogue.survivalRotation()
				ambush(target,targetHealth)
				rupture(target,targetHealth,IsLeftAltKeyDown())
				envenom(target,targetHealth,IsLeftAltKeyDown())
				dispatch(target,targetHealth)
				mutilate(target,targetHealth)
			else 
				burstDamage(target,targetHealth)
			end
			--poisions()
		end
	elseif specid == 261 then --Sub
		Rogue.openerType = 'None'
		Rogue.isOpening = false
		Rogue.rupture = false
		
		if IsLeftShiftKeyDown()  and not HaveBuff('player',51713) and isSpellAvailable(51713) and isSpellAvailable(137619)  then
			if shouldPoolEnergy(80) then
				return
			end
			shadowDance(target,targetHealth,true)
		else
		--print (Rogue.isBursting )
			if Rogue.isBursting  == false then
			kidneyShot(target,targetHealth,IsLeftAltKeyDown() or false)
			rupture(target,targetHealth,IsLeftAltKeyDown(),4,4)
			sliceAndDice(target,targetHealth,IsLeftAltKeyDown())
			eviscerate(target,targetHealth,IsLeftAltKeyDown())
			if HaveBuff('player',51713)  then
				cheapShot(target,targetHealth)
				ambush(target,targetHealth)
			end
			backStab(target,targetHealth)
			hemorage(target,targetHealth)
			else
				danceBurst(target,targetHealth)
			end
		end
		poisions()
	end
end

function Rogue.healingRotation(target,target2)

local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))
local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

local dist = GetUnitDistance(target)
if dist == nil then dist = 50 end

if dist > 40 or UnitIsEnemy('player',target) or UnitIsDeadOrGhost(target) or UnitCastingInfo("player") or UnitChannelInfo("player")  or not InLineOfSight(target) == 1 then
	return
end



end

function RotationTickRogue()


local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local _,UnitClass=UnitClass("player")
local targetHealth = 100 * UnitHealth("Target") / UnitHealthMax("Target")
		if UnitClass~="ROGUE" then
			return
		end


		local flag = { "Alliance Flag", "Horde Flag"  }
		for _,v in ipairs(flag) do
		AceInteractUnit(v)
		end

		--getHealingTarget()
		--[[local lowestPlayer,lowestTank = GroupInfo()
		local secondLowestPlayer,secondLowestTank
		if lowestPlayer[2] then secondLowestPlayer = lowestPlayer[2].Unit end
		if lowestTank[2] then secondLowestTank = lowestTank[2].Unit end]]--
			--print(UnitName(lowestPlayer))
		stealthFinder(6770)
		
		if not  HaveBuff('player',1784) and not UnitAffectingCombat('player') and   not IsMounted() and  InActiveBattlefield() then
			AceCastSpellByName(GetSpellInfo(1784),target)
		end
		
		
		
		if GetKeyState(HEALING_KEY) then

			Rogue.avoidSpellInturupt(nil,nil)
			Rogue.openRotation('target')
			--Rogue.healingRotation(lowestPlayer[1].Unit,secondLowestPlayer)
		end
		Rogue.survivalRotation()
		if  hasTarget and InLineOfSight('target') then
		premedSD("Target",targetHealth)
		sliceAndDice("Target",targetHealth,IsLeftAltKeyDown())
		end	
		
		if GetKeyState(DAMAGE_KEY) then
			Rogue.avoidSpellInturupt(nil,nil)
			Rogue.damageRotation('target')
		end
end



