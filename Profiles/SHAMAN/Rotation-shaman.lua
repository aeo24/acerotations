addonName, Rotations = ...
Rotations.shaman = {}
Rotations.shaman.ROTATION = function()
	RotationTickShaman()
	return false

end
Shaman = {}


function Shaman.dispellRotation(lowestTarget,secondLowestPlayer)

	local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		--print(UnitName('raid'..i))
		for _,dispell in pairs(dispelMagicCC) do
			if HaveDebuff(groupType..i, dispell) and isSpellAvailable(77130) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40  and (not HaveDebuff(groupType..i,30108) or IsLeftShiftKeyDown() or HaveBuff('player',642)) then
				 print("Attempting to dispell "..GetSpellInfo(dispell))
				 AceCastSpellByName(tostring(GetSpellInfo(77130)),groupType..i)
			end
		end
		for _,dispell in pairs(allRoots) do
			if HaveDebuff(groupType..i, dispell) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40 then
				 if isSpellAvailable(77130) then
					print("Attempting to dispell "..GetSpellInfo(dispell))
					AceCastSpellByName(tostring(GetSpellInfo(77130)),groupType..i)
				 end
			end
		end
    end
	for _,etarget in pairs(enemyTargets) do

		local distance = GetUnitDistance(etarget)
		local spellName, _, _, _, _, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
		for _, v in ipairs(polys) do
			if GetSpellInfo(v) == spellName and InLineOfSight(etarget) ==1 then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				AceStopSpellCasting()
				if isSpellAvailable(57994) and  distance < 25  and currentPercent > 30 then
					AceCastSpellByName(GetSpellInfo(57994),etarget)
				end
			end
		end



		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
			for _, v in ipairs(healingInterrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 8 and currentPercent > 25  then
						print("Inturrupted: "..GetSpellInfo(v) )
						if isSpellAvailable(57994) and  distance < 25  and currentPercent > 30 then
							AceCastSpellByName(GetSpellInfo(57994),etarget)
						end
					end
				end
			end
		local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
			for _, v in ipairs(channelInturrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and currentPercent > 15  and  distance < 7   then
						if isSpellAvailable(57994) and  distance < 25 then
							AceCastSpellByName(GetSpellInfo(57994),etarget)
						end
					end
				end
			end
	end
end


function Shaman.avoidSpellInturupt(lowestPlayer,secondLowestPlayer)

if not dfooframe then
	dfooframe = CreateFrame("Frame")
end

	local SIN_PlayerGUID = UnitGUID("player")
    dfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	dfooframe:SetScript("OnEvent", function(self, event, args, type, _, sourceGUID, sourceName,sourceFlags, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
            if type == "SPELL_CAST_SUCCESS" and destGUID == SIN_PlayerGUID and interruptID[spellID] then
                local isProtected =  select(9, UnitCastingInfo('player')) or select(8, UnitChannelInfo('player'))
					if not isProtected and not HaveBuff('player',31821) and not HaveBuff('player', 96267)  and not HaveBuff('player', 31821) then --96267 = priest's cast immunity
						AceStopSpellCasting()
						AceStopSpellCasting()
                        AceStopSpellCasting()
						AceStopSpellCasting()
						print("Attempting to Juke "..GetSpellInfo(spellID))
						--[[local locType, _, _, _, _, _, _, _, _, _ = C_LossOfControl.GetEventInfo(1) or '';
						print('Locked '..locType)
						if locType == "SCHOOL_INTERRUPT" then
							print("Failed to Juke "..GetSpellInfo(spellID))
						end]]--
						--Paladin.healingRotation(lowestPlayer,secondLowestPlayer)
                    end
            end
			--[[if type == "SPELL_CAST_FAILED" and sourceGUID == SIN_PlayerGUID then
				if  extraSpellID ~= "Can't do that while moving" and  extraSpellID ~= "You are dead" and  extraSpellID ~= "Ability is not ready yet" and  extraSpellID ~= "Not yet recovered" then
					print("Failed to cast: "..GetSpellInfo(spellID).." "..extraSpellID)
				end
			end]]--

			if type == "SPELL_CAST_SUCCESS" and  destGUID == UnitGUID('player') then
				for _,dispell in pairs(getPolys()) do
					if dispell == spellID then
						AceStopSpellCasting()
						AceStopSpellCasting()
						AceStopSpellCasting()
						AceCastSpellByName(GetSpellInfo(6940),(UnitIsUnit('player',lowestPlayer) and secondLowestPlayer or lowestPlayer))
					end
				end
			end
			--[[if ( event == "LOSS_OF_CONTROL_ADDED" ) then
			local eventIndex = args;
			local locType, spellID, text, iconTexture, startTime, timeRemaining, duration, lockoutSchool, priority, displayType = C_LossOfControl.GetEventInfo(eventIndex);

			end]]--
    end);
end





function Shaman.survivalRotation()

local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
if playerHealth < 35 and isSpellAvailable(108271) then
	AceCastSpellByName(108271)
end

if playerHealth < 35 and isSpellAvailable(108281) then
	AceCastSpellByName(108281)
end

if playerHealth < 20 and isSpellAvailable(30823) and not isSpellAvailable(108271) then
	AceCastSpellByName(30823)
end

end

function Shaman.damageRotation(target)

local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)

local haveTotem, totemName, startTime, duration = GetTotemInfo(1)

local name, _, icon, count = UnitBuff("player", GetSpellInfo(51530))
if count == nil then count = 0 end

if isSpellAvailable(3599) and not haveTotem then
	AceCastSpellByName(3599)
end

if count > 4 and isSpellAvailable(403) then
	AceCastSpellByName(403,target)
end

if isSpellAvailable(73680) then
	AceCastSpellByName(73680,target)
end


if isSpellAvailable(8050) and not HaveDebuff(target,8050,9,'player') and HaveDebuff(target,73683) then
	AceCastSpellByName(8050,target)
end

if isSpellAvailable(17364) then
	AceCastSpellByName(GetSpellInfo(17364),target)
end

if isSpellAvailable(60103) then
	AceCastSpellByName(60103,target)
end





if isSpellAvailable(8056) then
	AceCastSpellByName(8056,target)
end

--[[if count > 0 and isSpellAvailable(403) then
	AceCastSpellByName(403,target)
end]]--

if isSpellAvailable(3599) and duration < 20 then
	AceCastSpellByName(3599)
end


end




function Shaman.healingRotation(target,target2)
local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))
local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

local dist = GetUnitDistance(target)
if dist == nil then dist = 50 end

if dist > 40 or UnitIsEnemy('player',target) or UnitIsDeadOrGhost(target) or UnitCastingInfo("player") or UnitChannelInfo("player")  or not InLineOfSight(target) == 1 then
	return
end



end

function RotationTickShaman()


local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local _,UnitClass=UnitClass("player")

if UnitClass~="SHAMAN" then
	return
end


local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v)
end


local lowestPlayer,secondLowestPlayer = GroupInfo()
		--print(UnitName(lowestPlayer))
	stealthFinder(20271)
	if GetKeyState(HEALING_KEY) then

		Shaman.avoidSpellInturupt(lowestPlayer,secondLowestPlayer)
		Shaman.survivalRotation()
		Shaman.healingRotation(lowestPlayer,secondLowestPlayer)
	end

	if GetKeyState(DAMAGE_KEY) and hasTarget and InLineOfSight('target') then
		Shaman.survivalRotation()
		Shaman.damageRotation('target')
	end
end



