addonName, LURS = ...
LURS.warlock = {}
LURS.warlock.ROTATION = function()

--- Anti Spam


	
--- Si estas variables las pongo fuera del frame no las toma.

local playerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local soulShards = UnitPower("player", 7)
local ember = UnitPower("player", 14)
local emberMax = UnitPowerMax("player", 14)
local piedras = GetItemCount("Piedra de salud",nil,true)


Time = GetTime()

	-- Buff and HP
	
	
	-- HP BAJO
			
		if playerHP < 35 and ember >=1 then
			
			oexecute("CastSpellByName('Transfusión de ascuas')") -- Transfusión de ascuas
		
		elseif playerHP < 35 and not GetItemCooldown(5512) == 0 then
			
			oexecute("UseItemByName('Piedra de salud', 'player')") -- health stone
		
		elseif playerHP < 45 and GetSpellCooldown("Espiral mortal") == 0 then
		
			oexecute("CastSpellByName('Espiral mortal')") -- Transfusión de ascuas
			
		end
	
		if not UnitBuff("player", "Resguardo crepuscular") and GetSpellCooldown("Resguardo crepuscular") == 0 then
		--oexecute("SpellStopCasting()")
		oexecute("CastSpellByName('Resguardo crepuscular')")
		end
		
		if not UnitBuff("player", "Horror Sangriento") and GetSpellCooldown("Horror Sangriento") == 0 then
		--oexecute("SpellStopCasting()")
		oexecute("CastSpellByName('Horror Sangriento')")
		end
			
		if UnitExists('target') then
        local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
        if name and not notInterruptible and GetSpellCooldown("Explosión óptica") == 0 then
            if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
                oexecute("SpellStopCasting()")
                oexecute("CastSpellByName('Explosión óptica')")
            end
        end
		end
		-- replace 'Kick' to your spell  
	end
	
	if UnitIsDeadOrGhost("player") == nil and not UnitHasVehicleUI("player") then
	
		if not IsMoving() and GetItemCount("Piedra de salud") == 0 and GetSpellCooldown("Crear piedra de salud") == 0 then
		print("Tengo 'piedras' de salud")
		oexecute("CastSpellByName('Crear piedra de salud')")
		end
		
		if not UnitBuff("player", "Propósito oscuro") and GetSpellCooldown("Propósito oscuro") == 0 then
		--oexecute("SpellStopCasting()")
		oexecute("CastSpellByName('Propósito oscuro')")
		end
	
	-- BOTON 1
	
if ActionButton1:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then
	
		if playerHP < 35 and ember > 0 then
			
			oexecute("CastSpellByName('Transfusión de ascuas')") -- Transfusión de ascuas
		
		elseif playerHP < 35 and GetItemCount(5512) > 0 and GetItemCooldown(5512) == 0 then -- Piedra de salud
			
			print("Piedra de salud")
			oexecute("UseItemByName('Piedra de salud', 'player')") -- health stone
		
		elseif playerHP < 45 and GetSpellCooldown("Espiral mortal") == 0 then
		
			oexecute("CastSpellByName('Espiral mortal')") -- Transfusión de ascuas
		
		end
		
		if UnitExists('target') and GetSpellCooldown("Furia de las sombras") == 0 then 
		
		oexecute("CastSpellByName('Furia de las sombras')")
		oclick("target")
		
		end
		
end -- ActionButton1
	
	-- BOTON 2

	if ActionButton2:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then
	
		if LastActionTime == nil or Time - LastActionTime > 0.700 then
		
		LastActionTime = GetTime()
	
	--Check if chaosbolt is being casted to no cancel it
	--HP Normal
		
		if UnitExists('target') 
		and targetHP <= 20 
		and ember >= 1 then
		oexecute("SpellStopCasting()")
		oexecute("CastSpellByName('Quemadura de las sombras')") -- Quemadura de las sombras si esta por debajo de 20% de vida
		print("2. Quemadura de las sombras")
		
		elseif UnitExists('target') 
		and UnitBuff("player", "Contragolpe") then
		oface("target")
		oexecute("CastSpellByName('Incinerar')") -- Incinerar con Contragolpe
		print("2. Incinerar (Contragolpe)")					
			
		elseif not IsMoving() 
		and UnitExists('target') 
		and UnitDebuff("target", "Inmolar")
		and IsCurrentSpell("Descarga de Caos") == nil		
		and targetHP > 20 
		and ember > 2 then
		oface("target")	
		oexecute("CastSpellByName('Descarga de Caos')") -- Descarga de Caos, si tiene mas de 2 embers
		print("3. Descarga de caos")
			if GetSpellCooldown("Alma oscura") == 0 then
			oexecute("CastSpellByName('Alma oscura')")
			end
			
		elseif not IsMoving() 
		and UnitExists('target') 
		and not UnitDebuff("target", "Inmolar")
		and IsCurrentSpell("Inmolar") == nil
		and IsCurrentSpell("Descarga de Caos") == nil
		then -- Inmolar si no tiene debuff, si tiene ignora esto
		--oexecute("SpellStopCasting()")		
		oface("target")
		oexecute("CastSpellByName('Inmolar')")
		--oexecute("RunMacroText('/castsequence reset=2 '..GetSpellInfo(348))")
		print("4. Inmolar")
		
		elseif UnitExists('target') 
		and not UnitBuff("player", "Explosión de humo") 
		and GetSpellCooldown("Conflagrar") == 0 
		then -- Conflagrar si no tenemos explosion de humo y no esta en cooldown
		oexecute("CastSpellByName('Conflagrar')") -- Conflagrar si no tengo el buff Explosión de humo
		print("5. Conflagrar")
			
		elseif not IsMoving() 
		and UnitExists('target')
		and UnitBuff("player", "Explosión de humo")
		and IsCurrentSpell("Incinerar") == nil
		then
		oface("target")
		oexecute("CastSpellByName('Incinerar')") -- Incinerar
		print("6. Incinerar")
		
		elseif UnitExists('target')
		and playerMana > 50
		and UnitDebuff("target", "Inmolar")
		and not UnitDebuff("target","Lluvia de Fuego")
		then
		oinfo("target")
		oexecute("CastSpellByName('Lluvia de Fuego')") -- Llama vil
		oclick("target")
		
		print("7. Llama vil (Lluvia de Fuego)")
			
		elseif UnitExists('target')
		and playerMana > 50
		then
		oface("target")
		oexecute("CastSpellByName('Llama vil')") -- Llama vil
		print("7. Llama vil (mana dump)")
			
		
		elseif UnitExists('target')
		and not UnitDebuff("target", "Maldición de los elementos")
		then
		oface("target")
		oexecute("CastSpellByName('Maldición de los elementos')") -- Maldición de los elementos
		print("8. Maldición de los elementos")
		
		end
		
		end
		
	end	
		
	--- BOTON 3
		
	if ActionButton3:GetButtonState() == 'PUSHED' then
	
	if UnitExists('target') then
		print("Orden demoníaca")
		oexecute("CastSpellByID(1157703)") -- latigazo vil
		oinfo("target")
		print("oinfo")
		oclick("target")
		print("oclick")
		if UnitExists('target') and GetSpellCooldown("Lluvia de Fuego(Destrucción)") == 0 then -- and InCombatLockdown() 
		oexecute("CastSpellByName('Lluvia de Fuego(Destrucción)')")
		oinfo("target")
		oclick("target")		
		end
		end
		end
		
		-- BOTON 6
		
	if ActionButton6:GetButtonState() == 'PUSHED' and not UnitHasVehicleUI("player") then -- Un enemigo
		
		if UnitExists('target') then
		oexecute("CastSpellByName('Aullido de terror')")
		oexecute("CastSpellByName('Espiral mortal')")
		end
		
	end
	
	
-- BUFFs

  

	
	end