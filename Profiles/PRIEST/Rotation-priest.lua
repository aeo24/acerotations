addonName, Rotations = ...
Rotations.priest = {}
Rotations.priest.ROTATION = function()
	RotationTickPriest()
	return false

end
Priest = {}
Priest.lowestTarget= 'target'

Priest.immunDispel = {
    45438,        -- Ice Block
    110700,        -- Divine Shield (Paladin)
    110696,        -- Ice Block (Mage)
    45438,        -- Ice Block
    1022,         --Hand of Protection
    642            -- Divine Shield
}
Priest.dispelMagicCC = {
115001,        -- Remorseless Winter
2637,        -- Hibernate
110698,        -- Hammer of Justice (Paladin)
117526,        -- Binding Shot
3355,        -- Freezing Trap
1513,        -- Scare Beast
118271,        -- Combustion Impact
44572,        -- Deep Freeze
31661,        -- Dragon's Breath
118,        -- Polymorph
61305,        -- Polymorph: Black Cat
28272,        -- Polymorph: Pig
61721,        -- Polymorph: Rabbit
61780,        -- Polymorph: Turkey
28271,        -- Polymorph: Turtle
82691,        -- Ring of Frost
11129,         -- Combustion
123393,        -- Breath of Fire (Glyph of Breath of Fire)
115078,        -- Paralysis
105421,        -- Blinding Light
115752,        -- Blinding Light (Glyph of Blinding Light)
105593,        -- Fist of Justice
853,        -- Hammer of Justice
119072,        -- Holy Wrath
20066,        -- Repentance
10326,        -- Turn Evil
64044,        -- Psychic Horror
8122,        -- Psychic Scream
113792,        -- Psychic Terror (Psyfiend)
9484,        -- Shackle Undead
118905,        -- Static Charge (Capacitor Totem)
5782,        -- Fear
118699,        -- Fear
5484,        -- Howl of Terror
6789,        -- Mortal Coil
30283,        -- Shadowfury
104045,        -- Sleep (Metamorphosis)
115268,        -- Mesmerize (Shivarra)
113092,        -- Frost Bomb
6358,
51514,339,4167,51485       -- Hex
}

local defensiveCds = {118038, 19263,61336,22812,45438,642,47585,5277,108271,104773,1022,116849,53480,102342}
function getDefenceCds()
return defensiveCds
end


function getPlayerId(unit)

local guid = UnitGUID(unit)
guid  = guid:sub(11, 19)
return '0x'..guid

end

function Priest.dispellRotation(target,asap)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')


local purgeList = {
1044, -- Hand of Freedom
6940, -- Hand of Sacrifice
69369, -- Predatory Swiftness
12472, -- Icy Veins
1022, -- Hand of Protection
11426, -- Ice Barrier
--20925, -- Sacred Shield
--114250, -- Selfless Healer
17, -- Power Word: Shield
152118, --CoW
12043,
132158,
16188,
110909,
6346
--,974,774,155777
}

	if not enemyTargets
	then
		enemyTargets = {"arena1","arena2","arena3","arena4","arena5","target"}
	end

	local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		for _,dispell in pairs(Priest.dispelMagicCC) do
			if HaveDebuff(groupType..i, dispell) and isSpellAvailable(527) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40  and not HaveDebuff(groupType,30108) and asap == false then
				  print("Attempting to dispell "..GetSpellInfo(dispell))
				 AceCastSpellByName(tostring(GetSpellInfo(527)),groupType..i)
			end
		end
    end


	local canKill = false
	for y=1, 5 do
		local aTarget = "arena"..tostring(y)
		local aTargetHeath = 100 * UnitHealth(aTarget) / UnitHealthMax(aTarget)
		if aTargetHeath < 25 then
			canKill = true
		end
	end


	for _,etarget in pairs(enemyTargets) do

		for _,debuff in pairs(Priest.immunDispel) do
			if HaveBuff(etarget, debuff) and isSpellAvailable(32375) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) and asap == true then
				 AceCastSpellByName(tostring(GetSpellInfo(32375)))
				 AceRunMacroText("/script ClickArea('"..etarget.."')")
			end
		end

		for _,buff in pairs(purgeList) do
			if HaveBuff(etarget, buff) and isSpellAvailable(528) and InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 30 and UnitIsEnemy('player',etarget) and playerHealth > 50 and asap == true and UnitPower('player') > 30  then
				 AceCastSpellByName(tostring(GetSpellInfo(528),etarget))
			end
		end
	end


end
function Priest.survivalRotation(target)

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

if UnitCastingInfo("player") or UnitChannelInfo("player")  then
	return
end




if playerHealth < 70 and UnitAffectingCombat('player') and  isSpellAvailable(5512) then
			   AceRunMacroText('/use Healthstone')
	end

if playerHealth <60 and  isSpellAvailable(586) and UnitAffectingCombat('player') and IsLeftShiftKeyDown() then
		AceCastSpellByName(GetSpellInfo(586))
end


if playerHealth < 60 and  GetSpellCooldown(112833) ==0 and UnitAffectingCombat('player') and not HaveBuff('player',23335) and not HaveBuff('player',34976) and not HaveBuff('player',23333) then

		if HaveBuff('player',17) then
		--AceCastSpellByName(GetSpellInfo(112833))
		else
		--AceCastSpellByName(GetSpellInfo(17),'player')
		end
end



end
function Priest.avoidSpellInturupt()

if not pfooframe then
	pfooframe = CreateFrame("Frame")
    end
	local interruptID =     {
    [102060] = true,    --Disrupting Shout
    [106839] = true,    --Skull Bash
    [80964] = true,        --Skull Bash
    [115781] = true,    --Optical Blast
    [116705] = true,    --Spear Hand Strike
    [1766] = true,         --Kick
    [19647] = true,     --Spell Lock
    [2139] = true,         --Counterspell
    [47476] = true,        --Strangulate
    [47528] = true,     --Mind Freeze
    [57994] = true,     --Wind Shear
    [6552] = true,         --Pummel
    [96231] = true,     --Rebuke
    [31935] = true,        --Avenger's Shield
    [34490] = true,     --Silencing Shot
    [147362] = true     --Counter shot
    }

	--[[local SIN_PlayerGUID = UnitGUID("player")
    pfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
    pfooframe:SetScript("OnEvent", function(self, event, _, type, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
            if type == "SPELL_CAST_SUCCESS" and destGUID == SIN_PlayerGUID and interruptID[spellID] then
				print('stop')
                local isProtected =  select(9, UnitCastingInfo'player') or select(8, UnitChannelInfo'player')
                    if not isProtected and not HaveBuff('player', 96267) then 
						print("attempting to fake cast "..GetSpellInfo(spellID))
                        AceStopSpellCasting()
                    end
            end
    end);]]--
end

function preShield()

local groupType = IsInRaid() and "raid" or "party"
for i=1,GetNumGroupMembers() do

 local target = groupType..i
	if isSpellAvailable(17)  and not HaveBuff(target,17) and not  HaveDebuff(target,6788) then
			--print(GetSpellInfo(17))
			AceCastSpellByName(GetSpellInfo(17),target)
		end

end

end

function useSavingGrace(target)
local _,_,_,count = UnitDebuff('player',"Saving Grace")
if count == nil then count = 0 end
if count < 2 and not HaveBuff('player',114255) and not HaveBuff('player',157197) then
		return 152116
else
		return 2061
end
end
function Priest.healingRotation(target)

local dist = GetUnitDistance(target)
if dist == nil then dist = 50 end

if dist > 40 or UnitIsEnemy('player',target) or UnitIsDeadOrGhost(target) or UnitCastingInfo("player") or UnitChannelInfo("player")  or not InLineOfSight(target) == 1 then
	return
end



local targetHealth = CalculateHP(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
local isArena, isRegistered = IsActiveBattlefieldArena();
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	if specid == 256 then
	local allAbsorbs = UnitGetTotalAbsorbs(target) or 0


	-- if (UnitHealth(target) + allAbsorbs) > UnitHealthMax(target) then
		-- targetHealth = 100;
	-- else
		-- targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
	-- end


		if IsLeftShiftKeyDown() then
			preShield()
		end


		if targetHealth < 60 and isSpellAvailable(81700) then
				AceCastSpellByName(GetSpellInfo(81700))
		end


		local healToUse = useSavingGrace(target);

		if targetHealth < 35  and isSpellAvailable(33206) and UnitAffectingCombat(target) and not HaveBuff(target, getDefenceCds(),0) then
			--print(GetSpellInfo(33206))
			AceCastSpellByName(GetSpellInfo(33206),target)
		end


		if targetHealth < 40  and not isSpellAvailable(33206) and not HaveBuff('player',33206) and isSpellAvailable(62618) then
			--print(GetSpellInfo(33206))
			AceCastSpellByName(GetSpellInfo(62618))
			AceRunMacroText("/script ClickArea('"..target.."')")
		end




		if IsLeftAltKeyDown() and isSpellAvailable(healToUse) or HaveBuff('player',586,5) then
			AceCastSpellByName(GetSpellInfo(healToUse),target)
		end



		if targetHealth < 100  and isSpellAvailable(17)  and not HaveBuff(target,17) and not  HaveDebuff(target,6788) then
			--print(GetSpellInfo(17))
			AceCastSpellByName(GetSpellInfo(17),target)
		end

		if targetHealth < 75 or (targetHealth < 75 and HaveBuff('player',114255))  and spellReady(healToUse) then
			--print(GetSpellInfo(2061))
			AceCastSpellByName(GetSpellInfo(healToUse),target)
		end

		if playerHealth > 50 then
				Priest.dispellRotation(target,true)
		end



		if targetHealth < 85 and isSpellAvailable(47540) then
		--print(GetSpellInfo(47540))
			AceCastSpellByName(GetSpellInfo(47540),target)
		end

		Priest.dispellRotation(target,false)

		--if not HaveDebuff('player',allRoots()



		if targetHealth < 45 or (targetHealth < 75 and HaveBuff('player',114255))  and spellReady(healToUse) then
			--print(GetSpellInfo(2061))
			AceCastSpellByName(GetSpellInfo(healToUse),target)
		end

		if targetHealth < 95  and isSpellAvailable(152118) and  HaveDebuff(target,6788) and not HaveBuff(target,152118) and not MeleeAroundPlayer() then
                  --print(GetSpellInfo(17))
                  AceCastSpellByName(GetSpellInfo(152118),target)
		end



		if targetHealth < 80 and HaveBuff('player',114255)  and spellReady(healToUse) then
			--print(GetSpellInfo(2061))
			AceCastSpellByName(GetSpellInfo(healToUse),target)
		end

		if targetHealth < 99 and isSpellAvailable(33076) and not MeleeAroundPlayer() then
                  --print(GetSpellInfo(33076))
			AceCastSpellByName(GetSpellInfo(33076),target)
		end




		if targetHealth < 85 and isSpellAvailable(110744) then
			--print(GetSpellInfo(17))
			if not UnitIsUnit (target,'player') then
				--FaceUnit(target)
			end
			AceCastSpellByName(GetSpellInfo(110744),target)
		end

		if group.veryLow > 2 and isSpellAvailable(596) then
			--print(GetSpellInfo(596))
			AceCastSpellByName(GetSpellInfo(596),target)
		end

		if group.low > 1 and GetSpellCooldown(121135) ==0 then
			--print(GetSpellInfo(121135))
			AceCastSpellByName(GetSpellInfo(121135),target)
		end







		local healSpell = 2061
		if  not MeleeAroundPlayer() and HaveBuff('player',52798) then
			healSpell = 2061
		end


		if targetHealth < 75 and spellReady(healSpell) then
			--print("Heal 2")
			AceCastSpellByName(GetSpellInfo(healSpell),target)
		end
		
		if targetHealth < 100 and spellReady(2060) then
			--print("Heal 2")
			AceCastSpellByName(GetSpellInfo(2060),target)
		end

		else if specid == 257 then
		local heal = 2060
		local flashHeal = 2061


			if targetHealth < 25 and spellReady(47788) then
			--print(GetSpellInfo(33206))
					AceCastSpellByName(GetSpellInfo(47788),target)
			end

			if targetHealth < 100 and spellReady(17) and not HaveDebuff(target,6788) then
			--print(GetSpellInfo(17))
			AceCastSpellByName(GetSpellInfo(17),target)
			end

			if targetHealth < 99 and spellReady(139) and not HaveBuff(target,139) then
				--print(GetSpellInfo(17))
				AceCastSpellByName(GetSpellInfo(139),target)
			end

			if targetHealth < 75 and isSpellAvailable(88684) then
			--print(GetSpellInfo(47540))
				AceCastSpellByName(GetSpellInfo(88684),target)
			end


			if targetHealth < 85 and isSpellAvailable(110744) then
			--print(GetSpellInfo(17))
			AceCastSpellByName(GetSpellInfo(110744),target)
			end

			if group.veryLow > 2 and isSpellAvailable(596) and not UnitIsUnit('target',target) and not UnitIsUnit('player',target) then
				--print(GetSpellInfo(596))
				AceCastSpellByName(GetSpellInfo(596),target)
			end

			if targetHealth < 80 and isSpellAvailable(34861) then
				--print(GetSpellInfo(121135))
				AceCastSpellByName(GetSpellInfo(34861),target)
				--return
			end

			if targetHealth < 76 and isSpellAvailable(33076) then
				--print(GetSpellInfo(33076))
				AceCastSpellByName(GetSpellInfo(33076),target)
			end

			if group.low > 1 and GetSpellCooldown(121135) ==0 then
			--print(GetSpellInfo(121135))
				AceCastSpellByName(GetSpellInfo(121135),target)
			end


			if playerHealth < 90 and target2 ~= nil then
				heal = 32546
				flashHeal = 32546
				target = target2
				--print("tagret change to: "..target)
			end

		--binding
			if targetHealth < 45 then
				if HaveBuff('player',63733) then
					AceCastSpellByName(GetSpellInfo(heal),target)
				else
					AceCastSpellByName(GetSpellInfo(flashHeal),target)
				end
			end

			if targetHealth < 76 and isSpellAvailable(33076) then
				--print(GetSpellInfo(33076))
				AceCastSpellByName(GetSpellInfo(33076),target)
			end


			if targetHealth < 90 and spellReady(heal) then
				--print("Heal 2")

					AceCastSpellByName(GetSpellInfo(heal),target)
			end
		end
	end

	  if not HaveBuff('player',6346) and isSpellAvailable(6346) then
        AceCastSpellByName(GetSpellInfo(6346),'player')
    end

    if not HaveBuff('player',21562) and isSpellAvailable(21562) and not MeleeAroundPlayer() then
        AceCastSpellByName(GetSpellInfo(21562),'player')
    end
end

function Priest.damageRotation(target)

local targetHealth = CalculateHP(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')
local specid = select(1, GetSpecializationInfo(GetSpecialization()))

	if specid == 258 then

		local orbCount  = UnitPower("player" , SPELL_POWER_SHADOW_ORBS);
		local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')
		if orbCount == nil then orbCount = 0 end


		if playerHealth > 25 then
			Priest.dispellRotation(target,true)
		end

		--Cast Mind Spike  if you have a Surge of Darkness Icon Surge of Darkness proc (if you are using this talent).
		if HaveBuff('player',87160) then
			AceCastSpellByName(GetSpellInfo(73510))
		end


		--Always cast Shadow Word: Death a second time unless you have 5 Shadow Orbs. Both casts in the double-tap grant Shadow Orbs.
		--Shadow Word: Death is only usable on targets that are below 20% health.

		if orbCount < 5 and isSpellAvailable(32379) and targetHealth < 20 then
			AceCastSpellByName(GetSpellInfo(32379))
		end


		--Apply and maintain Shadow Word: Pain
		if not HaveDebuff('target',589) then
			AceCastSpellByName(GetSpellInfo(589))
		end
		--Apply and maintain  Vampiric Touch.
		if not HaveDebuff('target',34914) then
			AceCastSpellByName(GetSpellInfo(34914))
		end
		--Cast Devouring Plague  with 3 or more Shadow Orbs.
		if orbCount > 2 and isSpellAvailable(2944) then
			AceCastSpellByName(GetSpellInfo(2944))
		end
		--Cast Shadow Word: Death  if you have fewer than 5 Shadow Orbs.
		if orbCount < 5 and isSpellAvailable(32379) and targetHealth < 20 then
			AceCastSpellByName(GetSpellInfo(32379))
		end





		--Cast Mind Blast  if you have fewer than 5 Shadow Orbs.
		if orbCount < 5 and isSpellAvailable(8092) then
			AceCastSpellByName(GetSpellInfo(8092))
		end


		--Cast Insanity on the target when you have the Shadow Word: Insanity Icon Shadow Word: Insanity buff (if you are using the Insanity talent).


		--Cast Mind Flay as your filler spell.
		if not IsChanneling() then
			AceCastSpellByName(GetSpellInfo(15407))
		end
	end
end

function RotationTickPriest()

	local hasTarget = UnitExists("Target")
	local isDead = UnitIsDead("Target")

	local _,UnitClass=UnitClass("player")
	local specid = select(1, GetSpecializationInfo(GetSpecialization()))
	if UnitClass~="PRIEST" then
		return
	end

	if IsLeftShiftKeyDown() then
		local flag = { "Alliance Flag", "Horde Flag"  }
		for _,v in ipairs(flag) do
		 AceInteractUnit(v)
		end
	end

	local lowestPlayer,lowestTank = GetGroupInfo()
	local secondLowestPlayer,secondLowestTank

	if lowestPlayer[2] then secondLowestPlayer = lowestPlayer[2].Unit end

	if GetKeyState(HEALING_KEY) and specid ~= 258 then
		Priest.avoidSpellInturupt()
		Priest.survivalRotation(lowestPlayer[1].Unit)
		Priest.healingRotation(lowestPlayer[1].Unit,secondLowestPlayer)
		--for i = 1, #members do
		--	if UnitInRange(members[i].Unit) and not UnitIsUnit('target',members[i].Unit) and not UnitIsUnit('player',members[i].Unit) and InLineOfSight(members[i].Unit)  then
		--		Priest.healingRotation(members[i].Unit)
		--	end
		--end
	end
	if GetKeyState(DAMAGE_KEY) and hasTarget and InLineOfSight('target') == 1 and specid == 258 then
		Priest.avoidSpellInturupt()
		Priest.survivalRotation()
		Priest.damageRotation('target')
	end
end



