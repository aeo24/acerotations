addonName, Rotations = ...
Rotations.warrior = {}
Rotations.warrior.ROTATION = function()

	RotationTickWarrior()
	return false

end
Warrior = {}


function Warrior.survivalRotation()

local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

	if playerHealth < 70 and UnitAffectingCombat('player') and  isSpellAvailable(5512) then
			   AceRunMacroText('/use Healthstone')
	end

	if playerHealth < 30 and UnitAffectingCombat('player') and  isSpellAvailable(97462) then --RC
			   AceCastSpellByName(GetSpellInfo(97462))
	end

	if playerHealth < 50 and UnitAffectingCombat('player') then --dsatnce
				AceCastSpellByName(GetSpellInfo(71))
	end


	if playerHealth < 30 and UnitAffectingCombat('player') and  isSpellAvailable(112048) and not HaveBuff('player',112048) then ---SB
			   AceCastSpellByName(GetSpellInfo(112048))
	end

	if playerHealth < 30 and UnitAffectingCombat('player') and  isSpellAvailable(118038) and not HaveBuff('player',97462) then ---DBTS 118038
			   AceCastSpellByName(GetSpellInfo(118038))
	end



end

function Warrior.damageRotation()

local targetHealth = 100 * UnitHealth('target') / UnitHealthMax('target')
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local rage = UnitPower("player")
local _,_,_,rageingBlows,_,_,_,_,_=UnitBuff("player","Raging Blow","","player");
if rageingBlows == nil then rageingBlows = 0 end

	if specid == 71 then --Arms

		local _,_,_,_,_,_,rendTime,_,_= UnitDebuff('target',"Rend","","player");

		local rendEnd = 0
		if rendTime ~= nil then
			rendEnd = (rendTime -GetTime())
		end

		if isSpellAvailable(5308)  and HaveBuff('player',29725)  then
				AceCastSpellByName(GetSpellInfo(5308))
				return
		end


		-- 7.23 rend,if=!ticking&target.time_to_die>4
		if not UnitDebuff('target',"Rend","","player") or rendEnd < 5 then
			AceCastSpellByName(GetSpellInfo(772))

		end

		-- 0.00 ravager,if=cooldown.colossus_smash.remains<4
		if isSpellAvailable(152277) and  GetSpellCooldown(86346) < 4 then
			AceCastSpellByName(GetSpellInfo(152277))

		end

		-- 22.46 colossus_smash

		if isSpellAvailable(167105)  then
			AceCastSpellByName(GetSpellInfo(167105))
			return;
		end


		-- 73.44 mortal_strike,if=target.health.pct>20
		if isSpellAvailable(12294) and targetHealth > 20 then
			AceCastSpellByName(GetSpellInfo(12294))
			return;
		end

		-- 22.13 storm_bolt,if=(cooldown.colossus_smash.remains>4|debuff.colossus_smash.up)&rage<90
		if isSpellAvailable(107570) and   ( GetSpellCooldown(86346) > 4 or HaveDebuff('target',86346)) and rage < 70 then
			AceCastSpellByName(GetSpellInfo(107570))
			return

		end

		-- 0.00 siegebreaker
		if isSpellAvailable(176289) then
			AceCastSpellByName(GetSpellInfo(176289))

		end

		-- 0.00 dragon_roar,if=!debuff.colossus_smash.up


		-- 17.96 rend,if=!debuff.colossus_smash.up&target.time_to_die>4&ticks_remain<2
		if isSpellAvailable(772)  and not HaveDebuff('target',86346) and  rendEnd < 3 then
			AceCastSpellByName(GetSpellInfo(772))

		end

		-- 29.82 execute,if=(rage>=60&cooldown.colossus_smash.remains>execute_time)|debuff.colossus_smash.up|buff.sudden_death.react|target.time_to_die<5
		if isSpellAvailable(5308) and ((rage > 60 and targetHealth > 20) or ( HaveDebuff('target',86346) and targetHealth < 20)) or HaveBuff('player',29725) then
			AceCastSpellByName(GetSpellInfo(5308))

		end

		-- 7.25 impending_victory,if=rage<40&!debuff.colossus_smash.up&target.health.pct>20
		if isSpellAvailable(103840)  and rage < 40 and (not HaveDebuff('target',86346) and targetHealth > 20) then
			AceCastSpellByName(GetSpellInfo(103840))

		end

		-- 0.00 slam,if=(rage>20|cooldown.colossus_smash.remains>execute_time)&target.health.pct>20

		-- 156.84 whirlwind,if=!talent.slam.enabled&target.health.pct>20&(rage>=40|set_bonus.tier17_4pc|debuff.colossus_smash.up)
		if isSpellAvailable(1680)  and rage > 40 and targetHealth > 20 then
			AceCastSpellByName(GetSpellInfo(1680))

		end


	elseif specid == 72 then --Fury
		--Use Berserker Rage Icon Berserker Rage if your Enrage Icon Enrage falls off.
		if not HaveBuff('player',13046) and spellReady(18499) then
				AceCastSpellByName(GetSpellInfo(18499))
		end



--recklessness,if=target.health.pct<20&raid_event.adds.exists

--	wild_strike,if=rage>110&target.health.pct>20
if rage > 90 and spellReady(100130) and targetHealth > 20 then
				AceCastSpellByName(GetSpellInfo(100130))
		end
--	bloodthirst,if=(!talent.unquenchable_thirst.enabled&rage<80)|buff.enrage.down
	--Use Bloodthirst Icon Bloodthirst
		if (not HaveBuff('player',13046) or rage < 80) and isSpellAvailable(23881) then
				AceCastSpellByName(GetSpellInfo(23881))
				return
			end

--	ravager,if=buff.bloodbath.up|(!talent.bloodbath.enabled&(!raid_event.adds.exists|raid_event.adds.cooldown>60|target.time_to_die<40))

--	execute,if=buff.sudden_death.react
if isSpellAvailable(5308)  and HaveBuff('player',29725)  then
				AceCastSpellByName(GetSpellInfo(5308))
				return
		end
--	siegebreaker
--	storm_bolt

if isSpellAvailable(107570) then
			AceCastSpellByName(GetSpellInfo(107570))
			return

		end
--	wild_strike,if=buff.bloodsurge.up
if HaveBuff('player',46915) then
				AceCastSpellByName(GetSpellInfo(100130))
		end
--execute,if=buff.enrage.up|target.time_to_die<12
if HaveBuff('player',13046) and spellReady(5308) and targetHealth < 10 then
				AceCastSpellByName(GetSpellInfo(5308))
		end

--raging_blow
	if spellReady(85288) then
			AceCastSpellByName(GetSpellInfo(85288))
		end
--wild_strike,if=buff.enrage.up&target.health.pct>20
if HaveBuff('player',13046) and spellReady(100130) and targetHealth > 20 then
				AceCastSpellByName(GetSpellInfo(100130))
		end

--shockwave,if=!talent.unquenchable_thirst.enabled
--impending_victory,if=!talent.unquenchable_thirst.enabled&target.health.pct>20

end

end

function RotationTickWarrior()

local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")
local _,UnitClass=UnitClass("player")

	if UnitClass ~= "WARRIOR" then
			return
	end

	if IsLeftShiftKeyDown() then
		local flag = { "Alliance Flag", "Horde Flag"  }
		for _,v in ipairs(flag) do
			AceInteractUnit(v)
		end
	end

	if not enemyTargets
	then
		enemyTargets = {"target","focus","mouseover","arena1","arena2","arena3","arena4","arena5","arenapet1","arenapet2","arenapet3","arenapet4","arenapet5","pettarget"}
	end

	if GetKeyState('3') and hasTarget and InLineOfSight('target') then
		Warrior.survivalRotation()
		Warrior.damageRotation()
	end
end
