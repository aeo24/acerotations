addonName, Rotations = ...
Rotations.paladin = {}
Rotations.paladin.ROTATION = function()
	RotationTickPaladin()
	return false

end
Paladin = {}


function Paladin.dispellRotation(lowestTarget,secondLowestPlayer)

	local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		--print(UnitName('raid'..i))
		for _,dispell in pairs(dispelMagicCC) do
			if HaveDebuff(groupType..i, dispell) and isSpellAvailable(4987) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40  and (not HaveDebuff(groupType..i,30108) or IsLeftShiftKeyDown() or HaveBuff('player',642)) then
				 print("Attempting to dispell "..GetSpellInfo(dispell))
				 AceCastSpellByName(tostring(GetSpellInfo(4987)),groupType..i)
			end
		end
		for _,dispell in pairs(allRoots) do
			if HaveDebuff(groupType..i, dispell) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40 then
				 if isSpellAvailable(4987) then
					print("Attempting to dispell "..GetSpellInfo(dispell))
					AceCastSpellByName(tostring(GetSpellInfo(4987)),groupType..i)
				 end
			end
		end
		--[[for _,slow in pairs(getSlows()) do
			if HaveDebuff(groupType..i, slow) and InLineOfSight(groupType..i) == 1 and  GetUnitDistance(groupType..i) < 40 then
				if not HaveBuff(groupType..i,1044) and isSpellAvailable(1044)  then
					print("Freedom for "..GetSpellInfo(slow))
					AceCastSpellByName(tostring(GetSpellInfo(1044)),groupType..i)
				end
			end
		end]]--
    end
	for _,etarget in pairs(enemyTargets) do
	local distance = GetUnitDistance(etarget)
		local spellName, _, _, _, startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
			for _, v in ipairs(healingInterrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and GetUnitDistance(etarget) < 8 and currentPercent > 25  then
						print("Inturrupted: "..GetSpellInfo(v) )
						if isSpellAvailable(96231) and  distance < 7 then
							AceCastSpellByName(GetSpellInfo(96231),etarget)
						elseif isSpellAvailable(28730) and  distance < 7 then
							AceCastSpellByName(GetSpellInfo(28730),etarget)
						end
					end
				end
			end
		local spellName, _, _, _, startCast, endCast, _, canInterrupt = UnitChannelInfo(etarget)
			for _, v in ipairs(channelInturrupt) do
				if GetSpellInfo(v) == spellName and canInterrupt == false then
					local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
					local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
					local castTime = endCast - startCast
					local currentPercent = timeSinceStart / castTime * 100000
					if InLineOfSight(etarget) == 1 and currentPercent > 15  and  distance < 7   then
						AceCastSpellByName(GetSpellInfo(96231),etarget)
					end
				end
			end
	end
end


function Paladin.avoidSpellInturupt(lowestPlayer,secondLowestPlayer)

	for _,etarget in pairs(enemyTargets) do
		local distance = GetUnitDistance(etarget)
		local spellName, _, _, _,startCast, endCast, _, _, canInterrupt = UnitCastingInfo(etarget)
		for _, v in ipairs(polys) do
			if GetSpellInfo(v) == spellName and InLineOfSight(etarget) ==1 then
				local timeSinceStart = (GetTime() * 1000 - startCast) / 1000
				local timeLeft = ((GetTime() * 1000 - endCast) * -1) / 1000
				local castTime = endCast - startCast
				local currentPercent = timeSinceStart / castTime * 100000
				AceStopSpellCasting()
				if isSpellAvailable(96231) and  distance < 7  and currentPercent > 30 then
					AceCastSpellByName(GetSpellInfo(96231),etarget)
				elseif isSpellAvailable(28730) and distance < 7  and currentPercent > 30 then
					AceCastSpellByName(GetSpellInfo(28730),etarget)
				else
					if currentPercent > 30 then
						AceCastSpellByName(GetSpellInfo(6940),(UnitIsUnit('player',lowestPlayer) and secondLowestPlayer or lowestTarget) )
					end
				end
				return true
			end
		end
	end

if not dfooframe then
	dfooframe = CreateFrame("Frame")
end

	local SIN_PlayerGUID = UnitGUID("player")
    dfooframe:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	dfooframe:SetScript("OnEvent", function(self, event, args, type, _, sourceGUID, sourceName,sourceFlags, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
            if type == "SPELL_CAST_SUCCESS" and destGUID == SIN_PlayerGUID and interruptID[spellID] then
                local isProtected =  select(9, UnitCastingInfo('player')) or select(8, UnitChannelInfo('player'))
					if not isProtected and not HaveBuff('player',31821) and not HaveBuff('player', 96267)  and not HaveBuff('player', 31821) then --96267 = priest's cast immunity
						AceStopSpellCasting()
						AceStopSpellCasting()
                        AceStopSpellCasting()
						AceStopSpellCasting()
						print("Attempting to Juke "..GetSpellInfo(spellID))
						--[[local locType, _, _, _, _, _, _, _, _, _ = C_LossOfControl.GetEventInfo(1) or '';
						print('Locked '..locType)
						if locType == "SCHOOL_INTERRUPT" then
							print("Failed to Juke "..GetSpellInfo(spellID))
						end]]--
						--Paladin.healingRotation(lowestPlayer,secondLowestPlayer)
                    end
            end
			--[[if type == "SPELL_CAST_FAILED" and sourceGUID == SIN_PlayerGUID then
				if  extraSpellID ~= "Can't do that while moving" and  extraSpellID ~= "You are dead" and  extraSpellID ~= "Ability is not ready yet" and  extraSpellID ~= "Not yet recovered" then
					print("Failed to cast: "..GetSpellInfo(spellID).." "..extraSpellID)
				end
			end]]--

			if type == "SPELL_DISPEL"  and sourceGUID == SIN_PlayerGUID then
				print("Successfully "..GetSpellInfo(spellID).." removed "..(GetSpellInfo(extraSpellID) or " ").." from "..(destName or GetUnitName(target,false)))
			end
			if type == "SPELL_CAST_SUCCESS" and  destGUID == UnitGUID('player') then
				for _,dispell in pairs(getPolys()) do
					if dispell == spellID then
						AceStopSpellCasting()
						AceStopSpellCasting()
						AceStopSpellCasting()
						AceCastSpellByName(GetSpellInfo(6940),(UnitIsUnit('player',lowestPlayer) and secondLowestPlayer or lowestPlayer))
					end
				end
			end
			--[[if ( event == "LOSS_OF_CONTROL_ADDED" ) then
			local eventIndex = args;
			local locType, spellID, text, iconTexture, startTime, timeRemaining, duration, lockoutSchool, priority, displayType = C_LossOfControl.GetEventInfo(eventIndex);

			end]]--
    end);
	return false
end

function getBeaconHP()
local groupType = IsInRaid() and "raid" or "party"
	for i=1,GetNumGroupMembers() do
		local playerCount = 0
		local target = groupType..i
		if HaveBuff(target,53563,'player') then
			return target
		else
			return 'player'
		end
	end
	return 'player'
end

function isGCDOnCooldown(spellId)

local startDA,durDA,_ = GetSpellCooldown(spellId)
local cdDA = startDA + durDA - GetTime()

 return cdDA < GetSpellBaseCooldown(spellId) -2 and cdDA > 0

end

function Paladin.survivalRotation()

local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

	--if playerHealth < 15 and isSpellAvailable(642) and not HaveBuff('player',getFlags()) then
	--	AceStopSpellCasting()
	--	AceCastSpellByName(GetSpellInfo(642),'player') -- DS
	--end

	if playerHealth < 70 and UnitAffectingCombat('player') and  GetItemCooldown(5512) == 0 then
				   AceRunMacroText('/use Healthstone')
	end

	if HaveBuff('player',1044) then
		AceCastSpellByName(GetSpellInfo(85499),'player')
	end

	if playerHealth < 70 and isSpellAvailable(20925) and specid == 70 then
		AceCastSpellByName(GetSpellInfo(20925),'player')
	end

	if playerHealth < 80 and isSpellAvailable(498) then --DP
		AceCastSpellByName(GetSpellInfo(498),'player')
	end

	if playerHealth < 50 and isSpellAvailable(31821) and not HaveBuff('player',498) then --DA
		AceCastSpellByName(GetSpellInfo(31821),'player')
	end



	if not HaveBuff('player',642) and not HaveBuff('player',1022) and not HaveBuff('player',31821) then

		if playerHealth < 30 and isSpellAvailable(642) and not HaveBuff('player',getFlags()) then
			AceCastSpellByName(GetSpellInfo(642),'player') --DS
		end

		if playerHealth < 25 and isSpellAvailable(1022) and not HaveBuff('player',getFlags()) then
			AceCastSpellByName(GetSpellInfo(1022),'player') --HoP
		end

		if playerHealth < 20 and isSpellAvailable(633) then --Loh
			AceCastSpellByName(GetSpellInfo(633),'player')
		end
	end

end

function Paladin.damageRotation(target)

local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local holyPower  = UnitPower("player" , SPELL_POWER_HOLY_POWER);
local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
if holyPower == nil then holyPower = 0 end

if HaveBuff('player',86172) then
	if spellReady(85256) then
		AceCastSpellByName(GetSpellInfo(85256))
	else
		AceCastSpellByName(GetSpellInfo(53385))
	end
end

--Cast Templar's Verdict Icon Templar's Verdict (when you have 5 Holy Power).

if holyPower == UnitPowerMax('player', SPELL_POWER_HOLY_POWER) and spellReady(85256) then
	AceCastSpellByName(GetSpellInfo(85256))
end

--Cast Execution Sentence Icon Execution Sentence.
if isSpellAvailable(114157) and specid == 70 then
	AceCastSpellByName(GetSpellInfo(114157))
end

--Cast Hammer of Wrath Icon Hammer of Wrath on cooldown

if spellReady(24275) and (targetHealth < 20 or HaveBuff('player',31884)) then
	AceCastSpellByName(GetSpellInfo(24275),target)
end



--Cast Crusader Strike Icon Crusader Strike on cooldown (it generates Holy Power).

if spellReady(35395) and GetUnitDistance(target) < 8 then
	AceCastSpellByName(GetSpellInfo(35395),target)
end

--Cast Judgment Icon Judgment on cooldown (it generates Holy Power).

if spellReady(20271) then
	AceCastSpellByName(GetSpellInfo(20271),target)
end

--Cast Divine Storm Icon Divine Storm if you have the 4-Part Tier 16 Set Bonus, and you have a Divine Storm proc from it.

--Cast Exorcism Icon Exorcism on cooldown (it generates Holy Power).
if isSpellAvailable(879) and specid == 70 then
	AceCastSpellByName(GetSpellInfo(879))
end


--Cast Templar's Verdict Icon Templar's Verdict (when you have 3 Holy Power).
if holyPower > (UnitPowerMax('player', SPELL_POWER_HOLY_POWER) - 3) and spellReady(85256)  and specid == 70 then
	AceCastSpellByName(GetSpellInfo(85256),target)
end

--HolyShock
if isSpellAvailable(20473) then
	AceCastSpellByName(GetSpellInfo(20473),target)
end

if spellReady(2812) then
	AceCastSpellByName(GetSpellInfo(2812),target)
end




end

function preShield()

local groupType = IsInRaid() and "raid" or "party"
for i=1,GetNumGroupMembers() do

	local target = groupType..i
	if IsLeftShiftKeyDown()  and not HaveBuff(target, 20925) and GetSpellCharges(20925) > 0 then
		AceCastSpellByName(GetSpellInfo(20925),target)
	end

end

end
function attemptArenaKill()
arenaTargets = {"arena1","arena2","arena3","arena4","arena5"}
	for _, target in ipairs(arenaTargets) do
		local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
		if targetHealth < 25 and InLineOfSight(target) == 1 then
			Paladin.damageRotation(target)
		end
	end
end
function denounce(target,targetHealth,force)
	--Denounce
	local finish = 0
	local _,_,_,countDenounce,_,_,_,_,_=UnitBuff('player',GetSpellInfo(115654),"","player");
	if countDenounce == nil then
		 countDenounce = 0
	end
	if spellReady(2812) and targetHealth > 40 and (countDenounce > 2 or (targetHealth > 80 and not MeleeAroundPlayer() )) and UnitAffectingCombat('player') then
		if UnitExists('arena1') then
			for _,etarget in pairs(enemyTargets) do
				if UnitIsEnemy('player',etarget) and InLineOfSight(etarget) == 1 and not HaveBuff(etarget,getImmunes()) and not HaveDebuff(etarget,getDispellMagic()) and not HaveDebuff(etarget,51514) then
					AceCastSpellByName(GetSpellInfo(2812),etarget)
				end
			end
		else
			if UnitIsEnemy(target..'target','player') and InLineOfSight(target..'target') == 1  and not HaveBuff(target..'target',getImmunes()) and not HaveDebuff(target..'target',getDispellMagic()) and not HaveDebuff(target..'target',51514) then
				AceCastSpellByName(GetSpellInfo(2812),target..'target')
			else
				RunMacroText("/run TargetNearestEnemyPlayer()");
				if InLineOfSight('target') == 1 and not HaveDebuff('target',getDispellMagic()) and not HaveBuff('target',getImmunes()) and not HaveDebuff('target',51514)then
					AceCastSpellByName(GetSpellInfo(2812),'target')
				end
				RunMacroText("/run TargetLastTarget()");
			end
		end
	end
end
function holyShock(target,targetHealth,health)
	--HolyShock
	if targetHealth < health  and isSpellAvailable(20473) then
		AceCastSpellByName(GetSpellInfo(20473),target)
	end
end
function avengingWrath(target,targetHealth)
	--Wings
	if (targetHealth < 40 or group.veryLow > 2) and isSpellAvailable(31842) and not HaveBuff('player',642) then
			AceCastSpellByName(GetSpellInfo(31842))
	end
end
function handOfPurity(target,targetHealth)
	if HaveDebuff(target,{48181,172,980,30108}) and targetHealth < 50 and  isSpellAvailable(114039) then
		AceCastSpellByName(GetSpellInfo(114039),target)
	end
end

function sacredShield(target,targetHealth)
--Sacred Shield
	if (targetHealth < 100 or IsLeftShiftKeyDown()) and isSpellAvailable(20925) and not HaveBuff(target, 20925) then
		AceCastSpellByName(GetSpellInfo(20925),target)
	end
end

function handOfProtection(target,targetHealth)
	if targetHealth < 30 and spellReady(1022) and not HaveBuff(target,getFlags()) and not isSpellAvailable(31842) and not HaveBuff(target,getDefenceCds()) then --HoP if wings down
			AceCastSpellByName(GetSpellInfo(1022),target) --HoP
	end
end
function avengingWrathHeal(target,targetHealth)
	if HaveBuff('player',31842)  and targetHealth < 50 and isSpellAvailable(114157) then
		AceCastSpellByName(GetSpellInfo(114157),target)
	end

	if HaveBuff('player',31842) and targetHealth < 45 and isSpellAvailable(20473)  then
		AceCastSpellByName(GetSpellInfo(20473),target)
	end
end

function beaconOfLight(target,targetHealth)
	--Beacon
	local beaconHealth = 100 * UnitHealth(getBeaconHP()) / UnitHealthMax(getBeaconHP())
	if beaconHealth == 100 then beaconHealth = 111 end
	if targetHealth < 101 and isSpellAvailable(53563) and  (beaconHealth - targetHealth > 10) and not HaveBuff(target,53563) then
		AceCastSpellByName(GetSpellInfo(53563),target)
	end
end

function holyPrism(target,targetHealth)
	if targetHealth < 70 and isSpellAvailable(114165) then --Holy Prism
		AceCastSpellByName(GetSpellInfo(114165),target)
	end
end

function attemptFakeCast(target,targetHealth,health)
	if targetHealth > health and isSpellAvailable(19750)  and  canInturrupt() then
		AceCastSpellByName(GetSpellInfo(19750),target)
		 C_Timer.After(0.75, function()
		 local spellName, _, _, _, startCast, endCast, _, _, _ = UnitCastingInfo('player')
			--print(canInturrupt() )
			if spellName == 'Flash of Light' and   canInturrupt() then AceStopSpellCasting() end
		 end)
	end
end
function wordOfGlory(target,targetHealth)

local holyPower  = UnitPower("player" , SPELL_POWER_HOLY_POWER);
if holyPower == nil then holyPower = 0 end

	if targetHealth < 85 and isSpellAvailable(85673) and holyPower > 2 then -- WoG
		AceCastSpellByName(GetSpellInfo(85673),target)
	end
end

function infusionFlashOfLight(target,targetHealth)
	--HolyLight with infuson
	if targetHealth < 65 and isSpellAvailable(19750) and HaveBuff('player',54149) then
		AceCastSpellByName(GetSpellInfo(19750),target)
	end
end
function executionSentence(target,targetHealth)
	--Execution Sentence
	if targetHealth < 50 and isSpellAvailable(114157) then --ES
		AceCastSpellByName(GetSpellInfo(114157),target)
	end
end

function flashOfLight(target,targetHealth)
	--Flash of light
	if targetHealth < 65 and isSpellAvailable(19750) then
		AceCastSpellByName(GetSpellInfo(19750),target)
	end
end
function blessing()
	if not HaveBuff('player',20217) and not HaveBuff('player',19740) then
		if HaveBuff('player',1126) then
			AceCastSpellByName(GetSpellInfo(19740),'player')
		else

			AceCastSpellByName(GetSpellInfo(20217),'player')
		end
	end
end
function holyLight(target,targetHealth,health)
--HolyLight
	if targetHealth < 101 and isSpellAvailable(82326) then
		if UnitAffectingCombat('player') and not HaveBuff('player',54149) and targetHealth > health then
			AceCastSpellByName(GetSpellInfo(19750),target)
		else
			AceCastSpellByName(GetSpellInfo(82326),target)
		end
	end
end
function aeoHeal(target,targetHealth)
local holyPower  = UnitPower("player" , SPELL_POWER_HOLY_POWER);
if holyPower == nil then holyPower = 0 end
	if group.low > 4 and isSpellAvailable(85222) and MeleeAroundPlayer() and holyPower > 2 then
		AceCastSpellByName(GetSpellInfo(85222),target)
	end
end
function Paladin.healingRotation(target,target2)
local talentTierSeven = select(2, GetTalentRowSelectionInfo(7))

local holyPower  = UnitPower("player" , SPELL_POWER_HOLY_POWER);
if holyPower == nil then holyPower = 0 end

local dist = GetUnitDistance(target)
if dist == nil then dist = 50 end

if dist > 40 or UnitIsEnemy('player',target) or UnitIsDeadOrGhost(target) or UnitCastingInfo("player") or UnitChannelInfo("player")  or not InLineOfSight(target) == 1 then
	return
end

preShield()

local targetHealth = 100 * UnitHealth(target) / UnitHealthMax(target)
local playerHealth = 100 * UnitHealth('player') / UnitHealthMax('player')

--targetHealth = 40

if HaveBuff('player',31821) and InLineOfSight(target) == 1 then
	stopMoving()
end

beaconOfLight(target,targetHealth)
handOfProtection(target,targetHealth)
avengingWrath(target,targetHealth)
avengingWrathHeal(target,targetHealth)
holyShock(target,targetHealth,80)
executionSentence(target,targetHealth)

if playerHealth > 35 and targetHealth > 25 then
	Paladin.dispellRotation(target,target2)
end

if targetHealth > 30 then
	attemptArenaKill()
end
findTotems(20271)
--attemptFakeCast(target,targetHealth,60)
infusionFlashOfLight(target,targetHealth)
sacredShield(target,targetHealth)
denounce(target,targetHealth,false)
wordOfGlory(target,targetHealth)
flashOfLight(target,targetHealth)


tauntPet(62124)

blessing()
holyLight(target,targetHealth,80)

end

function RotationTickPaladin()


local hasTarget = UnitExists("Target")
local isDead = UnitIsDead("Target")
local specid = select(1, GetSpecializationInfo(GetSpecialization()))
local _,UnitClass=UnitClass("player")

if UnitClass~="PALADIN" then
	return
end


local flag = { "Alliance Flag", "Horde Flag"  }
for _,v in ipairs(flag) do
AceInteractUnit(v)
end


local lowestPlayer,lowestTank = GroupInfo()
local secondLowestPlayer,secondLowestTank

if lowestPlayer[2] then secondLowestPlayer = lowestPlayer[2].Unit end
if lowestTank[2] then secondLowestTank = lowestTank[2].Unit end
		--print(UnitName(lowestPlayer))
	stealthFinder(20271)
	Paladin.avoidSpellInturupt(lowestPlayer[1].Unit,secondLowestPlayer)
	if GetKeyState(HEALING_KEY) and specid == 65 then
		Paladin.survivalRotation()
		Paladin.healingRotation(lowestPlayer[1].Unit,secondLowestPlayer)
	end

	if GetKeyState(DAMAGE_KEY) and hasTarget and InLineOfSight('target') then
		Paladin.survivalRotation()
		Paladin.damageRotation('target')
	end
end



