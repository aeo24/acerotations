addonName, LURS = ...
LURS.paladin = {}
LURS.paladin.ROTATION = function()

local specID = GetInspectSpecialization() -- 256 Affli, 266 Demo, 267 Destro
local playerHP = 100 * UnitHealth("player") / UnitHealthMax("player")
local playerMana = 100 * UnitPower("player") / UnitPowerMax("player")
local targetHP = 100 * UnitHealth("target") / UnitHealthMax("target")
local holyPower = UnitPower("player", 9)
local spec = GetSpecialization() -- 1 Holy, 2 Prot, 3 Ret

--gran cruzad proc

		if UnitIsDeadOrGhost("player") == false then
			if playerHP < 25 
			and GetSpellCooldown("Imposición de manos") == 0
			then
			oexecute("CastSpellByName('Imposición de manos','player')")
			print("Imposición de manos")
					
			elseif playerHP < 35
			and GetSpellCooldown("Mano de protección") == 0
			then
			oexecute("CastSpellByName('Mano de protección','player')")
			print("Mano de protección")
			
			elseif playerHP < 35
			then
			oexecute("CastSpellByName('Destello de luz','player')")
			print("Destello de luz")
					
			elseif playerHP < 45
			and GetSpellCooldown("Escudo divino") == 0
			then
			oexecute("CastSpellByName('Escudo divino','player')")
			print("Escudo divino")
						
			elseif playerHP < 55
			and UnitBuff("player","Bastión de gloria")
			and GetSpellCooldown("Palabra de gloria") == 0
			and select(4, UnitBuff("player","Bastión de gloria")) >= 3
			then
			oexecute("CastSpellByName('Palabra de gloria','player')")
			print("Palabra de gloria")
			
			elseif playerHP < 65
			and UnitBuff("player","Sanación desinteresada")
			and GetSpellCooldown("Destello de luz") == 0
			and select(4, UnitBuff("player","Sanación desinteresada")) == 3 -- problemas aca Sanación desinteresada
			then
			oexecute("CastSpellByName('Destello de luz','player')")
			print("Destello de luz")
			end
			
			if not UnitBuff("player","Bendición de reyes") 
			and GetSpellCooldown("Bendición de reyes") == 0
			then
			oexecute("CastSpellByName('Bendición de reyes','player')")
			print("Bendición de reyes")
			end
			
			
		end
		
		-- Silenciar  --bastion de gloria proc -- can ttack target
	
	if UnitExists('target') then
			local name, _, _, _, startTime, endTime, _, _, notInterruptible = UnitCastingInfo("target")
			if name and not notInterruptible and GetSpellCooldown("Reprimenda") == 0 and IsSpellInRange("Reprimenda","target")==1 then
				if (GetTime()*1000 - startTime) * 100 / (endTime - startTime) > 50 then
					oexecute("SpellStopCasting()")
					oexecute("CastSpellByName('Reprimenda')")
				end
			end
	end

	if spec == 2 
	and UnitCanAttack("player", "target") == true 
	and UnitIsDeadOrGhost("target") == false-- Prot
	and IsCurrentSpell("Destello de luz") == false
	then
	
		if UnitExists('target') 
		and targetHP <= 20 
		and GetSpellCooldown("Martillo de cólera") == 0
		and  IsSpellInRange("Martillo de cólera", "target") == 1
		then
		oexecute("SpellStopCasting()")
		oface("target")
		oexecute("CastSpellByName('Martillo de cólera')")
		print("Martillo de cólera")
		
		elseif UnitExists('target')
		and holyPower == 3
		and playerHP < 50
		and GetSpellCooldown("Palabra de gloria") == 0
		and  IsSpellInRange("Palabra de gloria", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Palabra de gloria')")
		print("Palabra de gloria")
		
		elseif UnitExists('target')
		and holyPower == 3
		and GetSpellCooldown("Escudo del honrado") == 0
		and  IsSpellInRange("Escudo del honrado", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Escudo del honrado')")
				
		elseif UnitExists('target')
		and ActionButton2:GetButtonState() == 'PUSHED'
		and GetSpellCooldown("Golpe de cruzado") == 0
		and  IsSpellInRange("Golpe de cruzado", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Golpe de cruzado')")
		print("Golpe de cruzado")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Martillo del honrado") == 0
		and  IsSpellInRange("Martillo del honrado", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Martillo del honrado')")
		print("Martillo del honrado")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Sentencia") == 0
		and  IsSpellInRange("Sentencia", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Sentencia')")
		print("Sentencia")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Cólera Sagrada") == 0
		then
		oface("target")
		oexecute("CastSpellByName('Cólera Sagrada')")
		print("Cólera Sagrada")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Escudo de vengador") == 0
		and  IsSpellInRange("Escudo de vengador", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Escudo de vengador')")
		print("Escudo de vengador")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Consagración") == 0
		then
		oface("target")
		oexecute("CastSpellByName('Consagración')")
		print("Consagración")
		
		end
	
	elseif spec == 3 -- Ret 
		and UnitCanAttack("player", "target") == true 
		and UnitIsDeadOrGhost("target") == false-- Prot
		and IsCurrentSpell("Destello de luz") == false
		then
		
		if UnitExists('target') 
		and targetHP <= 20 
		and GetSpellCooldown("Martillo de cólera") == 0
		and  IsSpellInRange("Martillo de cólera", "target") == 1
		then
		oexecute("SpellStopCasting()")
		oface("target")
		oexecute("CastSpellByName('Martillo de cólera')")
		print("Martillo de cólera")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Golpe de cruzado") == 0
		and  IsSpellInRange("Golpe de cruzado", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Golpe de cruzado')")
		print("Golpe de cruzado")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Sentencia") == 0
		and  IsSpellInRange("Sentencia", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Sentencia')")
		print("2. Sentencia")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Cólera sagrada") == 0
		and  IsSpellInRange("Cólera sagrada", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Sentencia')")
		print("2. Sentencia")
		
		elseif UnitExists('target')
		and GetSpellCooldown("Exorcismo") == 0
		and  IsSpellInRange("Exorcismo", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Exorcismo')")
		print("2. Exorcismo")
		
		elseif UnitExists('target')
		and holyPower == 3
		and GetSpellCooldown("Veredicto del templario") == 0
		and  IsSpellInRange("Veredicto del templario", "target") == 1
		then
		oface("target")
		oexecute("CastSpellByName('Veredicto del templario')")
		print("2. Veredicto del templario")
		
		end
		
	end
	
end