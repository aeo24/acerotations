## Title: Ace Rotations
## Version: 1.0
## Author: Ace
## Interface: 50400
## Title: Ace Rotations Helper
## SavedVariablesPerCharacter: LURS_Settings,DAMAGE_KEY,HEALING_KEY



AceRotations.lua
AceRotations.xml

Libs\ObjectManager.lua
Libs\Acefunctions.lua
Libs\WWFfunctions.lua
Libs\LURSBindings.lua
Libs\Overrides.lua
Libs\TimeToDie.lua
Libs\GroupManager.lua
Libs\Fishing.lua

Profiles\DEATHKNIGHT\Rotation-deathknight.lua
Profiles\DRUID\Rotation-druid.lua
Profiles\HUNTER\Rotation-hunter.lua
Profiles\MAGE\Rotation-mage.lua
Profiles\MONK\Rotation-monk.lua
Profiles\PALADIN\Rotation-paladin.lua
Profiles\PRIEST\Rotation-priest.lua
Profiles\ROGUE\Rotation-rogue.lua
Profiles\SHAMAN\Rotation-shaman.lua
Profiles\WARLOCK\Rotation-warlock.lua
Profiles\WARRIOR\Rotation-warrior.lua

