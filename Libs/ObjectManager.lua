----------------------------------------------------------------------------------------------------
-- OBJECT MANAGER
----------------------------------------------------------------------------------------------------

--[[------------------------------------------------------------------------------------------------
    Name: ObjectManager
    Type: Function
    Arguments: range - int variable representing radius in yards to search from 'player'
    Return: None
    Description:
--]]
Ace.unitCacheAll = { }
Ace.unitCache = { }
Ace.unitCacheFriend = { }

local tableStorageCacheAll = { }  -- Create a table that will store unused table memory addresses
local tableStorageCache = { }  -- Create a table that will store unused table memory addresses
local tableStorageCacheFriend = { } 

local function ClearUnitCacheAll()
    for i = #Ace.unitCacheAll, 1, -1 do
        tinsert(tableStorageCacheAll, tremove(Ace.unitCacheAll))
    end
end
local function ClearUnitCache()
    for i = #Ace.unitCache, 1, -1 do
        tinsert(tableStorageCache, tremove(Ace.unitCache))
    end
end
local function ClearUnitCacheFriend()
    for i = #Ace.unitCacheFriend, 1, -1 do
        tinsert(tableStorageCacheFriend, tremove(Ace.unitCacheFriend))
    end
end

local function GetTableCacheAll()
    local t = tremove(tableStorageCacheAll)
    if t ~= nil then
        tinsert(Ace.unitCacheAll, t)
        return t
    else
        t = { }
        tinsert(Ace.unitCacheAll, t)
        return t
    end
end
local function GetTableCache()
    local t = tremove(tableStorageCache)
    if t ~= nil then
        tinsert(Ace.unitCache, t)
        return t
    else
        t = { }
        tinsert(Ace.unitCache, t)
        return t
    end
end
local function GetTableCacheFriend()
    local t = tremove(tableStorageCacheFriend)
    if t ~= nil then
        tinsert(Ace.unitCacheFriend, t)
        return t
    else
        t = { }
        tinsert(Ace.unitCacheFriend, t)
        return t
    end
end
function UnitCacheManager(range)
    ClearUnitCacheAll()
    ClearUnitCache()
	ClearUnitCacheFriend()
	if not UnitExists('arena1') or not InActiveBattlefield() or GetNumGroupMembers() == 0 then
		return
	end
    if range == nil then
        range = 40
    end


	local objectCount = ObjectCount()
	for i=1, objectCount do
            local object = GetObjectById(i)
            local objectExists = CallSecureFunction('UnitExists',object)

            if object then

                    local objectDistance = GetUnitDistance(object) or 50

                    if objectDistance <= range and InLineOfSight(object) == 1 then
					
                        local objectHealth = UnitHealth(object)
                        local objectIsDead = UnitIsDead(object)


                        if objectHealth > 0 and not objectIsDead then
                            local objectHealthMax = UnitHealthMax(object)
                            local objectHealthPercentage = math.floor((objectHealth / objectHealthMax) * 100)
                            local objectName = UnitName(object)
                            local objectReaction = UnitReaction(object,"player")

                            if objectReaction and objectReaction <= 4 then
							--Enemy
                                local tableCacheAll = GetTableCacheAll()
                                tableCacheAll.object = object
								tableCacheAll.distance = objectDistance

								local tableCache = GetTableCache()
								tableCache.object = object
								tableCache.name = objectName
								tableCache.health = objectHealthPercentage
								tableCache.distance = objectDistance
								
								if Ace.nearestUnitDistance == nil then
									Ace.nearestUnitDistance = objectDistance
									Ace.nearestUnit = object
								elseif objectDistance < Ace.nearestUnitDistance then
									Ace.nearestUnitDistance = objectDistance
									Ace.nearestUnit = object
								end
								
							else
							--Friendly
							if UnitInParty(object) or UnitInRaid(object) then
								local tableCacheFriend = GetTableCacheFriend()
                                tableCacheFriend.object = object
								tableCacheFriend.distance = objectDistance
								tableCacheFriend.name = objectName
								tableCacheFriend.health = objectHealthPercentage
								tableCacheFriend.TTD = NewTimeToX(1,1,object)
								
							end	
                            end
                        end
                    end

            end

    end

end

function eventHandlerSetup()

	if not eventFrame then
		eventFrame = CreateFrame("Frame")
	end

	local SIN_PlayerGUID = UnitGUID("player")
    eventFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	eventFrame:RegisterEvent("LOSS_OF_CONTROL_UPDATE");
	eventFrame:RegisterEvent("LOSS_OF_CONTROL_ADDED");
	eventFrame:RegisterEvent("UPDATE_SHAPESHIFT_FORM")
	eventFrame:RegisterEvent("PLAYER_ENTERING_WORLD");
	eventFrame:SetScript("OnEvent", function(self, event,args, type, _, sourceGUID, sourceName, _, _, destGUID, destName, _, _, spellID,spellname, spellschool, extraSpellID, extraSpellName, extraSchool)
            --COmbat Log Unfiltered
		
			if type == "SPELL_CAST_SUCCESS" and sourceGUID == SIN_PlayerGUID then
				print("Casting "..GetSpellInfo(spellID).."("..spellID..")")
				Ace.lastCast = spellID
				updateLastSpell(spellID)
				updateCurrentTarget(destName)
			end

			if type == "SPELL_DISPEL"  and sourceGUID == SIN_PlayerGUID then
				print("Successful "..GetSpellInfo(spellID).." removed "..(GetSpellInfo(extraSpellID) or " ").." at "..(destName or GetUnitName(target,false)))
			end


			if type == "SPELL_CAST_FAILED" and sourceGUID == SIN_PlayerGUID then
				if  extraSpellID ~= "Can't do that while moving" and  extraSpellID ~= "You are dead" and  extraSpellID ~= "Ability is not ready yet" and  extraSpellID ~= "Not yet recovered" then
					--print("Failed to cast: "..GetSpellInfo(spellID).." "..extraSpellID)
				end
			end


			--Other Events
			if ( event == "LOSS_OF_CONTROL_ADDED" or event == "LOSS_OF_CONTROL_UPDATE" ) then
				local eventIndex = C_LossOfControl.GetNumEvents()
				local locType, spellID, text, iconTexture, startTime, timeRemaining, duration, lockoutSchool, priority, displayType = C_LossOfControl.GetEventInfo(eventIndex);
				if locType == 'SILENCE' or locType == 'SCHOOL_INTERRUPT' or locType == 'STUN_MECHANIC' and not HaveBuff('player',5487) and not HaveBuff('player',33891)then
		
				end
			end
			if ( event == "PLAYER_ENTERING_WORLD") then
				 clearTimeToDieTable()
			end

    end);
end










