function GetLatency()
    		local latencyHome = select(3,GetNetStats()) or 100
    		local latencyWorld = select(4,GetNetStats()) or 100
    		if latencyWorld >= latencyHome then return latencyWorld/1000 else return latencyHome/1000 end
	end
	
	function Anti_Spam()
    		if GetTime() - Last_Activity_Time > Sleep_Time then
        		Last_Activity_Time = GetTime()
        		Sleep_Time = GetLatency()
        		return true
    		end
	end
	
	NBBL = nil
	function NBBL()
		if UnitDebuffID("player", 142913) then
			return true
		else
			return false
		end
	end
	
	SHBL = nil
	function SHBL()
		if UnitDebuffID("player", 137633) -- Tortos: Crystal Shell
		or (UnitDebuffID("player", 142861) and (UnitDebuffID("player", 142864) or UnitDebuffID("player", 142865))) -- Malkorok: Ancient Miasma and Ancient Barrier or Strong Ancient Barrier
		then
			return true
		else
			return false
		end
	end
	
	HaveGlyph = nil
	function HaveGlyph(id)
		
		for i = 1, GetNumGlyphSockets() do
			if select(4, GetGlyphSocketInfo(i)) == id then return true end
		end
		
		return false
		
	end
	
	GetSpellCD = nil
	function GetSpellCD(SpellID)
		
		if GetSpellCooldown(GetSpellInfo(SpellID)) == nil 
		or GetSpellCooldown(GetSpellInfo(SpellID)) == 0 then
			return 0
		else	
			local Start, Duration = GetSpellCooldown(GetSpellInfo(SpellID))
			return Start + Duration - GetTime()
		end
		
	end
	
	AggroCheck = nil
	function AggroCheck(unit1, unit2)
		
		if UnitExists(unit1) and UnitExists(unit2) then
			if not UnitThreatSituation(unit1, unit2) then
				return false
			elseif UnitThreatSituation(unit1, unit2) >= 2 then
				return true
			else
				return false
			end
		else
			return false
		end
		
	end
	
	CTT = nil
	function CTT()
		
		local t = 1
		local TrinketsTimers = {0, 0}
		
		local TrinketsBuffID = {
		138699,	--	Vicious Talisman of the Shado-Pan Assault
		138938,	--	Juju Madness
		138756,	--	Blades of Renataki
		146308,	--	Assurance of Consequence
		148896,	--	Sigil of Rampage
		146310,	--	Ticking Ebon Detonator
		148903,	--	Haromm's Talisman
							   }
		
		for i = 1, #TrinketsBuffID do
			if UnitBuffID("player", TrinketsBuffID[i]) then
				TrinketsTimers[t] = select(7,UnitBuffID("player", TrinketsBuffID[i])) - GetTime()
				if t == 1 then t = 2 end
			end
		end
		
		return TrinketsTimers[1], TrinketsTimers[2]
		
	end
	
	CBLT = nil
	function CBLT()
		
		local BLTimer = 0
		local BLid = {
					 	2825, 	--	Bloodlust
					 	32182,	--	Heroism
					 	80353,	--	Time Warp
					 	90355	--	Ancient Hysteria
					 }
		
		for i = 1, #BLid do
			if UnitBuffID("player", BLid[i]) then
				BLTimer = select(7, UnitBuffID("player", BLid[i])) - GetTime()
			end
		end
		
		return BLTimer
		
	end
	
	InstanceDifficulty = nil
	function InstanceDifficulty()
		
		ID = select(3, GetInstanceInfo())
		diff = "OutOfInstance"
		
		if ID == 0 then
			diff = "OutOfInstance"
		elseif ID == 1 or ID == 2 then
			diff = "5ppl"
		elseif ID == 3 or ID == 4 then
			diff = "NormalRaid"
		elseif ID == 5 or ID == 6 then
			diff = "HeroicRaid"
		elseif ID == 7 then
			diff = "LFR"
		elseif ID == 8 then
			diff = "ChallengeMode"
		elseif ID == 9 then
			diff = "40pplRaid"
		elseif ID == 11 then
			diff = "HeroicScenario"
		elseif ID == 12 then
			diff = "ScenarioInstance"
		elseif ID == 14 then
			diff = "Flex"
		end
		
		return diff
		
	end
	
	Range = nil
	function Range(unit1,unit2)
		
		if UnitIsUnit(unit1,unit2)
		or not UnitExists(unit1)
		or not UnitExists(unit2)
		then return nil end
		
		local x1,y1 = GetPlayerMapPosition(unit1)
		local x2,y2 = GetPlayerMapPosition(unit2)
		
		if x1 <= 0 and y1 <= 0 then return nil end
		if x2 <= 0 and y2 <= 0 then return nil end
	    
		local _,_,_,d,e,f,g,_,_,_ = GetAreaMapInfo(GetCurrentMapAreaID())
		
		local width = (d - e)
		local height = (f - g)
		
		local dx = (x2 - x1) * width
		local dy = (y2 - y1) * height
		
		return sqrt(dx*dx + dy*dy) -- distance in yard
		
	end
	
	RangeCheck = nil
	function RangeCheck(dist)
		
		if IsInRaid() then group = "raid"
		elseif IsInGroup() then group = "party"
		end
		
		for i=1, GetNumGroupMembers() do
			Distance = Range("player",group..i)
	   		if Distance ~= nil and Distance <= dist then return true end
		end
		
		return false
		
	end
	
	function CancelBuffByID(ID)
		local index = 1
		while UnitAura("player", index, "HELPFUL") do
			if select(11, UnitAura("player", index, "HELPFUL")) == ID then
				CancelUnitBuff("player", index, "HELPFUL")
				return
			end
			index = index + 1
		end
	end
	
	IsActiveTrinket = nil
	function IsActiveTrinket(table)
		
		local Trinket1 = GetInventoryItemID("player", 13)
		local Trinket2 = GetInventoryItemID("player", 14)
		local TrinketID = table or {"empty"}
		
		for i = 1, #TrinketID do
			if Trinket1 == TrinketID[i] or Trinket2 == TrinketID[i] then
				return true
			end
		end
		
		return false
		
	end
	
	TickingEbonDetonator = {
		105114,	-- 528 Raid Finder
		104865,	-- 540 Flexible
		102311,	-- 553 Normal
		105363,	-- 559 Normal Warforged
		104616,	-- 566 Heroic
		105612	-- 572 Heroic Warforged
				}
	
	FocusNPC = nil
	function FocusNPC(table)
		
		local name, realm = UnitName("target")
		if realm == nil then
			if name == nil then
				lasttargetname = "none"
			else lasttargetname = name end
		else
			lasttargetname = name.."-"..realm
		end
		
		local NPC_Table = table or {"empty"}
		
		for i = 1, #NPC_Table do
			oexecute("TargetUnit("..NPC_Table[i]..",true)")
			if UnitExists("target") then
				if UnitName("target") == NPC_Table[i] then
					oexecute("FocusUnit('target')")
					if lasttargetname == "none" then
						oexecute("ClearTarget()")
					else
						oexecute("RunMacroText('/targetexact '"..lasttargetname..")")
					end
					return true
				end
			end
		end
		
	end
	
	-- Check Debuff
	DispelValidation = nil
	function DispelValidation(t)          
		
		local HasValidDispel = false
		local i = 1
		local debuff = UnitDebuff(t, i)
		
		while debuff do
			
			local debuffType = select(5, UnitDebuff(t, i))
			
			if debuffType == "Poison" 
			or debuffType == "Disease" 
			then
				HasValidDispel = true
			end
			
			i = i + 1
			debuff = UnitDebuff(t, i)
			
		end
		
		return HasValidDispel
		
	end
	
	LoSTable = nil
	LoSTable = { }
	IsLineOfSight = nil
	function IsLineOfSight(targetCheck, time)
		local time=time or 3
		targetGUID = UnitGUID(targetCheck) or 0    
		
		for i=1, #LoSTable do
			if time < (GetTime() - LoSTable[i].time) then
 				table.remove(LoSTable, i)
				break
			end
		end
		
		function LoSCheck(self, event, ...)
			local targetGUID = targetGUID
			local Log = {...}
			local AlreadyTabled = false
			if Log[2] and Log[2] == "SPELL_CAST_FAILED" and targetGUID then
			
				if Log[4] and Log[4]==UnitGUID("player") then
					if Log[15] and Log[15]==SPELL_FAILED_LINE_OF_SIGHT 
					or Log[15] and Log[15]==SPELL_FAILED_NOT_INFRONT
					or Log[15] and Log[15]==SPELL_FAILED_OUT_OF_RANGE 
					or Log[15] and Log[15]==SPELL_FAILED_UNIT_NOT_INFRONT 
					or Log[15] and Log[15]==SPELL_FAILED_UNIT_NOT_BEHIND 
					or Log[15] and Log[15]==SPELL_FAILED_NOT_BEHIND 
					or Log[15] and Log[15]==SPELL_FAILED_MOVING 
					or Log[15] and Log[15]==SPELL_FAILED_IMMUNE 
					or Log[15] and Log[15]==SPELL_FAILED_FLEEING 
					or Log[15] and Log[15]==SPELL_FAILED_BAD_TARGETS 
					or Log[15] and Log[15]==SPELL_FAILED_NO_MOUNTS_ALLOWED 
					or Log[15] and Log[15]==SPELL_FAILED_STUNNED 
					or Log[15] and Log[15]==SPELL_FAILED_SILENCED 
					or Log[15] and Log[15]==SPELL_FAILED_NOT_IN_CONTROL 
					or Log[15] and Log[15]==SPELL_FAILED_VISION_OBSCURED
					or Log[15] and Log[15]==SPELL_FAILED_DAMAGE_IMMUNE
					or Log[15] and Log[15]==SPELL_FAILED_CHARMED 
					then
						for j=1, #LoSTable do
							if targetGUID == LoSTable[j].unit then
								AlreadyTabled = true
								break
							end
						end
						if not AlreadyTabled then
							table.insert(LoSTable, { unit=targetGUID, time=GetTime() } )
							targetGUID = nil
							_G.NovaLineOfSight:UnregisterAllEvents()
						end
					end
				end
			end        
		end
    	
    	if not NovaLineOfSight then
			frameLOS = CreateFrame("frame", "NovaLineOfSight")
			frameLOS:SetScript("OnEvent", LoSCheck)
		end
		
		for k=1, #LoSTable do
			if targetGUID and targetGUID == LoSTable[k].unit then
				return false
			end
		end
		
		_G.NovaLineOfSight:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
		return true
		
	end
