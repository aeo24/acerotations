
local A_TTD = {};

A_TTD.Settings = {
	Refresh = 0.1, -- Refresh time (seconds) : min=0.1, max=2, default = 0.2, aethys = 0.1
	HistoryTime = 20, -- History time (seconds) : min=5, max=120, default = 20, aethys = 10
	HistoryCount = 100 -- Max history count : min=20, max=500, default = 120, aethys = 100
};
A_TTD.Units = {};
A_TTD.Cache = {};
A_TTD.Throttle = 0;

function clearTimeToDieTable()
	table.empty(A_TTD.Units)
end
function TimeToDieRefresh (unitTable)

	if not unitTable then return end
	local FetchedUnits = unitTable;
	local TTDUnits = A_TTD.Units;
	local CurrentTime = GetTime();
	

	for Key, Value in pairs(TTDUnits) do
	local found = false;
		for i = 1, #FetchedUnits do
			if Key == UnitGUID(FetchedUnits[i].Unit) then found = true end
		end
		if found == false then TTDUnits[Key] = nil end
	end
	
	
	for i = 1, #FetchedUnits do
		local ThisUnit = FetchedUnits[i].Unit;
		local thisGUID = UnitGUID(FetchedUnits[i].Unit)
		if UnitExists(ThisUnit) and not UnitIsDeadOrGhost(ThisUnit) then 
			local ThisUnitString = thisGUID;
			local ThisUnitHealth = UnitHealth(ThisUnit);
			if not TTDUnits[ThisUnitString] or ThisUnitHealth > (TTDUnits[ThisUnitString][1][1][2]  )then
				TTDUnits[ThisUnitString] = {{}, UnitHealthMax(thisGUID), CurrentTime, -1};
			end
			local Values = TTDUnits[ThisUnitString][1];
			local Time = CurrentTime - TTDUnits[ThisUnitString][3];
			if ThisUnitHealth ~= TTDUnits[ThisUnitString][4] then
				table.insert(Values, 1, {Time, ThisUnitHealth});
				while (#Values > A_TTD.Settings.HistoryCount) or (Time - Values[#Values][1] > A_TTD.Settings.HistoryTime)  do
					table.remove(Values);
				end
				if ThisUnitHealth == UnitHealthMax(thisGUID) then 
					table.remove(Values);
				end
				TTDUnits[ThisUnitString][4] = ThisUnitHealth;
			end
		end
	end
end

function NewTimeToX (Percentage, MinSamples,unit)
	local Seconds = 555;
	local MaxHealth, StartingTime, CurrentTime;
	local UnitTable = A_TTD.Units[UnitGUID(unit) or unit];
	local MinSamples = MinSamples or 1;
	local a, b = 0, 0;
	-- Simple linear regression
	-- ( E(x^2)   E(x) )  ( a )   ( E(xy) )
	-- ( E(x)       n  )  ( b ) = ( E(y)  )
	-- Format of the above: ( 2x2 Matrix ) * ( 2x1 Vector ) = ( 2x1 Vector )
	-- Solve to find a and b, satisfying y = a + bx
	-- Matrix arithmetic has been expanded and solved to make the following operation as fast as possible
	if UnitTable then
		local Values = UnitTable[1];
		local n = #Values;
		if n > MinSamples then
			MaxHealth = UnitTable[2];
			StartingTime = UnitTable[3];
			CurrentTime = GetTime();
			local x, y = 0, 0;
			local Ex2, Ex, Exy, Ey = 0, 0, 0, 0;
			
			for _, Value in ipairs(Values) do
				x, y = unpack(Value);

				Ex2 = Ex2 + x * x;
				Ex = Ex + x;
				Exy = Exy + x * y;
				Ey = Ey + y;
			end
			-- Invariant to find matrix inverse
			local Invariant = Ex2*n - Ex*Ex;
			-- Solve for a and b
			a = (-Ex * Exy / Invariant) + (Ex2 * Ey / Invariant);
			b = (n * Exy / Invariant) - (Ex * Ey / Invariant);
		end
	end
	if b ~= 0 then
		-- Use best fit line to calculate estimated time to reach target health
		Seconds = (Percentage * 0.01 * MaxHealth - a) / b;
		-- Subtract current time to obtain "time remaining"
		Seconds = math.min(55555, Seconds - (CurrentTime - StartingTime));
		if Seconds < 1 then Seconds = 555; end
	end
	return math.floor(Seconds);
end 