function bestTarget(x,y)
	local xUnit = x.Unit
	local yUnit = y.Unit
	
	--local TTDx = NewTimeToX(1,1,xUnit)
	--local TTDy = NewTimeToX(1,1,yUnit)
	local TTDx = GetUnitPriority(xUnit)
	local TTDy = GetUnitPriority(yUnit)

	if TTDx == TTDy  then
		return x.HP < y.HP
	else
		return TTDx > TTDy
	end
	--return  select(2, CalculateHP(x.Unit)) < select(2, CalculateHP(y.Unit))
end
function GetUnitPriority(unit)

local UnitScore = 2
local UnitTimeToDie = NewTimeToX(1,1,unit)
local UnitRole = UnitGroupRolesAssigned(unit)
local UnitDistance  = GetUnitDistance(unit) or 50
local UnitHealthDifference = 100 - (100 * UnitHealth(unit) / UnitHealthMax(unit))

UnitScore = UnitScore + UnitHealthDifference
if UnitTimeToDie < 21 then 
	UnitScore = UnitScore + (20 - UnitTimeToDie)
end

if UnitRole == "TANK" or HaveBuff(unit,getFlags()) or UnitIsUnit('focus',unit) then
	UnitScore = UnitScore + 2;
end

if HaveBuff(unit,getDefenceCds()) then
		UnitScore = UnitScore + 2;
end

if not UnitAffectingCombat(unit) then
		UnitScore = UnitScore - 2;
end

if UnitIsAFK(unit) then
		UnitScore = UnitScore -1;
end
return math.floor(UnitScore)
end

function GetGroupInfo()
	members, group ={ }, { low = 0,veryLow = 0}
	allUnits  =  {}
	table.insert( members,{ Unit = 'player', HP = (100 * UnitHealth('player') / UnitHealthMax('player')) , UTT = 'target',aHP = 0,Distance = 0} )
	table.insert( allUnits,{ Unit = 'player', HP = (100 * UnitHealth('player') / UnitHealthMax('player')) , UTT = 'target',aHP = 0,Distance = 0} )
	if IsInRaid() then 
        group.type, group.number = "raid", GetNumGroupMembers()
    else
 
        group.type, group.number = "party", GetNumGroupMembers() - 1
    end



   for i = 1, group.number do
   
		local dist = GetUnitDistance(group.type..i) or 50
		local health = 100 * UnitHealth(group.type..i) / UnitHealthMax(group.type..i)
		local unit, hp, utt = group.type..i,health, group.type..i.."target"
		
		if health < 85 and dist < 50 then
			group.low = group.low + 1
		end
		if health < 60 and  dist < 50 then
			group.veryLow = group.veryLow + 1
		end


		
		if dist < 41 then
			table.insert( members,{ Unit = unit, HP = hp , UTT = utt,aHP = 0,Distance = dist } )	
		end
		table.insert( allUnits,{ Unit = unit, HP = hp , UTT = utt,aHP = 0,Distance = dist } )	
		if group.veryLow < 2 then
			dist = GetUnitDistance(group.type..'pet'..i) or 50
			if(dist < 40 and validTarget(group.type..'pet'..i))then

			 local phealth = 100 * UnitHealth(group.type..'pet'..i) / UnitHealthMax(group.type..'pet'..i)
			 local unit, hp, utt = group.type..'pet'..i, phealth, group.type..'pet'..i.."target"
			 table.insert( members,{ Unit = unit, HP = hp , UTT = utt,aHP = 0,Distance = dist } )
			 table.insert( allUnits,{ Unit = unit, HP = hp , UTT = utt,aHP = 0,Distance = dist } )	
			end
		end
  end

  table.sort(members, bestTarget)
  
  
return members
  
end