	IsChanneling = nil
	function IsChanneling()

		local _, _, _, _, _, playerChanneling = UnitChannelInfo("player")
		local isPlayerChanneling = false

		if playerChanneling ~= nil then
			local currentTime = GetTime() * 1000
			if (playerChanneling - currentTime) > 100 then
				isPlayerChanneling = true
			end
		end

		return isPlayerChanneling

	end

	IsCasting = nil
	function IsCasting()

		local _, _, _, _, _, playerCasting = UnitCastingInfo("player")
		local isPlayerCasting = false

		if playerCasting ~= nil then
			local currentTime = GetTime() * 1000
			if (playerCasting - currentTime) > 100 then
				isPlayerCasting = true
			end
		end

		return isPlayerCasting

	end

	IsCastingSpell = nil
	function IsCastingSpell(spellID, tar)

		local tar = tar or "player"
		local spellName = GetSpellInfo(spellID)
		local spellCasting = UnitCastingInfo(tar)

		if not spellCasting then spellCasting = UnitChannelInfo(tar) end

		if spellCasting == spellName then
			return true
		else
			return false
		end
	end


	function UnitInfront(unit1, unit2)
            if not (UnitExists(unit1) and UnitExists(unit2)) then return end
            local x1, y1, _,r = GetUnitPosition(unit1)
            local x2, y2, _ ,_= GetUnitPosition(unit2)
            local angle = atan2(y1 - y2, x1 - x2) - deg(r)
            if angle < 0 then
                angle = angle + 360
            end
            return (angle > 120 and angle < 240)
	end

	function isGCDOnCooldown(spellId)

	local startDA,durDA,_ = GetSpellCooldown(spellId)
	local cdDA = startDA + durDA - GetTime()

	return cdDA < GetSpellBaseCooldown(spellId) -2 and cdDA > 0

	end

	GCDSpellID = nil
	function GCDSpellID()

		local _, playerClass = UnitClass("player")

		if playerClass == "DEATHKNIGHT" then
			return 52375
		elseif playerClass == "DRUID" then
			return 774
		elseif playerClass == "HUNTER" then
			return 56641
		elseif playerClass == "MAGE" then
			return 1459
		elseif playerClass == "PALADIN" then
			return 85256
		elseif playerClass == "PRIEST" then
			return 2050
		elseif playerClass == "ROGUE" then
			return 1752
		elseif playerClass == "SHAMAN" then
			return 45284
		elseif playerClass == "WARLOCK" then
			return 980
		elseif playerClass == "WARRIOR" then
			return 1715
		elseif playerClass == "MONK" then
			return 100780
		else
			return 0
		end

	end

	isSpellAvailable = nil
	function isSpellAvailable(spellID)

		

		local gcdSpell = GCDSpellID()
		local gcdStartTime, gcdDuration = GetSpellCooldown(gcdSpell)
		local spellStartTime, spellDuration = GetSpellCooldown(spellID)
		local spellUsable = IsUsableSpell(spellID)
		local spellAvailable = false
		local SpellAvailableTime = 0.125

		if spellUsable then
			if spellStartTime ~= nil and gcdStartTime ~= nil then
				local spellTimeLeft = spellStartTime + spellDuration - GetTime()
				local gcdTimeLeft = gcdStartTime + gcdDuration - GetTime()
				if gcdTimeLeft <= 0 then
					--Our GCD spell is not on CD.
					if spellTimeLeft <= SpellAvailableTime then
						--spell will be off CD within 50ms.
						spellAvailable = true
					end
				else
					--Our GCD spell is on CD.
					if spellTimeLeft <= gcdTimeLeft + SpellAvailableTime then
						--spell time left is less than GCD time left + 50ms.
						spellAvailable = true
					end
				end
			end
		end

		return spellAvailable

	end
	
	UnitBuffID = nil
	function UnitBuffID(unit, spellID, filter)

		local spellName = GetSpellInfo(spellID)
		if filter == nil then
			return UnitBuff(unit, spellName)
		else
			local exactSearch = strfind(strupper(filter), "EXACT")
			local playerSearch = strfind(strupper(filter), "PLAYER")

			if exactSearch then
				--using the index does not support filter.
				for i=1,40 do
					local _, _, _, _, _, _, _, buffCaster, _, _, buffSpellID = UnitBuff(unit, i)
					if buffSpellID ~= nil then
						if buffSpellID == spellID then
							if (not playerSearch) or (playerSearch and (buffCaster == "player")) then
								return UnitBuff(unit, i)
							end
						end
					else
						return nil
					end
				end
			else
				--just pass the filter to UnitBuff and return.
				return UnitBuff(unit, spellName, nil, filter)
			end
		end

	end

	UnitDebuffID = nil
	function UnitDebuffID(unit, spellID, filter)

		local spellName = GetSpellInfo(spellID)
		if filter == nil then
			return UnitDebuff(unit, spellName)
		else
			local exactSearch = strfind(strupper(filter), "EXACT")
			local playerSearch = strfind(strupper(filter), "PLAYER")

			if exactSearch then
				--using the index does not support filter.
				for i=1,40 do
					local _, _, _, _, _, _, _, buffCaster, _, _, buffSpellID = UnitDebuff(unit, i)
					if buffSpellID ~= nil then
						if buffSpellID == spellID then
							if (not playerSearch) or (playerSearch and (buffCaster == "player")) then
								return UnitDebuff(unit, i)
							end
						end
					else
						return nil
					end
				end
			else
				--just pass the filter to UnitDebuff and return.
				return UnitDebuff(unit, spellName, nil, filter)
			end
		end

	end

	RegisterMovement = nil
	function RegisterMovement(elapsed)

		local playerMovementSpeed = GetUnitSpeed("player")

		if not CurrentMovingTime then CurrentMovingTime = 0 end
		if not CurrentStationaryTime then CurrentStationaryTime = 0 end
		if not ResetMovementTime then ResetMovementTime = 0.5 end

		if playerMovementSpeed ~= 0 then
			CurrentMovingTime = CurrentMovingTime + elapsed
			CurrentStationaryTime = 0
		else
			if CurrentStationaryTime < ResetMovementTime then
				CurrentStationaryTime = CurrentStationaryTime + elapsed
			end
			if CurrentStationaryTime > ResetMovementTime then
				CurrentMovingTime = 0
			end
		end

	end

	IsMoving = nil
	function IsMoving(seconds)

		if not seconds then seconds = 1 end
		if not CurrentMovingTime then CurrentMovingTime = 0 end

		local moltenFeathers = UnitBuffID("player", 98767)

		if CurrentMovingTime >= seconds and not moltenFeathers then
			return true
		else
			return false
		end
	end


function spellReady(spell)
                if IsSpellKnown(spell) and IsUsableSpell(spell) and isSpellAvailable(spell) then
                                return true
                end
                return false;
end


function GetCooldownTime(spellId)
		local startDA,durDA,_ = GetSpellCooldown(spellId)
		return startDA + durDA - GetTime() or 0
end

function HaveBuff(UnitID,SpellID,TimeLeft,Filter)
  if not TimeLeft then TimeLeft = 0 end
  if type(SpellID) == "number" then SpellID = { SpellID } end
  for i=1,#SpellID do
    local spell, rank = GetSpellInfo(SpellID[i])
    if spell then
      local buff = select(7,UnitBuff(UnitID,spell,rank,Filter))
      if buff and ( buff == 0 or buff - GetTime() > TimeLeft ) then return true end
      end
  end
end


function HaveDebuff(UnitID,SpellID,TimeLeft,Filter)
  if not TimeLeft then TimeLeft = 0 end
  if type(SpellID) == "number" then SpellID = { SpellID } end
  for i=1,#SpellID do
    local spell, rank = GetSpellInfo(SpellID[i])
    if spell then
      local debuff = select(7,UnitDebuff(UnitID,spell,rank,Filter))
      if debuff and ( debuff == 0 or debuff - GetTime() > TimeLeft ) then return true end
     end
  end
end




function CalculateHP(t)

	local incomingheals = UnitGetIncomingHeals(t) and UnitGetIncomingHeals(t) or 0
	local PercentWithIncoming = 100 *  UnitHealth(t) / UnitHealthMax(t)
	local ActualWithIncoming = ( UnitHealthMax(t) -  UnitHealth(t)  )
	if PercentWithIncoming and ActualWithIncoming then
					return PercentWithIncoming, ActualWithIncoming
	else
					return 100,UnitHealthMax("player")
	end
end

function validTarget(t)
	if not UnitIsCharmed(t)
	and UnitIsConnected(t)
	--and UnitExists(t)
	and not UnitIsEnemy(t,'player')
	and not UnitIsUnit(t,'player')
	and InLineOfSight(t) == 1
	and not UnitIsDeadOrGhost(t)
	and HaveDebuff(t,76577) == nil -- Smoke Bomb - Rogue
	and not HaveDebuff(t,33786)
	and (GetUnitDistance(t) or 50) < 40
	then
					return true
	else
					return false
	end
end


function table.empty(tbl)
  for i, _ in ipairs(tbl) do tbl[i] = nil end
end


function stealthFinder(spell)
	for _,v in pairs(enemyTargets) do
		if HaveBuff(v, {1856, 1784, 5215, 66, 58984})  and isSpellAvailable(spell) and UnitIsEnemy('player',v) and not HaveDebuff(v,spell) then
			AceCastSpellByName(GetSpellInfo(spell), v)
		end
	end
end


function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    elseif type(v) == 'boolean' then
      print(formatting .. tostring(v))
    else
      print(formatting .. v)
    end
  end
end



function isMage(target)
	local _,class,_ = UnitClass(target)
	return class == 'MAGE'
end

function IsMeleeDps(target)

		if  UnitIsPlayer(target) == nil then
			return false
		end
		local _,class,_ = UnitClass(target)
		local id = GetInspectSpecialization(target)
        return class == "ROGUE"
        or (class == "WARRIOR")
        or (class == "DEATHKNIGHT")
        or (class == "MONK" and (id == 269 ))
        or (class == "PALADIN" and (id == 67))
		or (class == "SHAMAN" and (id == 263 ))
        or (class == "DRUID" and (id == 103))
end

local slows = { 45524, 1715, 3408,  20164, 25809, 31589, 51585, 50040, 50041, 31126, 31124,  45524, 50040, 45334,  19185,  5116, 61394, 2974, 54644, 50245, 50271,
54706, 4167,   55080, 11113, 6136, 31589, 63529, 20170, 87194, 31125, 3409, 26679, 64695,
 63685, 8034, 18118, 18223, 63311, 23694, 1715, 12323, 39965, 55536, 13099, 29703 }
function getSlows()
return slows
end
--[[interruptID =     {
    [102060] = true,    --Disrupting Shout
    [106839] = true,    --Skull Bash
    [80964] = true,        --Skull Bash
    [115781] = true,    --Optical Blast
    [116705] = true,    --Spear Hand Strike
    [1766] = true,         --Kick
    [19647] = true,     --Spell Lock
    [2139] = true,         --Counterspell
    [47476] = true,        --Strangulate
    [47528] = true,     --Mind Freeze
    [57994] = true,     --Wind Shear
    [6552] = true,         --Pummel
    [96231] = true,     --Rebuke
    [31935] = true,        --Avenger's Shield
    [34490] = true,     --Silencing Shot
    [147362] = true     --Counter shot
    }]]--

	interruptID =     {
    [102060] = { class = "WARRIOR" },    --Disrupting Shout
    [106839] =  { class = "DRUID" },    --Skull Bash
    [80964] =  { class = "DRUID" },        --Skull Bash
    [115781] =  { class = "MAGE" },    --Optical Blast
    [116705] =  { class = "MONK" },    --Spear Hand Strike
    [1766] =  { class = "ROGUE" },         --Kick
    [19647] =  { class = "WARLOCK" },     --Spell Lock
    [2139] =  { class = "MAGE" },         --Counterspell
    [47476] =  { class = "DEATHKNIGHT" },        --Strangulate
    [47528] =  { class = "DEATHKNIGHT" },     --Mind Freeze
    [57994] =  { class = "SHAMAN" },     --Wind Shear
    [6552] = { class = "WARRIOR" },         --Pummel
    [96231] =  { class = "PALADIN" },     --Rebuke
    [31935] =  { class = "PALADIN" },        --Avenger's Shield
    [34490] =  { class = "HUNTER" },     --Silencing Shot
    [147362] =  { class = "HUNTER" }     --Counter shot
    }

polys = {118,28271,28272,61721,61780,126819,161354,161355,161372,51514,3355,20066,2094}
function getPolys()
return polys
end

healingInterrupt = {
 5185, -- healing touch
 8936, -- regrowth
 50464, -- nourishB
 331, -- healing wave
 1064, -- Chain Heal        (cast)
 77472, -- Greater Healing Wave    (cast)
 8004, -- Healing Surge        (cast)
 73920, -- Healing Rain        (cast)
 8936, -- Regrowth        (cast)
 2061, -- Flash Heal        (cast)
 2060, -- Greater Heal        (cast)
 5185, -- Healing Touch        (cast)
 596, -- Prayer of Healing    (cast)
 19750, -- Flash of Light    (cast)
 635,8004,47540,115175,33076,85673,8936,116858-- Holy Light        (cast)

}
function getHealingInterrupt()
return healingInterrupt
end

spellInterupt = {118,28271,28272,61721,61780,126819,161354,161355,161372,33786,5782,118699,51514,80240,20066} -- and cyclone

channelInturrupt = {
1120, -- Drain Soul		(channeling cast)
12051, -- Evocation		(channeling cast)
115294, -- Mana Tea		(channeling cast)
115175, -- Soothing Mist	(channeling cast)
64843, -- Divine Hymn		(channeling cast)
64901,47540,755 -- Hymn of Hope		(channeling cast)

 }
function getChannelInturrupt()
return channelInturrupt
end

allRoots = {
    91807,        -- Shambling Rush (Dark Transformation)
    45334,        -- Immobilized (Wild Charge - Bear)
    128405,        -- Narrow Escape
	107566,        -- Staggering Shout
    96294,        -- Chains of Ice (Chilblains)
    339,        -- Entangling Roots
    19975,        -- Entangling Roots (Nature's Grasp)
    102359,        -- Mass Entanglement
    110693,        -- Frost Nova (Mage)
    19185,        -- Entrapment
    50245,        -- Pin (Crab)
    54706,        -- Venom Web Spray (Silithid)
    4167,        -- Web (Spider)
    122,        -- Frost Nova
    111340,        -- Ice Ward
    33395,        -- Freeze
    113275,        -- Entangling Roots (Symbiosis)
    123407,        -- Spinning Fire Blossom
    113275,        -- Entangling Roots (Symbiosis)
    87194,        -- Glyph of Mind Blast
    114404,        -- Void Tendril's Grasp
    115197,        -- Partial Paralysis
    64695,        -- Earthgrab (Earthgrab Totem)
    63685        -- Freeze (Frozen Power)
    }
function aRoots()
return allRoots
end

defensiveCds = {118038, 19263,61336,22812,45438,642,47585,5277,108271,104773,1022,116849,53480,102342}
function getDefenceCds()
return defensiveCds
end


dmgCC = {
		-- Death Knight
		108194 , -- Asphyxiate
		 91800 , -- Gnaw (Ghoul)
		 91797 , -- Monstrous Blow (Dark Transformation Ghoul)
		115001 , -- Remorseless Winter
		-- Druid
		 22570 , -- Maim
		  5211 , -- Mighty Bash
		163505 ,1822, -- Rake (Stun from Prowl)
		-- Hunter
		117526 , 109248, -- Binding Shot
		 24394 , 19577, -- Intimidation
		-- Mage
		 44572 , -- Deep Freeze
		-- Monk
		119392 , -- Charging Ox Wave
		120086 , 113656, -- Fists of Fury
		119381 , -- Leg Sweep
		-- Paladin
		   853 , -- Hammer of Justice
		119072 , -- Holy Wrath
		105593 , -- Fist of Justice
		-- Rogue
		  1833 , -- Cheap Shot
		   408 , -- Kidney Shot
		-- Shaman
		118345 , -- Pulverize (Primal Earth Elemental)
		118905 , -- Static Charge (Capacitor Totem)
		-- Warlock
		 89766 , -- Axe Toss (Felguard)
		 30283 , -- Shadowfury
		 22703 , 1122, -- Summon Infernal
		-- Warrior
		132168 , -- Shockwave
		132169 , -- Storm Bolt
		-- Tauren
		 20549 , -- War Stomp

    }
function allStuns()
return dmgCC
end

dispelMagicCC = {
115001,        -- Remorseless Winter
2637,        -- Hibernate
110698,        -- Hammer of Justice (Paladin)
--117526,        -- Binding Shot
3355,        -- Freezing Trap
1513,        -- Scare Beast
118271,        -- Combustion Impact
44572,        -- Deep Freeze
31661,        -- Dragon's Breath
118,        -- Polymorph
61305,        -- Polymorph: Black Cat
28272,        -- Polymorph: Pig
61721,        -- Polymorph: Rabbit
61780,        -- Polymorph: Turkey
28271,        -- Polymorph: Turtle
82691,        -- Ring of Frost
11129,         -- Combustion
123393,        -- Breath of Fire (Glyph of Breath of Fire)
115078,        -- Paralysis
105421,        -- Blinding Light
115752,        -- Blinding Light (Glyph of Blinding Light)
105593,        -- Fist of Justice
853,        -- Hammer of Justice
119072,        -- Holy Wrath
20066,        -- Repentance
10326,        -- Turn Evil
64044,        -- Psychic Horror
8122,        -- Psychic Scream
113792,        -- Psychic Terror (Psyfiend)
9484,        -- Shackle Undead
118905,        -- Static Charge (Capacitor Totem)
5782,        -- Fear
118699,        -- Fear
5484,        -- Howl of Terror
6789,        -- Mortal Coil
30283,        -- Shadowfury
104045,        -- Sleep (Metamorphosis)
115268,        -- Mesmerize (Shivarra)
113092,        -- Frost Bomb
6358,
339,4167,145206,77351--,980,172--51514      -- Hex
}
function getDispellMagic()
return dispelMagicCC
end

local flags =
{112055,23335,34976,23333,140876,141210}
function getFlags()
return flags
end

enemyTargets = {"target","focus","mouseover","arena1","arena2","arena3","arena4","arena5","arenapet1","arenapet2","arenapet3","arenapet4","arenapet5","pettarget"}
function enemyNearDeath()
local arenaE = {"arena1","arena2","arena3","arena4","arena5"}
if not UnitExists('arena1') then
	return true
end
	for _,etarget in pairs(arenaE) do
		local arenaHealth = 100 * UnitHealth(etarget) / UnitHealthMax(etarget)
		if arenaHealth < 50 then return true end
	end
	return false
end
enemyTargets = {"target","focus","mouseover","arena1","arena2","arena3","arena4","arena5","arenapet1","arenapet2","arenapet3","arenapet4","arenapet5","pettarget"}
immunDispel = {
    45438,        -- Ice Block
    110700,        -- Divine Shield (Paladin)
    110696,        -- Ice Block (Mage)
    45438,        -- Ice Block
    1022,         --Hand of Protection
    642            -- Divine Shield
}
function getImmunes()
return immunDispel
end

--/script print(UnitGUID('player').." ----- "..GetObjectById(1))
function findTotems(spellId)
if not UnitExists('arena1') then
	--return
end

 local target  = UnitGUID('target')
 local totemGuid = nil
 local _, _, _, _, _, maxRange = GetSpellInfo(spellId)

 for i=1, #Ace.unitCacheAll do
	local guid = Ace.unitCacheAll[i].object
	local name,_ = UnitName(guid)
		 if (name == 'Spirit Link Totem' or name == 'Grounding Totem') and Ace.unitCacheAll[i].distance < maxRange then --and UnitCanAttack('player',guid) then or name == "Raider's Training Dummy"
			totemGuid = guid
			break
		 end
	end
	if totemGuid ~= nil then
		SetMouseoverTarget(totemGuid)
		if UnitCanAttack('player','mouseover') and InLineOfSight('mouseover') ==1 then
			FaceUnit('mouseover')
			AceCastSpellByName(GetSpellInfo(spellId),'mouseover')
		end
	end
 end

function MeleeAroundPlayer()

 for i=1, #Ace.unitCacheAll do
	local guid = Ace.unitCacheAll[i].object
	if UnitIsPlayer(guid) then
		if Ace.unitCacheAll[i].distance < 8 and IsMeleeDps(guid) and GetUnitTarget(guid) == UnitGUID('player') then
			return true
		end
	end
end
return false
end

 function stopMoving()
	AceRunMacroText("/run MoveBackwardStop()")
	AceRunMacroText("/run MoveForwardStop()")
	AceRunMacroText("/run StrafeLeftStop()")
	AceRunMacroText("/run StrafeRightStop()")
 end

 function tauntPet(spellId)

 local pets = {"arenapet1","arenapet2","arenapet3","arenapet4","arenapet5"}
	 for _,etarget in pairs(pets) do
		if UnitExists(etarget) then
			AceCastSpellByName(GetSpellInfo(spellId),etarget)
			break
		end
	 end
 end

function canInturrupt()


if not OmniBar then
	return false
end
local target = false
--[[ for i=1, #Ace.unitCacheAll do
	local guid = Ace.unitCacheAll[i].object


	if Ace.unitCacheAll[i].distance < 8 and IsMeleeDps(guid) then
			local unitClass,UnitClass=UnitClass(guid)
			--print(unitClass)
			for j = 1, #OmniBar.icons do
			--print(OmniBar.icons[j].cooldown)
				if OmniBar.icons[j].spellID ~= nil and  OmniBar.icons[j].cooldown.finish < GetTime() and OmniBar.icons[j].sourceGUID == guid then
				--print(OmniBar.icons[j].cooldown.finish)
				local name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(OmniBar.icons[j].spellID)
					if interruptID[OmniBar.icons[j].spellID] and interruptID[OmniBar.icons[j].spellID].class == UnitClass then
						print("Attempting to fake "..unitClass.."'s "..name)
						target = true
					end
				end
			end
	end
 end]]--
	return target
end
--/script print(checkDR(UnitGUID('player'),33786)) 1= 1/2 2 = 1/4 = 3 = Immune
function checkDR(target , spell)
local addon = _G.DiminishingReturns
local guid = UnitGUID(target)
	if addon then
	local cat = addon.SPELLS[spell]
	for catName,j,k,count in addon:IterateDR(guid)
		do
		if catName == cat then
		return count
		end
		end
	end
return 0
end

 local cooldownsList = {
	[47476]  = { default = true,  duration = 60,  class = "DEATHKNIGHT" },                                   -- Strangulate
	[47481]  = { default = false, duration = 60,  class = "DEATHKNIGHT", specID = { 252 } },                 -- Gnaw (Ghoul)
	[47482]  = { default = false, duration = 30,  class = "DEATHKNIGHT", specID = { 252 } },                 -- Leap (Ghoul)
	[47528]  = { default = true,  duration = 15,  class = "DEATHKNIGHT" },                                   -- Mind Freeze
	[48707]  = { default = false, duration = 45,  class = "DEATHKNIGHT" },                                   -- Anti-Magic Shell
	[48743]  = { default = false, duration = 120, class = "DEATHKNIGHT" },                                   -- Death Pact
	[48792]  = { default = false, duration = 180, class = "DEATHKNIGHT" },                                   -- Icebound Fortitude
	[49028]  = { default = false, duration = 90,  class = "DEATHKNIGHT", specID = { 250 } },                 -- Dancing Rune Weapon
	[49039]  = { default = false, duration = 120, class = "DEATHKNIGHT" },                                   -- Lichborne
	[49576]  = { default = false, duration = 25,  class = "DEATHKNIGHT" },                                   -- Death Grip
	[51052]  = { default = false, duration = 120, class = "DEATHKNIGHT" },                                   -- Anti-Magic Zone
	[51271]  = { default = false, duration = 60,  class = "DEATHKNIGHT", specID = { 251 } },                 -- Pillar of Frost
	[55233]  = { default = false, duration = 60,  class = "DEATHKNIGHT", specID = { 250 } },                 -- Vampiric Blood
	[77606]  = { default = false, duration = 30,  class = "DEATHKNIGHT" },                                   -- Dark Simulacrum
	[91802]  = { default = true,  duration = 30,  class = "DEATHKNIGHT", specID = { 252 } },                 -- Shambling Rush
	[96268]  = { default = false, duration = 30,  class = "DEATHKNIGHT" },                                   -- Death's Advance
	[108194] = { default = false, duration = 30,  class = "DEATHKNIGHT" },                                   -- Asphyxiate
	[108201] = { default = false, duration = 120, class = "DEATHKNIGHT" },                                   -- Desecrated Ground
	[152279] = { default = false, duration = 120, class = "DEATHKNIGHT" },                                   -- Breath of Sindragosa
	[498]    = { default = false, duration = 30,  class = "PALADIN" },                                       -- Divine Protection
	[642]    = { default = false, duration = 150, class = "PALADIN" },                                       -- Divine Shield
	[853]    = { default = false, duration = 60,  class = "PALADIN" },                                       -- Hammer of Justice
	    [105593] = { parent = 853, duration = 30 },                                                          -- Fist of Justice
	[1022]   = { default = false, duration = 300, class = "PALADIN", charges = 2 },                          -- Hand of Protection
	[1044]   = { default = false, duration = 25,  class = "PALADIN", charges = 2 },                          -- Hand of Freedom
	[6940]   = { default = false, duration = { default = 90, [65] = 110 }, class = "PALADIN", charges = 2 }, -- Hand of Sacrifice
	[20066]  = { default = false, duration = 15,  class = "PALADIN" },                                       -- Repentance
	[31821]  = { default = false, duration = 180, class = "PALADIN", specID = { 65 } },                      -- Devotion Aura
	[31884]  = { default = false, duration = 120, class = "PALADIN" },                                       -- Avenging Wrath
	[96231]  = { default = true,  duration = 15,  class = "PALADIN" },                                       -- Rebuke
	[114039] = { default = false, duration = 30,  class = "PALADIN" },                                       -- Hand of Purity
	[114157] = { default = false, duration = 60,  class = "PALADIN" },                                       -- Execution Sentence
	[115750] = { default = false, duration = 120, class = "PALADIN" },                                       -- Blinding Light
	[871]    = { default = false, duration = 180, class = "WARRIOR", specID = { 73 } },                      -- Shield Wall
	[1719]   = { default = false, duration = 180, class = "WARRIOR", specID = { 71, 72 } },                  -- Recklessness
	[3411]   = { default = false, duration = 30,  class = "WARRIOR" },                                       -- Intervene
	[5246]   = { default = false, duration = 90,  class = "WARRIOR" },                                       -- Intimidating Shout
	[6544]   = { default = false, duration = 45,  class = "WARRIOR" },                                       -- Heroic Leap
	[6552]   = { default = true,  duration = 15,  class = "WARRIOR" },                                       -- Pummel
	[18499]  = { default = false, duration = 30,  class = "WARRIOR" },                                       -- Berserker Rage
	[23920]  = { default = false, duration = 25,  class = "WARRIOR" },                                       -- Spell Reflection
		[114028] = { parent = 23920, duration = 30 },                                                        -- Mass Spell Reflection
	[46968]  = { default = false, duration = 20,  class = "WARRIOR" },                                       -- Shockwave
	[107570] = { default = false, duration = 30,  class = "WARRIOR" },                                       -- Storm Bolt
	[107574] = { default = false, duration = 90,  class = "WARRIOR" },                                       -- Avatar
	[114029] = { default = false, duration = 30,  class = "WARRIOR" },                                       -- Safeguard
	[118000] = { default = false, duration = 60,  class = "WARRIOR" },                                       -- Dragon Roar
	[118038] = { default = false, duration = 120, class = "WARRIOR", specID = { 71, 72 } },                  -- Die by the Sword
	[99]     = { default = false, duration = 30,  class = "DRUID" },                                         -- Disorienting Roar
	[5211]   = { default = false, duration = 50,  class = "DRUID" },                                         -- Bash
	[22812]  = { default = false, duration = 60,  class = "DRUID", specID = { 102, 104, 105 } },             -- Barkskin
	[33891]  = { default = false, duration = 180, class = "DRUID", specID = { 105 } },                       -- Incarnation: Tree of Life
	[50334]  = { default = false, duration = 180, class = "DRUID", specID = { 103, 104 } },                  -- Berserk
	[61336]  = { default = false, duration = 180, class = "DRUID", specID = { 103, 104 }, charges = 2 },     -- Survival Instincts
	[78675]  = { default = true,  duration = 60,  class = "DRUID", specID = { 102 } },                       -- Solar Beam
	[102280] = { default = false, duration = 30,  class = "DRUID" },                                         -- Displacer Beast
	[102342] = { default = false, duration = 60,  class = "DRUID", specID = { 105 } },                       -- Ironbark
	[102359] = { default = false, duration = 30,  class = "DRUID" },                                         -- Mass Entanglement
	[102543] = { default = false, duration = 180, class = "DRUID", specID = { 103 } },                       -- Incarnation: King of the Jungle
	[102560] = { default = false, duration = 180, class = "DRUID", specID = { 102 } },                       -- Incarnation: Chosen of Elune
	[106839] = { default = true,  duration = 15,  class = "DRUID", specID = { 103, 104 } },                  -- Skull Bash
	[108291] = { default = false, duration = 360, class = "DRUID" },                                         -- Heart of the Wild (Balance)
		[108292] = { parent = 108291 },                                                                      -- Heart of the Wild (Feral)
		[108293] = { parent = 108291 },                                                                      -- Heart of the Wild (Guardian)
		[108294] = { parent = 108291 },                                                                      -- Heart of the Wild (Resto)
	[112071] = { default = false, duration = 180, class = "DRUID", specID = { 102 } },                       -- Celestial Alignment
	[124974] = { default = false, duration = 90,  class = "DRUID" },                                         -- Nature's Vigil
	[132158] = { default = false, duration = 60,  class = "DRUID", specID = { 105 } },                       -- Nature's Swiftness
	[132469] = { default = false, duration = 30,  class = "DRUID" },                                         -- Typhoon
	[159630] = { default = false, duration = 90,  class = "PRIEST", specID = { 256, 257 } },                 -- Shadow Magic
	[8122]   = { default = false, duration = 30,  class = "PRIEST" },                                        -- Psychic Scream
	[15487]  = { default = true,  duration = 45,  class = "PRIEST", specID = { 256, 258 } },                 -- Silence
	[33206]  = { default = false, duration = 120, class = "PRIEST", specID = { 256 } },                      -- Pain Suppression
	[47585]  = { default = false, duration = 120, class = "PRIEST", specID = { 258 } },                      -- Dispersion
	[47788]  = { default = false, duration = 180, class = "PRIEST", specID = { 257 } },                      -- Guardian Spirit
	[64044]  = { default = false, duration = 45,  class = "PRIEST", specID = { 258 } },                      -- Psychic Horror
	[73325]  = { default = false, duration = 90,  class = "PRIEST" },                                        -- Leap of Faith
	[5484]   = { default = false, duration = 40,  class = "WARLOCK" },                                       -- Howl of Terror
	[6360]   = { default = false, duration = 25,  class = "WARLOCK" },                                       -- Whiplash
	[6789]   = { default = false, duration = 45,  class = "WARLOCK" },                                       -- Mortal Coil
	[19505]  = { default = false, duration = 15,  class = "WARLOCK" },                                       -- Devour Magic (Felhunter)
	[30283]  = { default = false, duration = 30,  class = "WARLOCK" },                                       -- Shadowfury
	[48020]  = { default = false, duration = 26,  class = "WARLOCK" },                                       -- Demonic Portal
	[89766]  = { default = false, duration = 30,  class = "WARLOCK" },                                       -- Axe Toss
	[108482] = { default = false, duration = 120, class = "WARLOCK" },                                       -- Unbound Will
	[119910] = { default = true,  duration = 24,  class = "WARLOCK" },                                       -- Spell Lock (Command Demon)
	    [19647]  = { parent = 119910 },                                                                      -- Spell Lock (Felhunter)
	    [119911] = { parent = 119910 },                                                                      -- Optical Blast (Command Demon)
	    [115781] = { parent = 119910 },                                                                      -- Optical Blast (Observer)
	    [132409] = { parent = 119910 },                                                                      -- Spell Lock (Grimoire of Sacrifice)
	    [171138] = { parent = 119910 },                                                                      -- Shadow Lock (Doomguard)
	    [171139] = { parent = 119910 },                                                                      -- Shadow Lock (Grimoire of Sacrifice)
	    [171140] = { parent = 119910 },                                                                      -- Shadow Lock (Command Demon)
	[111859] = { default = false, duration = 120, class = "WARLOCK" },                                       -- Grimoire: Imp
	[111896] = { default = false, duration = 120, class = "WARLOCK" },                                       -- Grimoire: Succubus
	[111897] = { default = true,  duration = 120, class = "WARLOCK" },                                       -- Grimoire: Felhunter
	[77801]  = { default = false, duration = 120, class = "WARLOCK", charges = 2 },                          -- Dark Soul
		[113858] = { parent = 77801 },                                                                       -- Dark Soul: Instability
		[113860] = { parent = 77801 },                                                                       -- Dark Soul: Misery
		[113861] = { parent = 77801 },                                                                       -- Dark Soul: Knowledge
	[115284] = { default = false, duration = 15,  class = "WARLOCK" },                                       -- Clone Magic (Observer)
	[115770] = { default = false, duration = 25,  class = "WARLOCK" },                                       -- Fellash
	[8143]   = { default = false, duration = 60,  class = "SHAMAN" },                                        -- Tremor Totem
	[8177]   = { default = false, duration = 25,  class = "SHAMAN" },                                        -- Grounding Totem
	[30823]  = { default = false, duration = 60,  class = "SHAMAN", specID = { 262, 263 } },                 -- Shamanistic Rage
	[51490]  = { default = false, duration = 45,  class = "SHAMAN", specID = { 262, 263 } },                 -- Thunderstorm
	[51514]  = { default = false, duration = 45,  class = "SHAMAN" },                                        -- Hex
	[57994]  = { default = true,  duration = 12,  class = "SHAMAN" },                                        -- Wind Shear
	[98008]  = { default = false, duration = 180, class = "SHAMAN" },                                        -- Spirit Link Totem
	[108269] = { default = false, duration = 45,  class = "SHAMAN" },                                        -- Capacitor Totem
	[108271] = { default = false, duration = 90,  class = "SHAMAN" },                                        -- Astral Shift
	[108273] = { default = false, duration = 60,  class = "SHAMAN" },                                        -- Windwalk Totem
	[108285] = { default = false, duration = 180, class = "SHAMAN" },                                        -- Call of the Elements
	[1499]   = { default = false, duration = { default = 20, [253] = 30, [254] = 30 }, class = "HUNTER" },   -- Freezing Trap
	    [60192] = { parent = 1499 },                                                                         -- Freezing Trap (Trap Launcher)
	[13813]  = { default = false, duration = { default = 20, [253] = 30, [254] = 30 }, class = "HUNTER" },   -- Explosive Trap
	    [82939] = { parent = 13813 },                                                                        -- Explosive Trap (Trap Launcher)
	[19263]  = { default = false, duration = 180, class = "HUNTER", charges = 2 },                           -- Deterrence
	[19386]  = { default = false, duration = 45,  class = "HUNTER" },                                        -- Wyvern Sting
	[19574]  = { default = false, duration = 60,  class = "HUNTER", specID = { 253 } },                      -- Bestial Wrath
	[53480]  = { default = false, duration = 60,  class = "HUNTER" },                                        -- Roar of Sacrifice
	[131894] = { default = false, duration = 60,  class = "HUNTER" },                                        -- A Murder of Crows
	[147362] = { default = true,  duration = 24,  class = "HUNTER" },                                        -- Counter Shot
	[66]     = { default = false, duration = 300, class = "MAGE" },                                          -- Invisibility
	[1953]   = { default = false, duration = 15,  class = "MAGE" },                                          -- Blink
	[2139]   = { default = true,  duration = 24,  class = "MAGE" },                                          -- Counterspell
	[11129]  = { default = false, duration = 45,  class = "MAGE", specID = { 63 } },                         -- Combustion
	[11958]  = { default = false, duration = 180, class = "MAGE" },                                          -- Cold Snap
	[12043]  = { default = false, duration = 90,  class = "MAGE", specID = { 62 } },                         -- Presence of Mind
	[12472]  = { default = false, duration = 180, class = "MAGE", specID = { 64 } },                         -- Icy Veins
	[31661]  = { default = false, duration = 20,  class = "MAGE", specID = { 63 } },                         -- Dragon's Breath
	[44572]  = { default = false, duration = 30,  class = "MAGE", specID = { 64 } },                         -- Deep Freeze
	[45438]  = { default = false, duration = 300, class = "MAGE" },                                          -- Ice Block
	[84714]  = { default = false, duration = 60,  class = "MAGE", specID = { 64 } },                         -- Frozen Orb
	[102051] = { default = false, duration = 20,  class = "MAGE" },                                          -- Frostjaw
	[113724] = { default = false, duration = 45,  class = "MAGE" },                                          -- Ring of Frost
	[157997] = { default = false, duration = 25,  class = "MAGE", specID = { 64 }, charges = 2 },            -- Ice Nova
	[408]    = { default = false, duration = 20,  class = "ROGUE" },                                         -- Kidney Shot
	[1766]   = { default = true,  duration = 15,  class = "ROGUE" },                                         -- Kick
	[1856]   = { default = false, duration = { default = 60, [261] = 120 }, class = "ROGUE" },               -- Vanish
	[2094]   = { default = false, duration = 120, class = "ROGUE" },                                         -- Blind
	[2983]   = { default = false, duration = 60,  class = "ROGUE" },                                         -- Sprint
	[5277]   = { default = false, duration = 180, class = "ROGUE" },                                         -- Evasion
	[13750]  = { default = false, duration = 180, class = "ROGUE", specID = { 260 } },                       -- Adrenaline Rush
	[14185]  = { default = false, duration = 300, class = "ROGUE" },                                         -- Preparation
	[31224]  = { default = false, duration = 60,  class = "ROGUE" },                                         -- Cloak of Shadows
	[36554]  = { default = false, duration = 20,  class = "ROGUE" },                                         -- Shadow Step
	[51690]  = { default = false, duration = 120, class = "ROGUE", specID = { 260} },                        -- Killing Spree
	[51713]  = { default = false, duration = 60,  class = "ROGUE", specID = { 261 } },                       -- Shadow Dance
	[74001]  = { default = false, duration = 120, class = "ROGUE" },                                         -- Combat Readiness
	[76577]  = { default = false, duration = 180, class = "ROGUE" },                                         -- Smoke Bomb
	[115176] = { default = false, duration = 180, class = "MONK", specID = { 268, 269 } },                   -- Zen Meditation
	[115203] = { default = false, duration = 180, class = "MONK" },                                          -- Fortifying Brew
	[115310] = { default = false, duration = 180, class = "MONK" },                                          -- Revival
	[116705] = { default = true,  duration = 15,  class = "MONK" },                                          -- Spear Hand Strike
	[116844] = { default = false, duration = 45,  class = "MONK" },                                          -- Ring of Peace
	[116849] = { default = false, duration = 55,  class = "MONK", specID = { 270 } },                        -- Life Cocoon
	[119381] = { default = false, duration = 45,  class = "MONK" },                                          -- Leg Sweep
	[119996] = { default = false, duration = 25,  class = "MONK" },                                          -- Transcendence: Transfer
	[122470] = { default = false, duration = 90,  class = "MONK", specID = { 269 } },                        -- Touch of Karma
	[122783] = { default = false, duration = 90,  class = "MONK" },                                          -- Diffuse Magic
	[137562] = { default = false, duration = 120, class = "MONK" },                                          -- Nimble Brew
}

local order = {
	["DEATHKNIGHT"] = 1,
	["PALADIN"] = 2,
	["WARRIOR"] = 3,
	["DRUID"] = 4,
	["PRIEST"] = 5,
	["WARLOCK"] = 6,
	["SHAMAN"] = 7,
	["HUNTER"] = 8,
	["MAGE"] = 9,
	["ROGUE"] = 10,
	["MONK"] = 11,
}

local resets = {
	--[[ Summon Felhunter
	     - Spell Lock
	  ]]
	[691] = { 119910 },

	--[[ Cold Snap
	     - Ice Block
	     - Presence of Mind
	     - Dragon's Breath
	  ]]
	[11958] = { 45438, 12043, 31661 },

	--[[ Preparation
	     - Sprint
	     - Vanish
	     - Evasion
	  ]]
	[14185] = { 2983, 1856, 5277 },

	--[[ Call of the Elements
	     - Tremor Totem
	     - Grounding Totem
	     - Capacitor Totem
	     - Windwalk Totem
	  ]]
	[108285] = { 8143, 8177, 108269, 108273 },
}
